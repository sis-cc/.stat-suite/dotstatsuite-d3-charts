import { expect } from 'chai';
import { getTimeMajorStep } from '../src/utils/time';

describe('getTimeMajorStep tests', () => {
  it('data step', () => {
    expect(getTimeMajorStep({
      labelSize: 75,
      rangeDelta: 700,
      domainDelta: 5,
      options: {
        linear: { frequency: 'month', step: 1 },
      }
    })).to.equal(1);
  });
  it('yearly computed step', () => {
    expect(getTimeMajorStep({
      labelSize: 75,
      rangeDelta: 100,
      domainDelta: 5,
      options: {
        linear: { frequency: 'month', step: 1 },
      }
    })).to.equal(6);
  });
  it('multi yearly computed step', () => {
    expect(getTimeMajorStep({
      labelSize: 75,
      rangeDelta: 100,
      domainDelta: 24,
      options: {
        linear: { frequency: 'month', step: 1 },
      }
    })).to.equal(24);
  });
});
