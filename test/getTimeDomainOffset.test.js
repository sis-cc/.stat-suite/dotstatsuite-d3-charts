import { expect } from 'chai';
import { getTimeDomainDelta } from '../src/utils/time';

describe('getTimeDomainDelta tests', () => {
  it('1 year step test', () => {
    expect(getTimeDomainDelta({
      domain: [new Date('January 01 2015'), new Date('January 01 2020')],
      frequency: 'year',
      step: 1
    })).to.equal(5);
  });
  it('3 year step test', () => {
    expect(getTimeDomainDelta({
      domain: [new Date('January 01 2010'), new Date('January 01 2020')],
      frequency: 'year',
      step: 3
    })).to.equal(12);
  });
  it('1 month step test', () => {
    expect(getTimeDomainDelta({
      domain: [new Date('January 01 2010'), new Date('May 01 2012')],
      frequency: 'month',
      step: 1
    })).to.equal(28);
  });
  it('5 month step test', () => {
    expect(getTimeDomainDelta({
      domain: [new Date('January 01 2010'), new Date('December 01 2010')],
      frequency: 'month',
      step: 5
    })).to.equal(15);
  });
  it('1 day step test', () => {
    expect(getTimeDomainDelta({
      domain: [new Date('January 01 2010'), new Date('February 08 2010')],
      frequency: 'day',
      step: 1
    })).to.equal(38);
  });
  it('7 day step test', () => {
    expect(getTimeDomainDelta({
      domain: [new Date('January 01 2010'), new Date('February 08 2010')],
      frequency: 'day',
      step: 7
    })).to.equal(42);
  });
  it('1 hour step test', () => {
    expect(getTimeDomainDelta({
      domain: [new Date('January 01 2010 00:00:00'), new Date('January 02 2010 02:30:00')],
      frequency: 'hour',
      step: 1
    })).to.equal(27);
  });
  it('4 hour step test', () => {
    expect(getTimeDomainDelta({
      domain: [new Date('January 01 2010 00:00:00'), new Date('January 02 2010 02:30:00')],
      frequency: 'hour',
      step: 4
    })).to.equal(28);
  });
  it('1 minute step test', () => {
    expect(getTimeDomainDelta({
      domain: [new Date('January 01 2010 00:00:00'), new Date('January 01 2010 02:32:15')],
      frequency: 'minute',
      step: 1
    })).to.equal(153);
  });
  it('1 minute step test', () => {
    expect(getTimeDomainDelta({
      domain: [new Date('January 01 2010 00:00:00'), new Date('January 01 2010 02:32:15')],
      frequency: 'minute',
      step: 20
    })).to.equal(160);
  });
});
