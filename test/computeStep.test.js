import { expect } from 'chai';
import { computeStep } from '../src/utils/linear';

describe('computeLinearStep tests', () => {
  it('small axis basic test', () => {
    expect(computeStep({
      rangeDelta: 500,
      domainDelta: 1000,
      minSize: 50,
      maxSize: 100,
      sizeThreshold: 550,
    })).to.deep.equal(200);
  });
  it('small axis less than 7 ticks', () => {
    expect(computeStep({
      rangeDelta: 100,
      domainDelta: 1000,
      minSize: 25,
      maxSize: 100,
      sizeThreshold: 550,
    })).to.deep.equal(300);
  });
  it ('small axis more than 7 ticks, more random sizes', () => {
    expect(computeStep({
      rangeDelta: 325,
      domainDelta: 2873,
      minSize: 18,
      maxSize: 100,
      sizeThreshold: 550,
    })).to.deep.equal(500);
  });
  it('big axis basic test', () => {
    expect(computeStep({
      rangeDelta: 1000,
      domainDelta: 2000,
      maxSize: 100,
      minSize: 5,
      sizeThreshold: 550
    })).to.deep.equal(200);
  });
  it('big axis less than 7 ticks', () => {
    expect(computeStep({
      rangeDelta: 600,
      domainDelta: 1750,
      maxSize: 120,
      minSize: 5,
      sizeThreshold: 550
    })).to.deep.equal(300);
  });
  it('big axis less than 7 ticks', () => {
    expect(computeStep({
      rangeDelta: 625,
      domainDelta: 6584,
      maxSize: 133,
      minSize: 5,
      sizeThreshold: 550,
    })).to.deep.equal(1500);
  });
});