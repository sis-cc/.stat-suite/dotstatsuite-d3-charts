import { expect } from 'chai';
import { computeReducedTicks } from '../src/utils/linear';

describe('computeReducedTicks', () => {
  it('basic test', () => {
    expect(computeReducedTicks({
      domainDelta: 72.614,
      min: 24.3
    })).to.deep.equal([25, 95]);
  });
  it('domainDelta nice rounded test', () => {
    expect(computeReducedTicks({
      domainDelta: 0.05,
      min: 87522.98213
    })).to.deep.equal([87522.99, 87523.015]);
  });
  it('big scale test', () => {
    expect(computeReducedTicks({
      domainDelta: 75625132,
      min: -58629.3
    })).to.deep.equal([-0, 70000000]);
  });
  it('small scale test', () => {
    expect(computeReducedTicks({
      domainDelta: 2.00125,
      min: 0.002
    })).to.deep.equal([0.002, 2.002]);
  })
});
