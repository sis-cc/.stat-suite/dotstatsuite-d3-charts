import { expect } from 'chai';
import { computeTicks } from '../src/utils/linear';

describe('computeLinearTicks tests', () => {
  it('basic test', () => {
    expect(computeTicks({
      min: -5,
      max: 5,
      pivot: 0,
      step: 5
    })).to.deep.equal([-5, 0, 5]);
  });
  it('all positive, pivot below', () => {
    expect(computeTicks({
      min: 122,
      max: 18057,
      pivot: 0,
      step: 2500
    })).to.deep.equal([2500, 5000, 7500, 10000, 12500, 15000, 17500]);
  });
  it('all positive, pivot above', () => {
    expect(computeTicks({
      min: 517,
      max: 4396,
      pivot : 5500,
      step: 500
    })).to.deep.equal([1000, 1500, 2000, 2500, 3000, 3500, 4000]);
  });
  it('all negative, pivot above', () => {
    expect(computeTicks({
      min: -830,
      max: -437,
      pivot : 10,
      step: 100
    })).to.deep.equal([-790, -690, -590, -490]);
  });
  it('all negative, pivot below', () => {
    expect(computeTicks({
      max: -1,
      min: -86,
      pivot : -94,
      step: 5
    })).to.deep.equal([-84, -79, -74, -69, -64, -59, -54, -49, -44, -39, -34, -29, -24, -19, -14, -9, -4]);
  });
  it('decimals pivot in domain', () => {
    expect(computeTicks({
      max: 0.0001,
      min: 0.00004,
      pivot : 0.00007,
      step: 0.00001
    })).to.deep.equal([0.00004, 0.00005, 0.00006, 0.00007, 0.00008, 0.00009, 0.0001]);
  });
  it('decimals with pivot above domain', () => {
    expect(computeTicks({
      max: 6234827.514,
      min: 6234827.482,
      pivot : 6234827.6,
      step: 0.01
    })).to.deep.equal([6234827.49, 6234827.5, 6234827.51]);
  });
});