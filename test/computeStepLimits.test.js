import { expect } from 'chai';
import { computeStepLimits } from '../src/utils/linear';

describe('computeStepLimits tests', () => {
  it('vertical test 1', () => {
    expect(computeStepLimits({
      options: {
        orient: 'left',
        linear: { minTickSizeFactor: 2, maxTickSizeFactor: 7 },
        font: {
          size: 10
        }
      }
    })).to.deep.equal({ min: 20, max: 70 });
  });
  it('vertical test 2', () => {
    expect(computeStepLimits({
      options: {
        orient: 'right',
        linear: { minTickSizeFactor: 2, maxTickSizeFactor: 7 },
        font: {
          size: 7
        }
      }
    })).to.deep.equal({ min: 14, max: 49 });
  });
  it('horzontal test 1', () => {
    expect(computeStepLimits({
      options: {
        orient: 'top',
        linear: { minTickSizeFactor: 2, maxTickSizeFactor: 7 },
        font: {
          size: 10
        }
      },
      domain: [-723.85, 19213],
      domainDelta: 19936.85
    })).to.deep.equal({min: 64, max: 144 });
  });
  it('horizontal test 2', () => {
    expect(computeStepLimits({
      options: {
        orient: 'bottom',
        linear: { minTickSizeFactor: 2, maxTickSizeFactor: 7 },
        font: {
          size: 10
        }
      },
      domain: [-0.00214, 0.00381],
      domainDelta: 0.00595
    })).to.deep.equal({ min: 72, max: 168 });
  });
});
