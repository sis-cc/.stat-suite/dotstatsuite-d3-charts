import { expect } from 'chai';
import { computeReducedStep } from '../src/utils/linear';

describe('computeReducedStep tests', () => {
  it('basic test', () => {
    expect(computeReducedStep({ domainDelta: 5.64 })).to.deep.equal({ step: 5, powerScale: 1 });
  });
  it('small decimals test', () => {
    expect(computeReducedStep({ domainDelta: 0.00349  })).to.deep.equal({ step: 0.003, powerScale: 0.001 });
  });
  it('big scale test', () => {
    expect(computeReducedStep({ domainDelta: 64635445 })).to.deep.equal({ step: 60000000, powerScale: 10000000 });
  });
});
