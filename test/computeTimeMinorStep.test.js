import { expect } from 'chai';
import { computeTimeMinorStep } from '../src/utils/time';

describe('computeTimeMinoStep tests', () => {
  it('data step equals major step', () => {
    expect(computeTimeMinorStep({
      dataStep: 1,
      majorStep: 1,
      domainDelta: 10,
      rangeDelta: 100,
      fequency: 'year'
    })).to.equal(1);
  });
  it('enough place for data step', () => {
    expect(computeTimeMinorStep({
      dataStep: 1,
      majorStep: 10,
      domainDelta: 10,
      rangeDelta: 100,
      frequency: 'year'
    })).to.equal(1);
  });
  it('month step computed', () => {
    expect(computeTimeMinorStep({
      dataStep: 12,
      majorStep: 24,
      domainDelta: 24,
      rangeDelta: 50,
      frequency: 'month'
    })).to.equal(12);
  });
});
