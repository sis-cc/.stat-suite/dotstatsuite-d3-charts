import * as R from 'ramda';

const datapointFactory = (reference, frequency) => (index) => {
  let x;
  if (frequency === 'yearly') {
    x = new Date(Date.UTC(reference.getUTCFullYear()+index, reference.getUTCMonth(), 1));
  } else if (frequency === 'monthly') {
    x = new Date(Date.UTC(reference.getUTCFullYear(), reference.getUTCMonth()+index, 1));
  }

  const y = Math.random() * 100;

  // JSON API exc
  if (index == 0) x = x.toJSON();

  return { x, y };
};

const datapointsFactory = (count=20, reference=new Date(), frequency='monthly') => (
  R.map(datapointFactory(reference, frequency), R.range(0, count))
);

const indexesBreaks = [0, 8, 9, 11, 16];

const serieFactory = (n) => ({
  datapoints: R.pipe(
    R.when(
      R.always(n === 0),
      R.addIndex(R.map)((dp, i) => R.includes(i, indexesBreaks)
        ? ({ ...dp, y: null }) : dp)
    ),
    R.when(
      R.always(n === 2),
      R.addIndex(R.reduce)((acc, dp, i) => R.includes(i, indexesBreaks)
        ? acc : R.append(dp, acc), [])
    )
  )(datapointsFactory()),
  highlightIndex: n - 1,
  category: n == 0 ? 'short category' : 'long very long and so long so very long category'
});

const seriesFactory = (count=3) => R.map(serieFactory, R.range(0, count));

export default () => seriesFactory();
