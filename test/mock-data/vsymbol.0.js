export default {
  data: [
    {
      "datapoints": [
        {
          "category": "Air passenger transport - Number",
          "formatedValues": [
            null,
            "4 855"
          ],
          "values": [
            null,
            4855
          ],
          "key": "TRANSPORT_AIR - NUMBER"
        },
        {
          "category": "Food and beverage serving industries - Number",
          "formatedValues": [
            null,
            "79 878"
          ],
          "values": [
            null,
            79878
          ],
          "key": "FOOD_BEV - NUMBER"
        },
        {
          "category": "Retail trade of country-specific tourism charasteristic goods - Number",
          "formatedValues": [
            null,
            "130 384"
          ],
          "values": [
            null,
            130384
          ],
          "key": "RETAIL - NUMBER"
        },
        {
          "category": "Tourism industries - Number",
          "formatedValues": [
            null,
            "288 907"
          ],
          "values": [
            null,
            288907
          ],
          "key": "IND - NUMBER"
        },
        {
          "category": "Passenger transport - Number",
          "formatedValues": [
            null,
            "31 860"
          ],
          "values": [
            null,
            31860
          ],
          "key": "TRANSPORT - NUMBER"
        },
        {
          "category": "Sports and recreation industry - Number",
          "formatedValues": [
            null,
            "13 060"
          ],
          "values": [
            null,
            13060
          ],
          "key": "SPORTS_REC - NUMBER"
        },
        {
          "category": "Road passenger transport - Number",
          "formatedValues": [
            null,
            "26 927"
          ],
          "values": [
            null,
            26927
          ],
          "key": "TRANSPORT_ROAD - NUMBER"
        },
        {
          "category": "Accommodation services for visitors - Number",
          "formatedValues": [
            null,
            "12 868"
          ],
          "values": [
            null,
            12868
          ],
          "key": "ACCOM_SVCS - NUMBER"
        },
        {
          "category": "Cultural industry - Number",
          "formatedValues": [
            null,
            "14 191"
          ],
          "values": [
            null,
            14191
          ],
          "key": "CULTURE - NUMBER"
        },
        {
          "category": "Travel agencies and other reservation services industry - Number",
          "formatedValues": [
            null,
            "6 666"
          ],
          "values": [
            null,
            6666
          ],
          "key": "TRVL_AGENT - NUMBER"
        },
        {
          "category": "Railways passenger transport - Number",
          "formatedValues": [
            null,
            "78"
          ],
          "values": [
            null,
            78
          ],
          "key": "TRANSPORT_RAIL - NUMBER"
        },
        {
          "category": "Railways passenger transport - Persons",
          "formatedValues": [
            "2 700",
            null
          ],
          "values": [
            2700,
            null
          ],
          "key": "TRANSPORT_RAIL - PERSONS"
        },
        {
          "category": "Cultural industry - Persons",
          "formatedValues": [
            "14 600",
            null
          ],
          "values": [
            14600,
            null
          ],
          "key": "CULTURE - PERSONS"
        },
        {
          "category": "Other industries - Persons",
          "formatedValues": [
            "20 500",
            null
          ],
          "values": [
            20500,
            null
          ],
          "key": "_O - PERSONS"
        },
        {
          "category": "Road passenger transport - Persons",
          "formatedValues": [
            "24 400",
            null
          ],
          "values": [
            24400,
            null
          ],
          "key": "TRANSPORT_ROAD - PERSONS"
        },
        {
          "category": "Sports and recreation industry - Persons",
          "formatedValues": [
            "27 100",
            null
          ],
          "values": [
            27100,
            null
          ],
          "key": "SPORTS_REC - PERSONS"
        },
        {
          "category": "Air passenger transport - Persons",
          "formatedValues": [
            "38 900",
            null
          ],
          "values": [
            38900,
            null
          ],
          "key": "TRANSPORT_AIR - PERSONS"
        },
        {
          "category": "Travel agencies and other reservation services industry - Persons",
          "formatedValues": [
            "42 600",
            null
          ],
          "values": [
            42600,
            null
          ],
          "key": "TRVL_AGENT - PERSONS"
        },
        {
          "category": "Other country-specific tourism industries - Persons",
          "formatedValues": [
            "55 500",
            null
          ],
          "values": [
            55500,
            null
          ],
          "key": "OTHER_COUNTRY_SPEC - PERSONS"
        },
        {
          "category": "Passenger transport - Persons",
          "formatedValues": [
            "66 000",
            null
          ],
          "values": [
            66000,
            null
          ],
          "key": "TRANSPORT - PERSONS"
        },
        {
          "category": "Accommodation services for visitors - Persons",
          "formatedValues": [
            "85 000",
            null
          ],
          "values": [
            85000,
            null
          ],
          "key": "ACCOM_SVCS - PERSONS"
        },
        {
          "category": "Retail trade of country-specific tourism charasteristic goods - Persons",
          "formatedValues": [
            "103 300",
            null
          ],
          "values": [
            103300,
            null
          ],
          "key": "RETAIL - PERSONS"
        },
        {
          "category": "Food and beverage serving industries - Persons",
          "formatedValues": [
            "203 100",
            null
          ],
          "values": [
            203100,
            null
          ],
          "key": "FOOD_BEV - PERSONS"
        },
        {
          "category": "Tourism industries - Persons",
          "formatedValues": [
            "597 100",
            null
          ],
          "values": [
            597100,
            null
          ],
          "key": "IND - PERSONS"
        },
        {
          "category": "Tourism - Persons",
          "formatedValues": [
            "617 600",
            null
          ],
          "values": [
            617600,
            null
          ],
          "key": "AGG_TOUR - PERSONS"
        }
      ],
      "symbolValues": [
        "Employment",
        "Enterprises"
      ]
    }
  ],
  getInitialOptions: null,
  options: {
    "chartClass": "VerticalSymbolChart",
    "base": {
      "width": 1618.3035888671875,
      "height": 494.238826751709,
      "padding": {
        "top": 20
      },
      "innerPadding": {
        "left": 20,
        "right": 20,
        "bottom": 10
      }
    },
    "axis": {
      "x": {
        "thickness": 0,
        "font": {
          "family": "'Segoe UI'",
          "color": "#575757",
          "size": 12
        },
        "linear": {
          "pivot": {
            "step": 1,
            "color": "white"
          }
        },
        "tick": {
          "thickness": 0,
          "size": 0
        },
        "format": { proc: d => d },
      },
      "y": {
        "thickness": 0,
        "font": {
          "family": "'Segoe UI'",
          "color": "#575757",
          "size": 12,
          "baseline": "ideographic"
        },
        "linear": {
          "pivot": {
            "step": 1,
            "color": "white"
          },
          "step": 1 // should be overridden
        },
        "padding": 10,
        "tick": {
          "thickness": 0
        },
        "format": {}
      },
    },
    "grid": {
      "x": {
        "color": "white",
        "baselines": [
          0
        ],
        "thickness": 0
      },
      "y": {
        "color": "white",
        "baselines": [
          0
        ]
      }
    },
    "background": {
      "color": "#DBEBF2"
    },
    "serie": {
      "colors": [
        "#8EA4B1"
      ],
      "overColors": [
        "#39617D"
      ],
      "highlightColors": [
        "#E73741",
        "#0F8FD9",
        "#993484",
        "#DF521E",
        "#719E24",
        "#E1B400",
        "#32A674",
        "#0B68AF"
      ],
      "baselineColors": [
        "#0B1E2D"
      ],
      "annotation": {
        "font": {
          "family": "'Segoe UI'",
          "size": 11
        },
        "format": {
          "datapoint": {}
        }
      },
      /*"symbol": {
        "markers": [
          {
            "style": {
              "stroke": "#39617D",
              "fill": "#39617D"
            }
          },
          {
            "style": {
              "stroke": "#39617D",
              "fill": "#DBEBF2"
            },
            "rotate": 45
          },
          {
            "style": {
              "stroke": "#39617D",
              "fill": "#39617D",
              "strokeWidth": 2
            }
          },
          {
            "style": {
              "stroke": "#39617D",
              "fill": "#DBEBF2"
            }
          },
          {
            "style": {
              "stroke": "#39617D",
              "fill": "#39617D"
            }
          }
        ],
        "markerDefaultSize": 60,
        "markerDefaultStrokeWidth": 1,
        "gapMinMaxColor": "#607D8B",
        "gapMinMaxThickness": 1,
        "gapAxisMinColor": "white",
        "gapAxisMinThickness": 1
      },*/
      "tooltip": {
        "font": {
          "family": "'Segoe UI'"
        }
      }
    },
    "map": {}
  },
};
