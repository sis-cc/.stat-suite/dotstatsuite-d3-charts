import { expect } from 'chai';
import { refineLinearDomain } from '../src/utils/linear-domain';

describe('refineLinearDomain tests', () => {
  it('uncanged test', () => {
    expect(refineLinearDomain([0, 5])).to.deep.equal([0, 5]);
  });
  it('3 decimals test', () => {
    expect(refineLinearDomain([10.0018, 10.0076])).to.deep.equal([10.001, 10.008]);
  });
  it('4 decimals test', () => {
    expect(refineLinearDomain([10.00181, 10.00364])).to.deep.equal([10.0018, 10.0037]);
  });
  it('big numbers test', () => {
    expect(refineLinearDomain([125000.00181, 225000.00364])).to.deep.equal([125000, 225001]);
  });
});
