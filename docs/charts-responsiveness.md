#Charts Responsiveness

## Data Display

For the specific Timeline, Horizontal Symbol and Vertical Symbol types of charts, if the size is considered too small, when some data series
are highlighted then only them will be displayed in the chart (if none, then all data is rendered normally).
The following size requirements are for the chart itself, not counting the size of the header and the footer.
The size thresholds can be customized through the chart options:
```
  options = {
    base: {
      minDisplayWidth: 100, //default
      minDisplayHeight: 300, //default
    }
  }
```
  - For both HorizontalSymbol and Timeline charts, the minDisplayHeight is taken into account
  - For VerticalSymbol chart, the minDisplayWidth is taken into account

## Axes and Grid Display

### Offset

In some cases, the text labels of an axis ticks ca be very large regarding the total size of a chart and tacking too much
space away from the data visualisation. That is why a configurable maximum ratio of the axis offset has been defined. In case the axis exceeds this size limit, it will be not rendered at all. The default value of this ratio is 0.5 (50%), and
can be customised through chart options:

```
  options = {
    axis: {
      x: {
        maxOffsetRatio: 0.25
      },
      y: {
        maxOffsetRatio: 1
      }
    }
  }
```

### Minimum Size

Regarding the type of axis, a minimum size is required in order to display the axis (a minimum width for a horizontal and
a minimum height for a vertical one). Here are the following default values:
  - For a numerical axis, the default minimum size required  is 100 px
  - For both ordinal axis and time axis, the default minimum size required is 300 px

This value can be customised through chart options:

```
  options = {
    axis: {
      x: {
        ordinal: {
          minDisplaySize: 350
        }
      },
      y: {
        linear: {
          minDisplaySize: 150
        }
      }
    }
  }
``` 

### Ticks Step Size

When generating numerical ticks for linear axis, based on the following configurable options `minTickSizeFactor` and `maxTickSizeFactor`, the engine will define a minimum and maximum step size between two ticks (will not be taken in considerations if a fixed step is given through customisation (1)).
These values can be customised in th options:
```
  options = {
    axis: {
      x: {
        linear: {
          minTickSizeFactor: 2,
          maxTickSizeFactor: 4
        }
      },
      y: {
        linear: {
          minTickSizeFactor: 1,
          maxTickSizeFactor: 3
        }
      }
    }
  }
```
Following this example, the vertical y axis, the minimal step size will be the height of ticks labels, and the maximal 3 times this height.
For the horizontal x axis, the minimal step size will be of 2 times the width of the first tick label, and the maximal 4 times this width. 


(1) For a complete list of chart customisations options see README.
