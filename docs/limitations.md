# limitations

## tooltip issue

If multiple charts are rendered in the same page, only the first one will have his tooltip display correctly.

## engine vulnerability to incorrect data input

Although this chart engine doesn't bother with incorrect format of data (wrong keys, serie of datapoints not passed through an array, ...), it doesn't however handle case of irrelevant value passed down in place of quantitative one expected.
Exemple: in a BarChart, passing a non-numerical value inside `y` key of a datapont, supposed to determine the height of the corresponding bar, will result of a bar with a `Nan` height.

## legends

For now, this engine doesn't handle the need to have a legend along with the chart. See getComputedOptions.

## ordinal axis labels

For ordinal axis labels it is needed to set some `proc: d => d` in respective option so that the engine can be able to display text label correctly

## ScatterChart annotations

As for ordinal axis labels, it is needed to set a `proc` method in `options.serie.annotations.format.datapoint` in order to get meaningful annotations displayed.

## responsiveness

### ScatterChart & LineChart annotations

For this charts, when some data is highlighted, annotations are displayed, giving data value with categorisation label.
But there is nothing to prevent those annotations to overlaping each other.

### axis tick labels

 - linear axis, only `options.axis.linear.minDisplaySize` and `options.axis.linear.maxOffsetRatio`thresholds to determine to render ticks and their label.
 Unlike for ordinal axis, nothing to prevent overlap of labels, vertically or horizontally. Instead of a process which display all or nothing, an enhancement in
 the ticks generation would be required.

 - time axis (timeline) same as linear axis, but without any threshold options.
