# getComputedOptions

  function called to every update of the engine, with computed options as props.

## usage
  ```javascript
  const logComputedOptions = (options) => {
    console.log(options);
  };

  BarChart(el, options, data, logComputedOptions);
  ```

  For now, this chart engine doesn't handle rendering of legends associated with some charts,
  this method can be used to get back all needed options to display those legends separately,
  with the insurrance to always be synchronised with the chart.
