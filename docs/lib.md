# documentation to use the chart engine as a lib

## building the lib

the engine is a living tool which is updated on a regular basis.  
in order to have the latest version, knowing how to build the lib is required.

> note: the version of the engine is accessible in the console (F12 in chrome)
> ![console](images/lib-console.png?raw=true)

1. get the latest source from the repository: `https://github.com/cis-itn-oecd/web-components`
1. checkout the branch you want to work on:
   * `master` is production-ready
   * `dev` is the latest prototype
1. go in the engine folder: `/lib/rcw-charts`
1. install the dependencies: `npm i`
1. use the automation tool to build the engine as a lib: `./node_modules/.bin/wce bundle --library`
1. the library and its sourcemaps are in `/lib/rcw-charts/dist`

## hello world usecase

> note: [source](../public/hello-world) of the usecase

the engine requires [D3](https://d3js.org/) and [lodash](https://lodash.com) to work.  
these libraries need to be loaded before loading the engine.

> note: read the `/lib/rcw-charts/package.json` for the compatible versions  
> ![console](images/lib-dependencies.png?raw=true)

overview of the API:
  * `rcwCharts` is exposed in the global namespace and has a constructor for each type of chart:  
    ![console](images/lib-types.png?raw=true)
  * a chart instance has 2 methods: `update(el, options, data)` and `destroy(el)`

> note: check the documentation of [options](options.md) and [data](data.md) for more details

```html
<!DOCTYPE html>
<html>
  <head>
    <title>hello world usecase</title>
    <meta charset="utf-8">
  </head>
  <body>
    <div id="root"></div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.16.1/lodash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.17/d3.min.js"></script>

    <script src="./rcw-charts.lib.3.11.0.js"></script>

    <script type="text/javascript">
      var el = document.getElementById('root');
      var options = { base: { width: 500, height: 300 } };
      var data = [{ datapoints: [
        {x: 1000, y: 15 },
        {x: 1001, y: -20 },
        {x: 1010, y: 10, highlightIndex: 0},
        {x: 1011, y: 40 },
        {x: 1100, y: 30 },
        {x: 1101, y: 25 },
        {x: 1110, y: 65, highlightIndex: 1},
        {x: 1111, y: 27 }
      ] }];

      var chart = new rcwCharts.BarChart(el, options, data);
    </script>
  </body>
</html>
```

when the index.html is loaded in a browser, a bar chart should be displayed:
![console](images/lib-hello-world.png?raw=true)

open the console (F12) and update the chart dimensions:
`chart.update(el, { base: { width: 200, height: 150 } }, data)`
![console](images/lib-update.png?raw=true)

## real world usecase

> note: [source](../public/real-world) of the usecase

the engine was designed to be flexible and capable to stick the oecd web charte.  
all features that were needed for this requirement have been implemented as an option.  
the engine has a lot of options but may miss some features.

in this usecase, the options have been set to stick the oecd web charte.  
the file `bar.js` contains these options and may be scary at first sight.  
think about them as a theme.

when the index.html is loaded in a browser, a bar chart should be displayed:
![console](images/lib-real-world.png?raw=true)
