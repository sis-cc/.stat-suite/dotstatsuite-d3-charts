# options

| name                                 | default                                  | type/domain                                                | description                                          |
| ------------------------------------ | ---------------------------------------- | ---------------------------------------------------------- | ---------------------------------------------------- |
| `base`                               | -                                        | `{}`                                                       | base options                                         |
| `base.width`                         | `(#12)`                                  | `<Integer>`                                                | width of the chart                                   |
| `base.height`                        | `(#12)`                                  | `<Integer>`                                                | height of the chart                                  |
| `base.margin`                        | `0`                                      | `<Integer>`                                                | margins (top, right, bottom, left) of the chart (#1) |
| `base.isAnnotated`                   | `false`                                  | `<Boolean>`                                                | serie annotations                                    |
| `base.padding`                       | `{top: 0, right: 0, bottom: 0, left: 0}` | `{}`                                                       | padding around base                                  |
| `base.innerPadding`                  | `{top: 0, right: 0, bottom: 0, left: 0}` | `{}`                                                       | padding around series                                |
| `background`                         | -                                        | `{}`                                                       | background options                                   |
| `background.color`                   | `#F2F2F2`                                | CSS                                                        | color of the background                              | 
| `axis`                               | -                                        | `{}`                                                       | axis options                                         |
| `axis.{x\|y}`                        | -                                        | `{}`                                                       | x or y axis options                                  |
| `axis.{x\|y}.color`                  | `black`                                  | CSS                                                        | color of the axis (line only)                        |
| `axis.{x\|y}.thickness`              | `1`                                      | `<Integer>`                                                | thickness of the axis (line only)                    |
| `axis.{x\|y}.orient`                 | `left`                                   | `[top\|right\|bottom\|left]`                               | orientation of the axis                              |
| `axis.{x\|y}.padding`                | `0`                                      | `<Integer>`                                                | padding of the axis (#4)                             |
| `axis.{x\|y}.maxOffsetRatio`         | `.5`                                    | `<Number>`                                                 | max ratio between axis offset total length (width for y, height for x) |
| `axis.{x\|y}.tick`                   | -                                        | `{}`                                                       | tick options                                         |
| `axis.{x\|y}.tick.size`              | `6`                                      | `<Integer>`                                                | length (width for y, height for x) of a tick         |
| `axis.{x\|y}.tick.color`             | `black`                                  | CSS                                                        | color of a tick                                      |
| `axis.{x\|y}.tick.thickness`         | `1`                                      | `<Integer>`                                                | thickness of a tick                                  |
| `axis.{x\|y}.font`                   | -                                        | -                                                          | font options                                         |
| `axis.{x\|y}.font.family`            | `sans-serif`                             | CSS                                                        | font family                                          |
| `axis.{x\|y}.font.size`              | `10`                                     | `<Integer>`                                                | font size                                            |
| `axis.{x\|y}.font.color`             | `black`                                  | CSS                                                        | font color                                           |
| `axis.{x\|y}.font.weight`            | `normal`                                 | CSS                                                        | font weight                                          |
| `axis.{x\|y}.font.rotation`          | `-45`                                    | `<Integer>`                                                | font rotation (#3)                                   |
| `axis.{x\|y}.format`                 | -                                        | -                                                          | format options                                       |
| `axis.{x\|y}.format.proc`            | `null`                                   | `<function>`                                               | proc to manually compute a format (#2)               |
| `axis.{x\|y}.format.pattern`         | `.0f`                                    | D3                                                         | pattern injected in `d3.format`                      |
| `axis.{x\|y}.format.isTime`          | `false`                                  | `<Boolean>`                                                | use `d3.time.format`                                 |
| `axis.{x\|y}.format.maxWidth`        | `null`                                   | `<Integer>`                                                | value to set a maximum to the width of the text (in pixels) |
| `axis.{x\|y}.ordinal.gap`            | `.3`                                     | `[0 -> 1]`                                                 | ratio for band gap                                   |
| `axis.{x\|y}.ordinal.padding`        | `.3`                                     | `[0 -> 1]`                                                 | space ratio around bands                             |
| `axis.{x\|y}.ordinal.minDisplaySize` | `300`                                    | `<Integer>`                                                | min size in pixel to display ordinal axis (width for x, height for y) |
| `axis.{x\|y}.linear.min`             | `0`                                      | `<Integer>`                                                | min value (#7)                                       |
| `axis.{x\|y}.linear.max`             | `0`                                      | `<Integer>`                                                | max value (#7)                                       |
| `axis.{x\|y}.linear.step`            | `0`                                      | `<Integer>`                                                | step that defines tick count (#8)                    |
| `axis.{x\|y}.linear.frequency`       | `year`                                   | `[year\|month\|week\|day\|hour\|minute\|second]`           | frequency used along with a step for timelines (#10) |
| `axis.{x\|y}.linear.pivot`           | -                                        | `{}`                                                       | pivot options                                        |
| `axis.{x\|y}.linear.pivot.value`     | `null`                                   | `<Integer>`                                                | pivot value, if null computed from min, max or data  |
| `axis.{x\|y}.linear.pivot.color`     | `black`                                  | CSS                                                        | color of pivot line                                  |
| `axis.{x\|y}.linear.pivot.thickness` | `2`                                      | `<Integer>`                                                | width of pivot line                                  |
| `axis.{x\|y}.linear.minDisplaySize`  | `100`                                    | `<Integer>`                                                | min size in pixel to display linear axis (width for x, height for y) |
| `grid`                               | -                                        | `{}`                                                       | grid options                                         |
| `grid.{x\|y}`                        | -                                        | `{}`                                                       | x or y grid options                                  |
| `grid.{x\|y}.color`                  | `black`                                  | CSS                                                        | color of a line                                      |
| `grid.{x\|y}.thickness`              | `1`                                      | `<Integer>`                                                | thickness of a line                                  |
| `grid.{x\|y}.baselines`              | `[]`                                     | `Array of <Integer>`                                       | grid lines considered as pivot line (#9)             |
| `serie`                              | -                                        | `{}`                                                       | serie options                                        |
| `serie.colors`                       | `['gray']`                               | `[CSS]`                                                    | colors of a serie                                    |
| `serie.overColors`                   | `['green']`                              | `[CSS]`                                                    | colors of a hovered serie                            |
| `serie.highlightColors`              | `['blue']`                               | `[CSS]`                                                    | colors of highlighted series                         |
| `serie.baselineColors`               | `['#263238']`                            | `[CSS]`                                                    | colors of baseline series                            |
| `serie.annotation`                   | -                                        | `{}`                                                       | annotation options                                   |
| `serie.annotation.font.family`       | `sans-serif`                             | CSS                                                        | font family                                          |
| `serie.annotation.font.size`         | `10`                                     | `<Integer>`                                                | font size                                            |
| `serie.annotation.font.weight`       | `normal`                                 | CSS                                                        | font weight                                          |
| `serie.annotation.margin`            | `2`                                      | `<Integer>`                                                | margin around annotation                             |
| `serie.annotation.format`            | -                                        | `{}`                                                       | annotation format options                            |
| `serie.annotation.format.datapoint`  | -                                        | `{}`                                                       | datapoint annotation format (cf axis.format)         |
| `serie.annotation.format.serie`      | -                                        | `{}`                                                       | serie annotation format (cf axis.format)             |
| `serie.annotation.display`           | `focus`                                  | `[never\|always\|highlight\|baseline\|focus]`              | annotation display mode (#6)                         |
| `serie.scatter`                      | -                                        | `{}`                                                       | scatter options                                      |
| `serie.scatter.markerRadius`         | `7`                                      | `<Integer>`                                                | radius of a marker                                   |
| `serie.scatter.areaIndex`            | `.1`                                     | `[0 -> 1]`                                                 | ratio that control annotation position on edges      |
| `serie.line.shadow`                  | `10`                                     | `<Integer>`                                                | shadow line for hovering more easily                 |
| `serie.line.thickness`               | `1`                                      | `<Integer>`                                                | thickness of a line                                  |
| `serie.line.marker`                  | -                                        | `{}`                                                       | marker options for a line                            |
| `serie.line.marker.shadow`           | `2`                                      | `<Integer>`                                                | shadow border of a marker                            |
| `serie.line.marker.shape`            | `circle`                                 | `[circle\|cross\|diamond\|square\|triangle-up\| triangle-down]` | shape of a marker                               |
| `serie.line.focused`                 | -                                        | `{}`                                                       | focused line options                                 |
| `serie.line.focused.thickness`       | `2`                                      | `<Integer>`                                                | thickness of a focused line                          |
| `serie.line.densityRatio`            | `32`                                     | `<Integer>`                                                | ratio for visibility of markers and annotations      |
| `serie.tooltip`                      | -                                        | `{}`                                                       | scatter options                                      |
| `serie.tooltip.display`              | `over`                                   | `[never\|over]`                                            | tooltip display mode                                 |
| `serie.tooltip.layout`               | `<proc>`                                 | `<function>`                                               | proc that render an html string (#5)                 |
| `serie.symbol`                       | -                                        | `{}`                                                       | symbol options                                       |
| `serie.symbol.markers`               | (#11)                                    | `[]`                                                       | set of symbol markers                                |
| `serie.symbol.markers[]`             | -                                        | `{}`                                                       | symbol marker entry                                  |
| `serie.symbol.markers[].size`        | -                                        | `<Integer>`                                                | size of the marker entry                             |
| `serie.symbol.markers[].path`        | -                                        | `<function>`                                               | path generator of a marker, given size as parameters |
| `serie.symbol.markers[].rotate`      | -                                        | `<Integer>`                                                | set the rotation angle of marker (in degrees)        |
| `serie.symbol.markers[].style`       | -                                        | `{}`                                                       | style options of a marker                            |
| `serie.symbol.markers[].style.fill`  | -                                        | CSS                                                        | color for the stroke of the marker                   |
| `serie.symbol.markers[].style.stroke` | -                                       | CSS                                                        | color to fill the marker                             |
| `serie.symbol.markers[].style.strokeWidth` | -                                  | `<Integer>`                                                | stroke-width of a marker                             |
| `serie.symbol.markerDefaultSize`     | `50`                                     | `<Integer>`                                                | default size for markers                             |
| `serie.symbol.markerDefaultStrokeWidth` | `1`                                   | `<Integer>`                                                | default stroke-width for markers                     |
| `serie.symbol.gapAxisMinColor`       | `red`                                    | CSS                                                        | color of line from axis to minimum value (X axis for vertical symbol chart, Y for horizontal symbol chart) |
| `serie.symbol.gapAxisMinThickness`   | `1`                                      | `<Integer>`                                                | thickness of line from axis to minimum value         |
| `serie.symbol.gapMinMaxColor`        | `#607D8B`                                | CSS                                                        | color of line from minimum value to maximum value    |
| `serie.symbol.gapAxisMinColor`       | `1`                                      | `<Integer>`                                                | thickness of line from minimum value to maximum value |
| `serie.symbol.highlightLineThickness` | `2`                                     | `<Integer>`                                                | thickness of an highlighted (or baselined) line (corresponding to the gap between minimum value and maximum value) |
| `serie.stacked`                      | -                                        | `{}`                                                       | stacked options                                      |
| `serie.stacked.strokeThickness`      | `1`                                      | `<Integer>`                                                | thickness of stroke delimitation of each bars        |
| `serie.stacked.strokeColors`         | `black`                                  | CSS                                                        | color of stroke delimitation of each bars            |

* (#1) compliant with the [d3 standard](https://bl.ocks.org/mbostock/3019563)
* (#2) `proc(datum) => datum.x%2 === 0 ? datum.x : 'odd values are odd...'`
* (#3) only used for x if labels are too long
* (#4) correlated to orient (left, right), only for y
* (#5) ``proc(serie, datum, color) => `<div style="${style}">X: ${datum.x}<br />Y: ${datum.y}</div>` ``
* (#6) focus is the aggregation of all focus modes (highlight, baseline)
* (#7) min and max values are overriden by data to avoid clamping and misleading charts
* (#8) if min is 0 and max is 100 with 20 step, ticks will be [0,20,40,60,80,100], a 0 step (default) is considered invalid and will be overriden by data
* (#9) pivot has an impact on all the computations, baselines have no impact. They are declarative, if they are drawn they have the same style as a pivot line.
* (#10) a time axis has the same behavior as a linear axis except that ticks are handle by the combination of a frequency and a step (ie every quarter is `month` and `3`)
* (#11) default markers:
```javascript
const markers = [
  {
    style: {
      stroke: '#607D8B',
      fill: '#607D8B'
    },
    path: (size) => {
      const r = Math.sqrt(size) / 2;
      return `M0,${r} A${r},${r} 0 1,1 0,${-r} A${r},${r} 0 1,1 0,${r} Z`;
    }
  },
  {
    style: {
      stroke: '#607D8B',
      fill: '#f2f2f2'
    },
    rotate: 45,
    path: (size) => {
      const r = Math.sqrt(size) / 2;
      return `M${-r},${-r} L${r},${-r} ${r},${r} ${-r},${r} Z`;
    }
  }
];
```

* (#12) in case of an absence of width and height provided through options, engine will search for them inside `el` DOM element. If non-founded, then they will be taken as`null`.

