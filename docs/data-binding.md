# data binding, accessible through annotations and tooltip

tooltip layout signature:
```javascript
function layout(serie, datum, color) {
  const style = `
    font-size: 10px;
    font-family: sans-serif;
    color: ${color};
    padding: 5px;
    border: 1px solid ${color};
    background: white;
    opacity: .8;
  `;
  return `<div style="${style}">X: ${datum.x}<br />Y: ${datum.y}</div>`;
}
```

## bar / row

- tooltip triggered by overing bars
- `datum` bind to plots exposed to tooltip layout call is the corresponding datapoint passed to the chart through `data`
- `serie` exposed to tooltip layout call is the first serie of datapoints send to the chart through `data`
- only quantitative value of the datapoint bind to annotation

## scatter

- tooltip triggered by overing plots
- `datum` bind to plots exposed to tooltip layout call is the corresponding datapoint passed to the chart through `data`
- `serie` exposed to tooltip layout call is the first serie of datapoints send to the chart through `data`
- all `datum` bind to annotation

Note: this means that it is required to set a relevant `proc` method in `options.serie.annotations.format.datapoint`
to have a meaningful annotation rendered (nothing in default).

## line / timeline

- tooltip triggered by overing line plots
- `datum` bind to plots exposed to tooltip layout call is the corresponding datapoint passed to the chart through `data`
- `serie` exposed to tooltip layout call is the corresponding line serie send to the chart through `data`
- only quantitative value of the datapoint bind to annotation

## hsymbol / vsymbol

- tooltip triggered by overing symbols
- `datum` bind to symbols, exposed to tooltip layout call:
```javascript
{
  x,
  y,
  symbolValue, // label matching the corresponding symbol definition
  marker // index matching the corresponding symbol definition
}
```
- `serie` exposed to tooltip layout call is the corresponding datapoint send through `data`
- no annotations

## stacked bar

- tooltip triggered by overing bars.
- `datum` bind to bars, exposed to tooltip layout call:
```javascript
  {
    x,
    y, //
    y0, //
    layerIndex, // index matching corresponding layer of the bar in layer Series
    layerLabel, // label categorizing corresponding layer of the bar
    height // quantitative value
  }
```
- `serie` exposed to tooltip layout call is `null`.
- no annotations