# data

## bar and row

```javascript
const data = [
  {  datapoints: [
      { x, y, highlightIndex, baselineIndex },
      ...
    ]
  }
];
```

For those charts, the engine expects one unique serie of datapoints, (the first occurence in the array will be the only one taken in account), each datapoint corresponding to a bar. The BarChart drawing vertical bars, y key of the datapoint is expected to be the quantitative data, x the caracterisation one, the X axis being ordinal. Opposite for RowChart, where bars are horizontals, x quantitative and y caracterisation. highlightedIndex and baselineIndex aren't mandatory, an absence of those keys will be the same as providing the default `-1` index value, corresponding of a non highlight or baseline coloring of the bar. Note that if both highlightIndex and baselineIndex are provided with both not `-1` value, the baselineIndex will prevail over the highlightIndex.

## scatter

```javascript
const data = [
  {  datapoints: [
      { x, y, highlightIndex, baselineIndex },
      ...
    ]
  }
];
```

As for Bar and Row for this chart the engine will expect one unique serie of datapoints. Here both x and y keys of a datapoint is meant to be quantitative, both x and y axes being linear, defining ordinary cartesian coordinate system for datapoints to be rendered as plots. Same mecanism for highlight and baseline indexes as Bar and Rows.

## line and timeline

```javascript
const data = [
  {  datapoints: [
      { x, y },
      ...
    ],
    category,
    highlightIndex,
    baselineIndex,
  },
  ...
];
```

Here for those charts, the engine expects several series of datapoints, each one corresponding to a line of the chart.
Like BarChart the y keys of the datapoints will be quantitative values for the points of a line, x keys the categorisation.
Here the highlight and baseline indexes will be expected at the serie level, being applied for the whole line, a category key is also present to categorize the all line, being accessible for tooltip and line annotation.

For timeline chart, x axis is a time axis and each x value must be a javascript Date value.

## hsymbool and vsymbol

```javascript
const data = [
  {  datapoints: [
      { category, values, highlightIndex, baselineIndex },
      ...
    ],
    symbolValues
  }
];
```

Here for those charts, the input is identical regarding HorzontalSymbolChart or VerticalSymbolChart.
An unique serie of datapoints expected, for each datapoints, a category key meant for categorisation, to x coordinate for vertical, y coordinate for horizontal. Inside values key, the engine expect an array o quantitative data,  each one used as the quantitative coordinate (y for vertical, x for horizontal) of a symbol. Here highlight and baseline indexes are applied for the all set of symbols of the datapoint. Here, inside the serie, an array of labels is expected through the symbolValues key, to categorise the different symbols.

## stacked bar

```javascript
const data = [
  {  datapoints: [
      { x, y, highlightIndex, baselineIndex },
      ...
    ],
    layerSeries: [
      { id, label },
      ...
  }
];
```

This chart is an extension of the BarChart. Here the y key of a datapoint is now expected to be an array of quantitative values, (like the values key in symbols). The serie will also expect, plus of datapoints, a layerSeries array, each one composed of and id and a label props, used tho categorise each bar on a same x level, binded along the data of a bar, so accessible for the tooltip.
