export default function selectors(target) {
  const prefix = 'rcw-chart-legend';

  target.selectors = {
    ...target.selectors,
    legend: `${prefix}_legend`,
    wrapper: `${prefix}_wrapper`,
    axis: `${prefix}_axis`,
    colors: `${prefix}_colors`
  }
}