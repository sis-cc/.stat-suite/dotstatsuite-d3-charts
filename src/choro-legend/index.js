import d3 from 'd3';

import accessors from './accessors';
import choroplethAccessors from '../choropleth/accessors';
import choroplethComputors from '../choropleth/computors';
import computors from './computors';
import eauors from './eauors';
import mapAccessors from '../map/accessors';
import selectors from './selectors';

import prepareOptions from '../utils/options';
import { noElError, prefix, setupElement, selector } from '../utils';

import * as R from 'ramda';

@eauors
@computors
@accessors
@selectors
@choroplethComputors
@choroplethAccessors
@mapAccessors
export default class ChoroplethLegend {
  constructor(el, options={}, data=[]) {
    this.options = prepareOptions(options, el);
    _constructor(
      el,
      this.options,
      data,
      {
        accessors: this.constructor.accessors,
        computors: this.constructor.computors,
        eauors: this.constructor.eauors,
        selectors: this.constructor.selectors
      }
    );
  }

  update(el, options={}, data=[]) {
    this.options = prepareOptions(options, el);
    _update(
      el,
      this.options,
      data,
      {
        accessors: this.constructor.accessors,
        computors: this.constructor.computors,
        eauors: this.constructor.eauors,
        selectors: this.constructor.selectors
      }
    );
  }
}

function _constructor(el, options, data, ors){

  noElError(el);

  d3.select(el)
    .call(setupElement, 'svg:svg', ors.selectors.legend)
    .select(selector(ors.selectors.legend))
    .call(setupElement, 'svg:g', ors.selectors.axis);

  _update(el, options, data, ors);
}

function _update(el, options, data, ors) {
  if (process.env.NODE_ENV !== 'production') {
    console.time(`${prefix} ChoroplethLegend#_update`);
  }

  const domain = ors.computors.domain(options, data);
  if (R.isNil(domain)
    || R.any(R.anyPass([R.isNil, v => isNaN(v)]))(domain)
    || R.head(domain) === R.last(domain)) {
    
    d3.select(el).select(selector(ors.selectors.legend)).attr({ width: 0, height: 0 });
    d3.select(el).selectAll(selector(ors.selectors.colors)).remove();
    d3.select(el).select(selector(ors.selectors.legend)).selectAll('.tick').remove();
    d3.select(el).select(selector(ors.selectors.legend)).selectAll('path').remove();
    if (process.env.NODE_ENV !== 'production') {
      console.timeEnd(`${prefix} ChoroplethLegend#_update`);
    }
    return;
  }

  const legend = d3.select(el)
    .select(selector(ors.selectors.legend))
    .call(ors.eauors.legendUpdate, options, ors.accessors);

  const axis = ors.computors.axis(options, data);

  const axisSvg = legend
    .select(selector(ors.selectors.axis))
    .call(ors.eauors.axisEnter, axis, options, ors.computors);

  const offset = ors.computors.axisOffset(axisSvg, options);

  axisSvg.call(ors.eauors.axisUpdate, options, offset, ors.computors);

  const colorsJoin = legend
    .selectAll(selector(ors.selectors.colors))
    .data(ors.computors.colorBars(options, data, offset));

  colorsJoin.enter().call(ors.eauors.colorEnter, ors.selectors);
  colorsJoin.call(ors.eauors.colorUpdate);
  colorsJoin.exit().remove();

  if (process.env.NODE_ENV !== 'production') {
    console.timeEnd(`${prefix} ChoroplethLegend#_update`);
  }
}