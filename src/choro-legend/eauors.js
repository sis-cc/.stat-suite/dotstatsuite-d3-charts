import { path, pathOr, prop } from 'ramda';
import { fontStyle, lineStyle, setupElement } from '../utils';

function legendUpdate(selection, options, accessors) {
  selection.attr({
    width: accessors.width(options),
    height: accessors.height(options)
  })
}

function axisEnter(selection, axis, options) {
  const legendOptions = pathOr({}, ['legend', 'choropleth'], options);
  selection.call(axis)
  selection.selectAll('path').style(lineStyle(prop('axis', legendOptions)));
  selection.selectAll('line').style(lineStyle(path(['axis', 'tick'], legendOptions)));
  selection.selectAll('text').style(fontStyle(path(['axis', 'font'], legendOptions)));
}

function axisUpdate(selection, options, offset, computors) {
  selection.attr('transform', computors.axisTranslate(offset, options));
}

function colorEnter(selection, selectors) {
  setupElement(selection, 'svg:rect', selectors.colors);
}

function colorUpdate(selection) {
  selection.attr({
    x: datum => datum.x,
    y: datum => datum.y,
    width: datum => datum.width,
    height: datum => datum.height
  })
  .style({
    fill: datum => datum.color
  });
}

export default function eauors(target){
  target.eauors = {
    ...target.eauors,
    axisEnter,
    axisUpdate,
    colorEnter,
    colorUpdate,
    legendUpdate,
  }
}