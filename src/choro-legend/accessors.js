import { path, pathOr } from 'ramda';
import { BOTTOM } from '../utils/constants';

export default function accessors(target) {
  target.accessors = {
    ...target.accessors,
    format: (options) => path(['legend', 'choropleth', 'axis', 'format'], options),
    height: (options) => pathOr(null, ['legend', 'choropleth', 'height'], options),
    orient: (options) => pathOr(BOTTOM, ['legend', 'choropleth', 'axis', 'orient'], options),
    margin: (options) => path(['legend', 'choropleth', 'margin'], options),
    tickSize: (options) => path(['legend', 'choropleth', 'axis', 'tick', 'size'], options),
    width: (options) => pathOr(null, ['legend', 'choropleth', 'width'], options)
  }
}
