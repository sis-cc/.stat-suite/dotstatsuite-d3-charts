import d3 from 'd3';
import * as R from 'ramda';
import { BOTTOM, LEFT, RIGHT, TOP } from '../utils/constants';
import { getElementDimensions, getFormat } from '../utils';


export default function computors(target) {
  target.computors = {
    ...target.computors,
    axisRange: (options) => {
      const orient = target.accessors.orient(options);
      const { bottom, left, right, top } = target.accessors.margin(options);
      if (orient === BOTTOM || orient === TOP) {
        return [left, target.accessors.width(options) - right];
      }
      else {
        return [top, target.accessors.height(options) - bottom];
      }
    },
    axisScale: (options, data) => (
      d3.scale.linear()
        .domain(target.computors.domain(options, data))
        .range(target.computors.axisRange(options))
    ),
    axisTicks: (options, data) => {
      const scale = target.computors.colorScale(options, data);
      const colors = target.accessors.colors(options);
      return R.pipe(
        R.map(color => scale.invertExtent(color)),
        R.unnest,
        R.uniq
      )(colors);
    },
    axisOffset: (selection, options) => {
      const orient = target.accessors.orient(options);
      if (orient === LEFT || orient === RIGHT) {
        return getElementDimensions(selection).width;
      }
      return getElementDimensions(selection).height;
    },
    axisTranslate: (offset, options) => {
      const orient = target.accessors.orient(options);
      const { bottom, left, right, top } = target.accessors.margin(options);
      const width = target.accessors.width(options);
      const height = target.accessors.height(options);
      if (orient === TOP) {
        return (`translate(0, ${offset + top})`);
      }
      else if (orient === BOTTOM) {
        return (`translate(0, ${height - offset - bottom})`)
      }
      else if (orient === LEFT) {
        return (`translate(${offset + left}, 0)`);
      }
      else if (orient === RIGHT) {
        return (`translate(${width - offset - right}, 0)`);
      }
    },
    axis: (options, data) => {
      const tickSize = target.accessors.tickSize(options);
      return d3.svg.axis()
        .scale(target.computors.axisScale(options, data))
        .orient(target.accessors.orient(options))
        .tickSize(tickSize, tickSize)
        .tickFormat(getFormat(target.accessors.format(options)))
        .tickValues(target.computors.axisTicks(options, data))
    },
    colorBars: (options, data, offset) => {
      const colorScale = target.computors.colorScale(options, data);
      const axisScale = target.computors.axisScale(options, data);
      const width = target.accessors.width(options);
      const height = target.accessors.height(options);
      const orient = target.accessors.orient(options);
      const { bottom, left, right, top } = target.accessors.margin(options);
      return R.map(
        color => {
          const [minExtent, maxExtent] =  colorScale.invertExtent(color);
          const colorRange = axisScale(maxExtent) - axisScale(minExtent);
          if(orient === LEFT || orient === RIGHT) {
            return ({
              height: colorRange,
              width: width - offset - right - left,
              y: axisScale(minExtent),
              x: left + (orient === LEFT ? offset : 0),
              color
            });
          }
          return ({
            width: colorRange,
            height: height - offset - top - bottom,
            x: axisScale(minExtent),
            y: top + (orient === TOP ? offset : 0),
            color
          })
        },
        target.accessors.colors(options)
      );
    },
  }
}