import d3 from 'd3';
import { is, isEmpty, isNil, length } from 'ramda';
import * as utils from '../utils/';
import LineChart from '../line';
import accessors from './accessors';
import computors from './computors';
import eauors from './eauors';
import { computeTimeSteps, computeMinorTicks } from '../utils/time';
import { getLinearAxisOptions } from '../utils/getAxisOptions';

@accessors
@computors
@eauors
export default class TimelineChart extends LineChart {
  constructor(el, options={}, data=[], getInitialOptions, getAxisOptions) {
    super(el, options, data, getInitialOptions);
    _update(
      el,
      this.options,
      data,
      {
        selectors: this.constructor.selectors,
        accessors: this.constructor.accessors,
        computors: this.constructor.computors,
        eauors: this.constructor.eauors
      },
      this.base,
      this.axis
    );
  }

  update(el, options={}, data=[], getInitialOptions, getAxisOptions) {
    super.update(el, options, data, getInitialOptions);
    _update(
      el,
      this.options,
      data,
      {
        selectors: this.constructor.selectors,
        accessors: this.constructor.accessors,
        computors: this.constructor.computors,
        eauors: this.constructor.eauors
      },
      this.base,
      this.axis,
      getAxisOptions
    );
  }
}

function _update(el, options, data, ors, base, axis, getAxisOptions) {
  if (isNil(axis)) {
    return;
  }
  const xAxisElement = d3.select(el).select(utils.selector(ors.selectors.xAxis));
  const ticks = xAxisElement.selectAll('g');
  const ticksData = ticks.data();
  if (isEmpty(ticksData)) {
    return;
  }
  const scale = axis.xAxis.scale();
  const range = scale.range();
  const domain = scale.domain();
  const { majorStep, minorStep } = computeTimeSteps({ range, domain, options: options.axis.x });
  const minorTicks = computeMinorTicks({ domain, majorStep, minorStep, size: length(ticksData), options: options.axis.x });
  ticks.call(ors.eauors.minorTicksUpdate, minorTicks, options.axis.x);

  if (is(Function, getAxisOptions)) {
    const yAxisoptions = getLinearAxisOptions(axis.yAxis, options.axis.y);
    getAxisOptions({
      computedMinY: yAxisoptions.min,
      computedMaxY: yAxisoptions.max,
      computedStepY: yAxisoptions.step,
      computedPivotY: yAxisoptions.pivot,
    });
  }
};