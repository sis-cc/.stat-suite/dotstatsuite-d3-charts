import d3 from 'd3';
import * as R from 'ramda';

function minorTicksUpdate(selection, minorTicks, options) {
  selection.each(function(d) {
    const element = d3.select(this);
    if (R.includes(d, minorTicks)) {
      element.select('line').attr('y2', R.path(['tick', 'minorSize'], options));
      element.select('line').style({ 'stroke-width': R.path(['tick', 'minorThickness'], options) });
      element.select('text').attr({ 'display': 'none' });
    }
    else {
      element.select('line').attr('y2', R.path(['tick', 'size'], options));
      element.select('text').attr({ 'display': 'block' });
    }
  })
}

export default function eauors(target) {
  target.eauors = {
    ...target.eauors,
    minorTicksUpdate,
  };
}
