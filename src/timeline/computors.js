import * as R from 'ramda';
import { computeTimeDomain, computeTimeScale, computeTimeAxis } from '../utils/computors';

export default function computors(target) {
  target.computors = {
    ...target.computors,
    xDomain: (options, data, range) => {
      return computeTimeDomain(
        options.axis.x,
        target.computors.data(data),
        target.accessors.x,
        range
      );
    },
    xScale: (options, data, offset, annotationOffset) => {
      const tickOffset = target.computors.xTickOffset(options, data);
      const range = target.computors.xRange(options, offset, tickOffset, annotationOffset);
      return computeTimeScale(
        range,
        target.computors.xDomain(options, data, range)
      );
    },
    xAxis: computeTimeAxis,
    linesData: (serie, options) => {
      const { frequency, step } = R.path(['axis', 'x', 'linear'], options);
      const interval = R.has(frequency, d3.time) ? R.prop(frequency, d3.time) : d3.time.year;
      const datapoints = serie.datapoints;
      const _split = R.reduce((acc, dp) => {
        const onGoingLine = R.last(acc);
        if (R.isNil(dp.y)) {
          return R.isEmpty(onGoingLine) ? acc : R.append([], acc);
        }
        if (R.isEmpty(onGoingLine)) {
          return R.over(R.lensIndex(-1), R.append(dp))(acc);
        }
        const lastDp = R.last(onGoingLine);
        const intervalRange = interval.range(
          target.accessors.x(lastDp),
          target.accessors.x(dp),
          step
        );
        return R.length(intervalRange) > 1
          ? R.append([dp], acc)
          : R.over(R.lensIndex(-1), R.append(dp))(acc);
      }, [[]], datapoints);
      const split = R.isEmpty(R.last(_split)) ? R.dropLast(1, _split) : _split;
      return R.map(datapoints => ({ ...serie, datapoints }), split);
    }
  }
}
