import d3 from 'd3';
import { is, length, map, max } from 'ramda';
import {  getRangeDelta } from '../utils';

const getTimeX = datum => is(String, datum.x) ? new Date(datum.x) : datum.x

export default function accessors(target) {
  target.accessors = {
    ...target.accessors,
    x: getTimeX,
    xTicks: (_, axis) => {
      return axis.xAxis.scale().ticks()
    },
    isCondensed: (options, serie, scale) => {
      // fastest way to handle x accessor override is to duplicate here
      const datapoints = target.accessors.datapoints(serie);
      const datapointsCount = length(datapoints);
      const datapointsXPositions = map(
        dp => scale(getTimeX(dp)),
        datapoints
      );
      const datapointsXExtent = d3.extent(datapointsXPositions);
      const rangeDelta = getRangeDelta(datapointsXExtent);
      const ratio = rangeDelta / max(1, datapointsCount);
      return ratio < options.line.densityRatio;
    }
  };
}
