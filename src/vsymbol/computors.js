import { filter, flatten, get, map, nth, size } from 'lodash';
import { computeOrdinalScale, computeOrdinalAxis } from '../utils/computors';

export default function computors(target) {
  target.computors = {
    ...target.computors,
    data: (data) => {
      return filter(flatten(
        map(
          target.accessors.data(data),
          (serie) => (
            map(
              serie.values,
              (value)  => (
                {
                  y: value,
                  x: get(serie, 'category', ''),
                  baselineIndex: get(serie, 'baselineIndex', -1),
                  highlightIndex: get(serie, 'highlightIndex', -1)
                }
              )
            )
          )
        )
      ), d => d.y !== null);
    },
    computeOrdinalFocus: (data) => map(
      target.accessors.data(data),
      datapoint => ({
        baselineIndex: get(datapoint, 'baselineIndex', -1),
        highlightIndex: get(datapoint, 'highlightIndex', -1)
      })
    ),
    computeMarkers: (symbolValues) => (datum) => filter(
      map(
        datum.values,
        (v, i) => ({
          baselineIndex: get(datum, 'baselineIndex', -1),
          highlightIndex: get(datum, 'highlightIndex', -1),
          formatedValue: nth(get(datum, 'formatedValues', []), i),
          y: v,
          x: datum.category,
          marker: i,
          symbolValue: get(symbolValues, `[${i}]`, '')
        })
      ),
      d => d.y !== null
    ),
    xValues: data => map(target.accessors.data(data), target.accessors.x),
    xScale: (options, data, offset) => {
      return computeOrdinalScale(
        options.axis.x.ordinal,
        target.computors.xRange(options, offset),
        target.computors.xValues(data)
      );
    },
    xAxis: (scale, pivot, options) => computeOrdinalAxis(scale, options)
  };
}