import { get, size } from 'lodash';
import * as utils from '../utils';

export default function accessors(target) {
  target.accessors = {
    ...target.accessors,
    data: (data) => get(data, '[0].datapoints', []),
    symbolValues: (data) =>get(data, '[0].symbolValues', []),
    marker: (options, datum) => {
      const markers = options.symbol.markers;
      const index = datum.marker % size(markers);
      return markers[index];
    },
    markerColor: (options, keyword, datum) => {
      const marker = target.accessors.marker(options.serie, datum);
      const color = get(marker, `style.${keyword}`, null);
      return ((color === null) || (target.accessors.isFocused(datum))) ? utils.getColor(options.serie, target.accessors.focusData(datum)) : color;
    },
    markerStrokeWidth : (options, datum) => {
      const marker = target.accessors.marker(options.serie, datum);
      const defaultSW = get(options, 'serie.symbol.markerDefaultStrokeWidth');
      return get(marker, 'style.strokeWidth', defaultSW);
    },
    markerRotate: (options, datum) => {
      const marker = target.accessors.marker(options.serie, datum);
      return get(marker, 'rotate', 0);
    },
    yRange: (base, axis) => base.xScale.rangeExtent(),
    xTicks: (base, axis) => axis.xAxis.scale().domain(),
  };
}