import d3 from 'd3';
import { head, min, max } from 'lodash';
import { is } from 'ramda';

import BackgroundChart from '../background';

import * as utils from '../utils/';

import selectors from './selectors';
import accessors from './accessors';
import computors from './computors';
import eauors from './eauors';
import { getLinearAxisOptions } from '../utils/getAxisOptions';

@selectors
@computors
@accessors
@eauors
export default class VerticalSymbolChart extends BackgroundChart {
  static ors = ['selectors', 'accessors', 'computors', 'eauors'];

  constructor(el, options={}, data=[], getInitialOptions, getAxisOptions) {
    const datapoints = VerticalSymbolChart.computors.data(data);
    //maybe not ideal, but since override of computors.data from BaseChart doesn't work ...
    super(el, options, [{ datapoints }], getInitialOptions);
    _contructor(
      el,
      this.options,
      data,
      {
        selectors: this.constructor.selectors,
        accessors: this.constructor.accessors,
        computors: this.constructor.computors,
        eauors: this.constructor.eauors
      },
      this.base,
      this.axis
    );
  }

  update(el, options={}, data=[], getInitialOptions, getAxisOptions) {
    const datapoints = VerticalSymbolChart.computors.data(data);
    super.update(el, options, [{ datapoints }], getInitialOptions);
    _update(
      el,
      this.options,
      data,
      {
        selectors: this.constructor.selectors,
        accessors: this.constructor.accessors,
        computors: this.constructor.computors,
        eauors: this.constructor.eauors
      },
      this.base,
      this.axis,
      getAxisOptions
    );
  }
}

function _contructor(el, options, data, ors, base, axis) {

  utils.noElError(el);

  d3.select(el)
    .select(utils.selector(ors.selectors.base))
    .call(utils.setupElement, 'svg:g', ors.selectors.serie);

  _update(el, options, data, ors, base, axis);

}

function _update(el, options, data, ors, base, axis, getAxisOptions) {
  if (process.env.NODE_ENV !== 'production') {
    console.time(`${utils.prefix} VerticalSymbolChart#_update`);
  }

  utils.noElError(el);
  if (utils.noDataError(ors.accessors.data(data), '\t\t\t\t', 'VerticalSymbolChart')) {
    d3.select(el)
      .selectAll(utils.selector(ors.selectors.symb)).remove();
    return;
  }
  
  const scales = {
    xScale: ors.accessors.xScale(base, axis),
    yScale: ors.accessors.yScale(base, axis)
  };

  const symbolValues = ors.accessors.symbolValues(data);
  //dimension value name matching the symbol, for the tooltip ...

   const symbJoin = d3.select(el)
    .select(utils.selector(ors.selectors.serie))
    .selectAll(utils.selector(ors.selectors.symb))
    .data(ors.accessors.data(data)) //bound to the ordinal series of datapoints
  symbJoin.enter().call(ors.eauors.symbEnter, options.serie, ors.selectors.symb);
  symbJoin.exit().remove();

  const axisCoord = min(scales.yScale.domain());
  //needed for the following lines,
  //get the coordinate of the ordinalAxis from the linear,
  //the x of yAxis in horizontal, the y of xAxis in vertical,
  //->min value of linear scale domain

  const gapAxisMinJoin = symbJoin //line gap between ordinal axis and min values
    .selectAll(utils.selector(ors.selectors.gapAxisMin))
    .data(data => [data])
  gapAxisMinJoin.enter().call(ors.eauors.gapAxisMinEnter, ors.selectors);
  gapAxisMinJoin.call(ors.eauors.gapAxisMinUpdate, options, ors.accessors, scales, axisCoord);
  gapAxisMinJoin.exit().remove();

  const gapMinMaxJoin = symbJoin //line gap between min and max values
    .selectAll(utils.selector(ors.selectors.gapMinMaxLine))
    .data(data => [data])
  gapMinMaxJoin.enter().call(ors.eauors.gapMinMaxEnter, ors.selectors);
  gapMinMaxJoin.call(ors.eauors.gapMinMaxUpdate, options, ors.accessors, scales);
  gapMinMaxJoin.exit().remove();

  // these two gaps Join annoys me a little => two data join identical
  //didn't find a way to build the two lines inside an unique dataJoin;

  const markerJoin = symbJoin
    .selectAll(utils.selector(ors.selectors.marker))
    .data(ors.computors.computeMarkers(symbolValues));
  markerJoin.enter().call(ors.eauors.markerEnter, options, ors.selectors, ors.accessors, symbJoin, base.hideTooltip);
  markerJoin.call(ors.eauors.markerUpdate, options, ors.accessors, scales, base.showTooltip);
  markerJoin.exit().remove();

  const xAxisLabelJoin = d3.select(el)
    .select(utils.selector(ors.selectors.xAxis))
    .selectAll('text')
    .data(ors.computors.computeOrdinalFocus(data));
  xAxisLabelJoin.call(ors.eauors.xAxisLabelUpdate, options.serie, ors.accessors);

  if (is(Function, getAxisOptions)) {
    const yAxisoptions = getLinearAxisOptions(axis.yAxis, options.axis.y);
    getAxisOptions({
      computedMinY: yAxisoptions.min,
      computedMaxY: yAxisoptions.max,
      computedStepY: yAxisoptions.step,
      computedPivotY: yAxisoptions.pivot
    });
  }
  if (process.env.NODE_ENV !== 'production') {
    console.timeEnd(`${utils.prefix} VerticalSymbolChart#_update`);
  }
}