import { get, head } from 'lodash';
import { HIGHLIGHT, BASELINE } from '../utils/constants';

export default function accessors(target) {
  target.accessors = {
    ...target.accessors,
    layerSeries: data => get(head(data), 'layerSeries', []),
    rectStrokeColor: options => get(options, 'stacked.strokeColor', null),
    rectStrokeThickness: options => get(options, 'stacked.strokeThickness', null),
    colorCoeff: (datum, options) => {
      const isFocused = target.accessors.isFocused(datum);
      const coeffKey = isFocused ? 'focusColorCoeff' : 'colorCoeff';
      return get(options, `stacked.layerSeries[${datum.layerIndex}].${coeffKey}`, null);
    },
    focusData: datum => {
      if (target.accessors.isABaseline(datum)) 
        return { index: target.accessors.baselineIndex(datum), type: BASELINE };
      else if (target.accessors.isHighlighted(datum))
        return { index: target.accessors.highlightIndex(datum), type: HIGHLIGHT };
      return { index: get(datum, 'layerIndex', 0) };
    }
  };
}