import RowChart from '../row';
import accessors from './accessors';
import computors from './computors';
import eauors from './eauors';

import * as utils from '../utils/';

@accessors
@computors
@eauors
export default class StackedRowChart extends RowChart {
  constructor(el, options={}, data=[], getInitialOptions, getAxisOptions) {
    const _data = StackedRowChart.computors.stackedData(data);
    const _options = StackedRowChart.computors.computeOptions(options, data);
    super(el, _options, _data, getInitialOptions, getAxisOptions);
    _constructor(
      el,
      this.options,
      data,
      {
        selectors: this.constructor.selectors,
        accessors: this.constructor.accessors,
        computors: this.constructor.computors,
        eauors: this.constructor.eauors
      }
    )
  }

  update(el, options={}, data=[], getInitialOptions, getAxisOptions) {
    const _data = StackedRowChart.computors.stackedData(data);
    const _options = StackedRowChart.computors.computeOptions(options, data);
    super.update(el, _options, _data, getInitialOptions, getAxisOptions);
    _update(
    el,
      this.options,
      data,
      {
        selectors: this.constructor.selectors,
        accessors: this.constructor.accessors,
        computors: this.constructor.computors,
        eauors: this.constructor.eauors
      }
    )
  }
}

function _constructor(el, options, data, ors){
  utils.noElError(el);
  _update(el, options, data, ors);
}

function _update(el, options, data, ors){
  
  const yAxisLabelJoin = d3.select(el)
    .select(utils.selector(ors.selectors.yAxis))
    .selectAll('text')
    .data(ors.computors.nonStackedData(data));
  yAxisLabelJoin.call(ors.eauors.yAxisLabelUpdate, options.serie, ors.accessors);

  // multi bars per serie caused a bad data binding in row chart, this is needed to have a good label serie coloring ...
}