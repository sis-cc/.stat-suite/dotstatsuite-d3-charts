import d3 from 'd3';
import { concat, filter, flatten, get, head, map, nth, reduce, size } from 'lodash';
import compose from 'lodash.compose';
import { NEVER } from '../utils/constants';

export default function computors(target) {
  target.computors = {
    ...target.computors,
    computeOptions: (options, data) => ({
      ...options,
      axis: {
        ...get(options, 'axis', {}),
        linear: {
          ...get(options, 'axis.linear', {}),
          pivot: {
            ...get(options, 'axis.linear.pivot', {}),
            value: 0
          }
        }
      },
      serie: {
        ...get(options, 'serie', {}),
        annotation: {
          ...get(options, 'serie.annotation', {}),
          display: NEVER
        },
        stacked: {
          ...get(options, 'serie.stacked', {}),
          layerSeries: target.accessors.layerSeries(data),
        }
      }
    }),
    /*
      Little trick here, d3.layout.stack only works with y value of a datapoint,
      so here x and y are inverted and reinverted after stack computation.
    */
    layerData: dataValidator => data => map(
      target.accessors.layerSeries(data),
      (layer, index) => ({
        datapoints: map(
          target.accessors.data(data),
          datapoint => {
            const x = get(datapoint, `x[${index}]`, 0);
            return ({
              ...datapoint,
              y: dataValidator(x) ? x : 0,
              x: datapoint.y,
              formater: nth(get(datapoint, 'formaters', []), index),
              layerIndex: index,
              layerLabel: get(layer, 'label', null)
            })
          }
        )
      })
    ),
    stackData: data => (
      d3.layout.stack()
      .values(d => d.datapoints)
      .out((d, y0, y) => {
        d.y = d.x;
        d.x = y + y0; //needed for computing xDomain
        d.width = y;
        d.x0 = y0;
      })
      (data)
    ),
    nonStackedData: data => flatten(map(data, serie => get(serie, 'datapoints', []))),
    // need for axis Labels, see index ...
    stackedNegativeData: data => compose(
      target.computors.stackData,
      target.computors.layerData(d => d < 0)
    )(data),
    stackedPositiveData: data => compose(
      target.computors.stackData,
      target.computors.layerData(d => d > 0)
    )(data),
    stackedData: data => concat(
      target.computors.stackedPositiveData(data),
      target.computors.stackedNegativeData(data)
    ),
    data: data => filter(
      flatten(map(data, serie => get(serie, 'datapoints', []))),
      dp => dp.width
    )
    /* 
    even if this data computors is not taken in account in base,
    it doesn't perturb the good data process, but it will be taken in account inside
    bar and will prevent overloading the dom and d3 processes with null bars (minimum the half of the datapoints generated ...)
    */
  }
}