import { map } from 'lodash';

import { computeOrdinalScale, computeOrdinalAxis } from '../utils/computors';

export default function computors(target) {
  const annotationData = target.computors.annotationData;
  target.computors = {
    ...target.computors,
    yValues: data => map(target.computors.data(data), target.accessors.y),
    yScale: (options, data, offset) => {
      return computeOrdinalScale(
        options.axis.y.ordinal,
        target.computors.yRange(options, offset),
        target.computors.yValues(data)
      );
    },
    yAxis: (scale, pivot, options) => computeOrdinalAxis(scale, options),
    annotationData: (options, scale, datum) => {
      const limitWidth = scale.rangeBand();
      if (limitWidth < options.minWidth) {
        return [];
      }
      return annotationData(options.display, scale);
    }
  };
}