import { get, head } from 'lodash';
import * as utils from '../utils/';

export default function accessors(target) {
  target.accessors = {
    ...target.accessors,
    data: data => get(head(data), 'datapoints', []),
    yTicks: (base, axis) => axis.yAxis.scale().domain(),
    xRange: (base, axis) => axis.yAxis.scale().rangeExtent(),
    annotation: (options, datum) => utils.getFormat(options.format.datapoint)(datum.x),
  };
}
