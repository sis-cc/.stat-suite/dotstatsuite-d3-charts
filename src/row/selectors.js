export default function selectors(target) {
  const chart = target.selectors.chart;

  target.selectors = {
    ...target.selectors,
    serie: `${chart}__serie`,
    row: `${chart}__row`,
    rect: `${chart}__rect`,
    annotation: `${chart}__annotation`
  };
}
