import {
  setupElement, getColor, getOverColor, fontStyle, getElementDimensions, fontBaselineOffset
} from '../utils/';
import * as C from '../utils/constants';
import { curry, gt, round, min, max } from 'lodash';

function rowEnter(selection, options, selector) {
  setupElement(selection, 'svg:g', selector);
}

function rowUpdate(selection, options, accessors) {
  selection.style({ fill: datum => getColor(options, accessors.focusData(datum)) });
}

export function rectWidth(pivot, scale, accessor, datum) {
  return Math.abs(scale(accessor(datum)) - scale(pivot));
}

export function rectOrigin(pivot, scale, accessor, datum) {
  return scale(Math.min(accessor(datum), pivot));
}

export function rectOver(options, accessors, datum) {
  d3.select(this.parentNode).style({ fill: datum => getOverColor(options, accessors.focusData(datum)) });
}

export function rectOut(options, accessors, datum) {
  d3.select(this.parentNode).style({ fill: datum => getColor(options, accessors.focusData(datum)) });
}

function rectEnter(selection, options, selectors, accessors, hideTooltip) {
  setupElement(selection, 'svg:rect', selectors.rect)
    .on('mouseover', curry(rectOver)(options, accessors))
    .on('mouseout', curry(rectOut)(options, accessors))
    .on('mouseleave', curry(hideTooltip));
}

function rectUpdate(selection, options, accessors, { xScale, yScale }, { xPivot, yPivot }, showTooltip) {
  selection.attr({
    width: curry(rectWidth)(xPivot, xScale, accessors.x),
    height: yScale.rangeBand(),
    x: curry(rectOrigin)(xPivot, xScale, accessors.x),
    y: datum => yScale(accessors.y(datum))
  })
  .on('mousemove', function(datum) {
    const serie = d3.select(this.parentNode.parentNode).datum();
    showTooltip(options.serie, { xScale, yScale }, serie, datum);
  });
}

export function annotationAnchor(pivot, scale, accessor, datum) {
  return gt(scale(accessor(datum)), scale(pivot)) ? C.TEXT_START : C.TEXT_END;
}

export function annotationAlign(scale, accessor, datum) {
  return scale(accessor(datum)) + scale.rangeBand();
}

function annotationEnter(selection, options, selector) {
  setupElement(selection, 'svg:text', selector);
}

function annotationBaseline(pivot, scale, accessor, position, datum) {
  switch(position) {
    case 'left':
      return rectOrigin(pivot, scale, accessor, datum);
    case 'right':
      return rectOrigin(pivot, scale, accessor, datum) + rectWidth(pivot, scale, accessor, datum);
    default:
      return null;
  }
}

function rangeLimits(options, scale, position) {
  const range = options.axis.y.orient === C.RIGHT ? [0, max(scale.range()) + options.base.innerPadding.right]
    : [min(scale.range()) - options.base.innerPadding.left, options.base.width];
  // take this instead of just scale.range() to be only limited by Y axis offset and not innerpaddings ...
  switch(position) {
    case 'left':
      return range;
    case 'right':
      return [range[1], range[0]];
    default:
      return null;
  }
}

function annotationWidthOffset(position, adjustment, textWidth, rectWidth, margin) {
  if (position === 'left') {
    switch (adjustment) {
      case 'adjacent':
        return -1 * margin;
      case 'inner':
        return margin + textWidth;
      case 'opposite':
        return margin + textWidth + rectWidth;
      default:
        return 0;
    }
  }
  else if (position === 'right') {
    switch (adjustment) {
      case 'adjacent':
        return margin;
      case 'inner':
        return -1 * margin - textWidth;
      case 'opposite':
        return -1 * margin - textWidth - rectWidth;
      default:
        return 0;
    }
  }
  return 0;
}

function hideAnnotation(selection) {
  selection.style({ display: C.NOT_DISPLAYED });
}

function annotationAdjustment(selection, options, accessors, scale, pivot, maxHeight, datum, position) {
  //const range = scale.range();
  const textHeight = fontBaselineOffset(selection, C.TEXT_BELOW);
  const textWidth = getElementDimensions(selection).width;
  const margin = options.serie.annotation.margin;
  const [adjRangeLimit, opRangeLimit] = rangeLimits(options, scale, position);
  const recWidth = rectWidth(pivot, scale, accessors.x, datum);
  const baseline = annotationBaseline(pivot, scale, accessors.x, position, datum);
  const oppositeBaseline = (position === 'left') ? (baseline + recWidth) : baseline - recWidth;
  const diffHeight = maxHeight - textHeight;
  let adjustment = null;
  if (Math.abs(adjRangeLimit - baseline) > margin + textWidth) {
    adjustment = 'adjacent';
  }
  else if (recWidth > margin + textWidth) {
    adjustment = 'inner';
  }
  else if (Math.abs(opRangeLimit - oppositeBaseline) > margin + textWidth) {
    adjustment = 'opposite';
  }
  if (!adjustment) {
    return hideAnnotation(selection);
  }
  const wOffset = annotationWidthOffset(position, adjustment, textWidth, recWidth, margin);
  const hOffset = -1 * round(diffHeight / 2); 
  selection.attr({ transform: `translate(${wOffset}, ${hOffset})`});
  if (adjustment === 'inner') {
    selection.style({ fill: options.serie.annotation.font.innerColor });
  }
}

function getAnnotationsHeight(selection) {
  // unlike bar, here we don't need to map over all annotations displayed, height equal for all
  const firstNode = selection.node();
  return firstNode ? fontBaselineOffset(d3.select(firstNode), C.TEXT_BELOW) : 0;
}

function annotationUpdate(selection, options, accessors, { xScale, yScale }, { xPivot, yPivot }) {
  // see annotationUpdate in BarChart for explanations ...
  function annotationDefault(datum) {
    let position = null;
    const _selection = d3.select(this);
    if (gt(xScale(accessors.x(datum)), xScale(xPivot))) {
      position = 'right';
    }
    else {
      position = 'left';
    }
    _selection
      .text(curry(accessors.annotation)(options.serie.annotation))
      .attr({
        x: curry(annotationBaseline)(xPivot, xScale, accessors.x, position),
        y: curry(annotationAlign)(yScale, accessors.y)
      })
      .style({
        ...fontStyle(options.serie.annotation.font),
        display: C.DISPLAYED,
        'text-anchor': curry(annotationAnchor)(xPivot, xScale, accessors.x)
      });
      annotationAdjustment(_selection, options, accessors, xScale, xPivot, yScale.rangeBand(), datum, position);
  }
  selection.each(annotationDefault);
  const annotationsHeight = getAnnotationsHeight(selection);
  if (annotationsHeight > yScale.rangeBand()) {
    hideAnnotation(selection);
  }
}

export default function eauors(target) {
  target.eauors = {
    ...target.eauors,
    rowEnter,
    rowUpdate,
    rectEnter,
    rectUpdate,
    annotationEnter,
    annotationUpdate
  };
}
