import * as R from 'ramda';
import * as utils from '.';
import { LEFT, RIGHT } from './constants';
const FEW_TICKS_FACTORS = [1, 1.5, 2, 3, 4, 5];
const MANY_TICKS_FACTORS = [1, 1.5, 2, 2.5, 5];
const TICKS_COUNT_THRESHOLD = 7;
const SIZE_THRESHOLD = 150;

export const factorizeNegativeScale = (value) => {
  let factor = value;
  let powerScale = 1;
  while (Math.abs(factor) < 1) {
    factor = factor * 10;
    powerScale = powerScale / 10;
  }
  return ({ factor, powerScale });
}

const factorizePositiveScale = (value) => {
  let factor = value;
  let powerScale = 1;
  while (Math.abs(factor) > 10) {
    factor = factor / 10;
    powerScale = powerScale * 10;
  }
  return ({ factor, powerScale });
}

export const factorize = (value) => R.pipe(
  factorizeNegativeScale,
  R.when(
    R.propEq('powerScale', 1),
    ({ factor }) => factorizePositiveScale(factor)
  )
)(value);

export const computeReducedStep = ({ domainDelta }) => {
  const { factor, powerScale } = factorize(domainDelta);
  const step = Math.floor(factor) * powerScale;
  return ({
    step: step === domainDelta ? step / 2 : step,
    powerScale
  });
};

export const computeReducedTicks = ({ domainDelta,  min }) => {
  const { step } = computeReducedStep({ domainDelta });
  const rest = domainDelta - step;
  const { powerScale } = factorize(domainDelta - step);
  const minTick = Math.ceil(min / powerScale) * powerScale;
  return [minTick, minTick + step];
};

export const computeStepLimits = ({ domainDelta, domain, options }) => {
  const fontSize = R.path(['font', 'size'], options);
  const orient = options.orient;
  const isVertical = R.includes(orient, [LEFT, RIGHT]);
  const { minTickSizeFactor, maxTickSizeFactor } = options.linear;
  if (isVertical) {
    return ({ min: fontSize * minTickSizeFactor, max: fontSize * maxTickSizeFactor });
  }
  const charWidth = Math.ceil(fontSize * 0.75);
  const { factor, powerScale } = factorizeNegativeScale(domainDelta);
  const _powerScale = factor > 5 ? powerScale : powerScale / 10;
  return R.pipe(
    R.map(
      v => {
        const rounded = Math.floor(v / _powerScale) * _powerScale;
        return R.length(String(rounded));;
      }
    ),
    d => R.max(...d),
    v => ({ min: (v + 3) * charWidth, max: ((v * 3) + 3) * charWidth })
  )(domain);
};

const getIsLarge = ({ rangeDelta, minSize, maxSize }) => {
  const maxTicks = Math.floor(rangeDelta / minSize) + 1;
  const minTicks = Math.floor(rangeDelta / maxSize) + 1;
  return (Math.abs(4 - minTicks) < Math.abs(4 - maxTicks));
}

// small charts require smallest step above minimum, big charts biggest under maximum
export const computeStep = ({ rangeDelta, minSize, maxSize, domainDelta }) => {
  const isLarge = getIsLarge({ rangeDelta, minSize, maxSize });
  const extremum = isLarge ? maxSize : minSize;
  const slices = Math.floor(rangeDelta / extremum);
  if (slices === 1) {
    return R.prop('step', computeReducedStep({ domainDelta }));
  }
  const nTicks = slices + 1;
  const factors = nTicks < TICKS_COUNT_THRESHOLD ? FEW_TICKS_FACTORS : MANY_TICKS_FACTORS;
  const rawStep = domainDelta / slices;
  const { factor, powerScale } = factorize(rawStep);
  return R.pipe(
    R.reject(isLarge ? f => f > factor : f => f < factor),
    R.ifElse(R.always(isLarge), R.last, R.head),
    R.when(R.isNil, R.always(isLarge ? 0.5 : 10)),
    f => f * powerScale
  )(factors);
};

export const computeIntegerTicks = ({ max, min, pivot, step }) => {
  const _pivot = R.when(R.isNil, R.always(0))(pivot);
  let tick = _pivot;
  if (_pivot >= min && _pivot <= max) {
    let ticksBelowPivot = [];
    let ticksAbovePivot = [];
    while (tick >= min) {
      ticksBelowPivot = R.prepend(tick, ticksBelowPivot);
      tick -= step;
    }
    tick = _pivot;
    while (tick <= max) {
      ticksAbovePivot = R.append(tick, ticksAbovePivot);
      tick += step;
    }
    return R.concat(ticksBelowPivot, R.tail(ticksAbovePivot));
  }
  let ticks = [];
  if (_pivot < min) {
    while (tick < min) {
      tick += step;
    }
    ticks = [tick];
    while (tick + step <= max) {
      tick += step;
      ticks = R.append(tick, ticks);
    }
    return ticks;
  }
  while (tick > max) {
    tick -= step;
  }
  ticks = [tick];
  while (tick - step >= min) {
    tick -= step;
    ticks = R.prepend(tick, ticks);
  }
  return ticks;
};

export const computeTicks = ({ max, min, pivot, step }) => {
  let _pivot = Math.abs(pivot);
  let power = 1;
  while (Math.floor(_pivot) < _pivot) {
    _pivot *= 10;
    power *= 10;
  }
  let _step = step * power;
  while (Math.floor(_step) < _step) {
    _step *= 10;
    _pivot *= 10;
    power *= 10;
  }
  const ticks = computeIntegerTicks({
    max: max * power,
    min: min * power,
    pivot: pivot < 0 ? _pivot * -1 : _pivot,
    step: _step
  });
  if (power === 1) {
    return ticks;
  }
  return R.map(t => R.divide(t, power), ticks);
}

export function computeLinearTicks({ range, domain, options }) {
  const axisSize = options.baseSize;
  const minDisplaySize = options.linear.minDisplaySize;
  const hideAxis = R.not(options.tick.display);
  
  if (axisSize < minDisplaySize || hideAxis) {
    return([]);
  }
  const max = R.max(...domain);
  const min = R.min(...domain);
  const domainDelta = max - min;
  const pivot = R.path(['linear', 'pivot', 'value'], options);
  const inputStep = R.path(['linear', 'step'], options);
  const hasInputStep = R.not(R.isNil(inputStep)) && inputStep < domainDelta;
  const rangeDelta = Math.abs(range[1] - range[0]) + 1;
  const stepLimits = computeStepLimits({ domainDelta, domain, options });
  const minSize = stepLimits.min;
  if (minSize > (rangeDelta / 2) || rangeDelta < SIZE_THRESHOLD) {
    if (hasInputStep)
      console.info(`${utils.prefix} Chart#computeLinearTicks: inputStep (${inputStep}) overridden (#computeReducedTicks)`);
    return computeReducedTicks({ domainDelta, min });
  }
  if (hasInputStep) {
    const inputStepRange = rangeDelta / (domainDelta / inputStep);
    if (inputStepRange >= minSize) {
      return computeTicks({ max, min, pivot, step: inputStep });
    }
    console.info(`${utils.prefix} Chart#computeLinearTicks: inputStep (${inputStep}) overridden (#computeStep)`);
  }
  const step = computeStep({
    rangeDelta,
    domainDelta,
    minSize,
    maxSize: stepLimits.max,
  });
  if (step > (domainDelta / 2)) {
    return computeReducedTicks({ domainDelta, min });
  }
  return computeTicks({ max, min, step, pivot });
};
