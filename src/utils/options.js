import { get, gt, defaults, map, head, reverse, size, min } from 'lodash';
import * as R from 'ramda';
import { TEXT_MIDDLE, LEFT, BOTTOM, OVER, FOCUS, ALWAYS, DISPLAYED, DEFAULT_PROJECTION } from './constants';
import { getColorShade } from './';
import * as chroma from 'd3-scale-chromatic';
import d3 from 'd3';

export function getAndForceAboveZero(options, path, defaultValue=1) {
  const option = get(options, path, defaultValue);
  return gt(option, 0) ? option : defaultValue;
}

export function padding(options) {
  return defaults({ ...options }, { top: 0, right: 0, bottom: 0, left: 0 });
}

export function base(options, el) {
  const margin = Math.floor(get(options, 'margin', 0));
  const { width, height } = el ? el.getBoundingClientRect() : { width: null, height: null };
  const outerWidth = Math.floor(get(options, 'width', width));
  const outerHeight = Math.floor(get(options, 'height', height));
  const innerWidth = outerWidth - margin * 2;
  const innerHeight = outerHeight - margin * 2;

  return {
    width: innerWidth,
    height: innerHeight,
    outerHeight,
    outerWidth,
    margin,
    padding: padding(get(options, 'padding', {})),
    innerPadding: padding(get(options, 'innerPadding', {})),
    isAnnotated: get(options, 'isAnnotated', false)
  };
}

export function format(options) {
  return {
    proc: get(options, 'proc'),
    pattern: get(options, 'pattern', '.2f'),
    isTime: get(options, 'isTime', false),
    maxWidth: get(options, 'maxWidth', null)
  };
}

export function tick(options) {
  const size = get(options, 'size', 6);
  const minorSize = get(options, 'minorSize', 0);
  return {
    size,
    minorSize: (Math.abs(minorSize) > Math.abs(size)) ? size : minorSize,
    minorThickness: get(options, 'minorThickness', 1),
    color: get(options, 'color', 'black'),
    thickness: get(options, 'thickness', 1),
    display: get(options, 'display', true),
    step: get(options, 'step', 0) // time
  };
}

export function font(options) {
  return {
    family: get(options, 'family', 'Segoe UI, sans-serif'),
    size: get(options, 'size', 10),
    color: get(options, 'color', 'black'),
    weight: get(options, 'weight', 'normal'),
    rotation: get(options, 'rotation', -45),
    baseline: get(options, 'baseline', TEXT_MIDDLE),
  };
}

export function ordinal(options) {
  return {
    gap: get(options, 'gap', 0),
    padding: get(options, 'padding', 0),
    minDisplaySize: get(options, 'minDisplaySize', 300)
  };
}

export function pivot(options) {
  return {
    value: get(options, 'value', null),
    color: get(options, 'color', 'black'),
    thickness: get(options, 'thickness', 2),
  };
}

export function linear(options) {
  const step = R.pipe(
    R.propOr(null, 'step'),
    R.when(
      R.complement(R.isNil),
      R.when(R.gte(0), R.always(null))
    )
  )(options);
  return {
    min: get(options, 'min', null),
    max: get(options, 'max', null),
    step,
    frequency: get(options, 'frequency', 'year'), // time
    pivot: pivot(get(options, 'pivot', {})),
    minDisplaySize: get(options, 'minDisplaySize', 100),
    minOffset: get(options, 'minOffset', 30),
    minTickSizeFactor: get(options, 'minTickSizeFactor', 2),
    maxTickSizeFactor: get(options, 'maxTickSizeFactor', 5),
  };
}

export function axe(options) {
  return {
    format: format(get(options, 'format', {})),
    color: get(options, 'color', 'black'),
    thickness: get(options, 'thickness', 1),
    tick: tick(get(options, 'tick', {})),
    font: font(get(options, 'font', {})),
    ordinal: ordinal(get(options, 'ordinal', {})),
    linear: linear(get(options, 'linear', {})),
    padding: get(options, 'padding', 0),
    maxOffsetRatio: get(options, 'maxOffsetRatio', 0.5)
  };
}

export function axis(options) {
  const x = get(options, 'x', {});
  const y = get(options, 'y', {});

  return {
    x: {
      ...axe(x),
      orient: get(x, 'orient', BOTTOM),
    },
    y: {
      ...axe(y),
      orient: get(y, 'orient', LEFT),
    },
  };
}

export function gridLine(options) {
  return {
    color: get(options, 'color', 'lightgrey'),
    thickness: get(options, 'thickness', 1),
    baselines: get(options, 'baselines', [])
  };
}

export function grid(options) {
  return {
    x: gridLine(get(options, 'x', {})),
    y: gridLine(get(options, 'y', {})),
  };
}

export function background(options) {
  return {
    color: get(options, 'color', '#f2f2f2'),
  };
}

export function annotation(options) {
  let annotationFont = font(get(options, 'font', {}));
  const innerColor = get(options, 'font.innerColor', 'white');
  delete annotationFont.color;
  return {
    font: { ...annotationFont, innerColor },
    format: {
      datapoint: format(get(options, 'format.datapoint', {})),
      serie: format(get(options, 'format.serie', {}))
    },
    margin: get(options, 'margin', 4),
    display: get(options, 'display', FOCUS),
    minWidth: get(options, 'minWidth', 32)
  };
}

export function scatter(options) {
  return {
    markerRadius: get(options, 'markerRadius', 7),
    areaIndex: get(options, 'areaIndex', .1),
  };
}

export function line(options) {
  return {
    shadow: get(options, 'shadow', 10),
    thickness: get(options, 'thickness', 1),
    marker: {
      shadow: get(options, 'marker.shadow', 2),
      shape: get(options, 'marker.shape', 'circle')
    },
    focused: {
      thickness: get(options, 'focused.thickness', 2),
    },
    densityRatio: get(options, 'densityRatio', 32)
  };
}

export const symbolMarkers = () => {
  const paths = {
    circle: size => {
      const r = Math.sqrt(size) / 2;
      return `M0,${r} A${r},${r} 0 1,1 0,${-r} A${r},${r} 0 1,1 0,${r} Z`;
    },
    square: size => {
      const r = Math.sqrt(size) / 2;
      return `M${-r},${-r} L${r},${-r} ${r},${r} ${-r},${r} Z`;
    }
  };
  return ([
    { style: { stroke: '#607D8B', fill: '#607D8B' }, path: paths.circle },
    { style: { stroke: '#607D8B', fill: '#f2f2f2' }, rotate: 45, path: paths.square },
  ]);
}

export function symbol(options) {
  return {
    markers: get(options, 'markers', symbolMarkers()),
    markerDefaultSize: get(options, 'markerDefaultSize', 50),
    markerDefaultStrokeWidth: get(options, 'markerDefaultStrokeWidth', 1),
    highlightLineThickness: get(options, 'highlightLineThickness', 2),
    gapMinMaxColor: get(options, 'gapMinMaxColor', '#607D8B'),
    gapMinMaxThickness: get(options, 'gapMinMaxThickness', 1),
    gapAxisMinColor: get(options, 'gapAxisMinColor', 'red'),
    gapAxisMinThickness: get(options, 'gapAxisMinThickness', 1),
  }
}

export function tooltip(options) {
  function layout(serie, datum, color) {
    const style = `
      font-size: 10px;
      font-family: sans-serif;
      color: ${color};
      padding: 5px;
      border: 1px solid ${color};
      background: white;
      opacity: .8;
    `;
    return `<div style="${style}">X: ${datum.x}<br />Y: ${datum.y}</div>`;
  }

  return {
    display: get(options, 'display', OVER),
    layout: get(options, 'layout', layout),
    zIndex: get(options, 'zIndex', 1500),
  };
}

function geoMap(options) {
  return ({
    defaultAreaColor: get(options, 'defaultAreaColor', 'white'),
    scale: get(options, 'scale', null),
    innerPadding: padding(get(options, 'innerPadding', {})),
    extBoundariesColor: get(options, 'extBoundariesColor', '#8EA4B1'),
    extBoundariesThickness: get(options, 'extBoundariesThickness', 1),
    extBoundariesDisplay: get(options, 'extBoundariesDisplay', DISPLAYED),
    intBoundariesColor: get(options, 'intBoundariesColor', 'black'),
    intBoundariesThickness: get(options, 'intBoundariesThickness', 1),
    intBoundariesDisplay: get(options, 'intBoundariesDisplay', DISPLAYED),
    projection: get(options, 'projection', DEFAULT_PROJECTION)
  })
}

function choropleth(options) {
  const _divisions = get(options, 'divisions', 10);
  const divisions = _divisions >= 1 ? _divisions : 10;
  const _domain = get(options, 'domain', 'YlOrRd');
  const domain = chroma[`interpolate${_domain}`] ? _domain : 'YlOrRd';
  const scale = chroma[`interpolate${domain}`];
  const coeff = d3.scale.linear().domain([1, divisions]).range([0, 1]);
  let colors = [];
  for(let i = 1; i <= divisions; i++) {
    colors.push(scale(coeff(i)));
  }
  if (get(options, 'invertColors', false)) {
    reverse(colors);
  }
  return ({
    colors: get(options, 'colors', colors),
    labelDisplay: get(options, 'labelDisplay', DISPLAYED),
    labelFont: font(get(options, 'labelFont', {})),
    maxDomain: get(options, 'maxDomain', undefined),
    minDomain: get(options, 'minDomain', undefined),
  })
}

function layerSeries(layers, colors, colorCoeff, layersAmount) {
  const nColors = R.length(colors);
  return map(
    layers,
    (layer, index) => {
      const color = R.nth(index % nColors, colors);
      const _colorCoeff = colorCoeff(index, layersAmount);
      return ({
        ...layer,
        color,
        colorCoeff: nColors === 1 ? _colorCoeff : 0,
        focusColorCoeff: _colorCoeff,
        baseColor: getColorShade(color, nColors === 1 ? _colorCoeff : 0)
      });
    }
  );
}

function defaultColorCoeff(index, layersAmount) {
  return layersAmount ? index * (2 / layersAmount) : null;
}

export function stacked(options, colors) {
  const layers = get(options, 'layerSeries', []);
  const layersAmount = size(layers);
  const colorCoeff = get(options, 'colorCoeff', defaultColorCoeff);
  return ({
    colorCoeff,
    layerSeries: layerSeries(layers, colors, colorCoeff, layersAmount),
    strokeThickness: get(options, 'strokeThickness', 1),
    strokeColor: get(options, 'strokeColor', 'black'),
  });
}

export function serie(options) {
  const colors = get(options, 'colors', ['#607D8B']);
  return {
    colors,
    overColors: get(options, 'overColors', ['#E91E63']),
    highlightColors: get(options, 'highlightColors', ['#3F51B5', '#4CAF50']),
    baselineColors: get(options, 'baselineColors', ['#263238']),
    annotation: annotation(get(options, 'annotation', {})),
    scatter: scatter(get(options, 'scatter', {})),
    line: line(get(options, 'line', {})),
    symbol: symbol(get(options, 'symbol', {})),
    tooltip: tooltip(get(options, 'tooltip', {})),
    stacked: stacked(get(options, 'stacked', {}), colors),
    choropleth: choropleth(get(options, 'choropleth', {}))
  };
}

export function choroLegend(options, el) {
  const { width, height } = el ? el.getBoundingClientRect() : { width: null, height: null };
  return ({
    width: get(options, 'width', width),
    height: get(options, 'height', height),
    margin: padding(get(options, 'margin', {})),
    axis: {
      ...axe(get(options, 'axis', {})),
      orient: get(options, 'axis.orient', BOTTOM)
    }
  });
}

export function legend(options, el) {
  return ({
    choropleth: choroLegend(get(options, 'choropleth', {}), el)
  });
}

export default function (options, el) {
  return {
    base: base(get(options, 'base', {}), el),
    axis: axis(get(options, 'axis', {})),
    grid: grid(get(options, 'grid', {})),
    background: background(get(options, 'background', {})),
    map: geoMap(get(options, 'map', {})),
    legend: legend(get(options, 'legend', {}), el),
    serie: serie(get(options, 'serie', {})),
  };
}

// reduce methods avoid spreading options manipulations
// if options structure evolve, it is easier to update reducers
// instead of tracking options usage everywhere
// (to be perfectly coherent, we should introduce getters)
export function reduceAxisPadding(_axe, padding, options) {
  return { ...options, axis: { ...options.axis, [_axe]: { ...options.axis[_axe], padding } } };
}

export function hideAxisLabels(_axe, options) {
  return {
    ...options,
    axis: {
      ...options.axis,
      [_axe]: {
        ...options.axis[_axe],
        tick: {
          ...options.axis[_axe].tick,
          display: false
        }
      }
    }
  };
}
