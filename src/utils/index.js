import { isEmpty, sumBy, head, last, isUndefined, isFunction, get, sortBy, each, size, ceil, round, isDate } from 'lodash';
import compose from 'lodash.compose';
import d3 from 'd3';
import meta from '../../package.json';
import * as C from '../utils/constants';

export const prefix = `[${meta.name}@${meta.version}]`;

export function noElError(el) {
  if (!el) throw new Error('[rcw] no el');
}

export function noDataError(data, ...msg) {
  const noData = isEmpty(data);
  if (noData) console.warn(prefix, ...msg, 'no data');
  return noData;
}

export function setupElement(selection, tag, cls) {
  return selection.append(tag).classed(cls, true);
}

export function selector(_selector) {
  return `.${_selector}`;
}

export function getElementDimensions(element) {
  const box = element.node().getBBox();
  return { width: box.width, height: box.height, x: box.x, y: box.y };
}

export function getExtentDelta(extent) {
  return sumBy(extent, Math.abs);
}

export function getRangeDelta(range) {
  return getExtentDelta([head(range) - last(range)]);
}

export function getDomainDelta(domain) {
  return getExtentDelta([head(domain) - last(domain)]);
}

export function getScaleExtent(scale) {
  return isUndefined(scale.rangeExtent) ? scale.range() : scale.rangeExtent();
}

export function lineStyle({ color, thickness }) {
  return {
    fill: 'none',
    'shape-rendering': 'crispEdges',
    stroke: color,
    'stroke-width': thickness,
  };
}

export function fontStyle({ family, size, color, weight }) {
  return {
    'font-family': family,
    'font-size': `${size}px`,
    fill: color,
    'font-weight': weight,
    'user-select': 'none',
    cursor: 'default'
  };
}

export function fontBaselineOffset(textElement, baseline) {
  // IE does not support dominant-baseline style, need to position text manually
  const textHeight = getElementDimensions(textElement).height;

  let offset = 0;
  switch(baseline) {
    case C.TEXT_ABOVE:
      offset = textHeight / 2 * -1;
      break;
    case C.TEXT_MIDDLE:
      offset = 0;
      break;
    case C.TEXT_BELOW:
      offset = textHeight / 2;
      break;
  }
  return offset;
}

export function getFormat({ proc, pattern, isTime }) {
  if (isFunction(proc)) return proc;
  return isTime ? d3.time.format(pattern) : compose(parseFloat, d3.format(pattern));
}

export function getColor(options, { index, type }, defaultKeyColor='colors') {
  const colors = get(options, (type ? `${type}Colors` : defaultKeyColor), []);
  return colors[index % colors.length];
}

export function getOverColor(options, { index, type }) {
  return getColor(options, { index, type }, 'overColors');
}

export function getColorShade(color, colorCoeff) {
  return d3.rgb(color).brighter(colorCoeff).toString();
}

export function isScaleLinear(scale) {
  return isUndefined(scale.rangeBand);
}

export function getScaleType(scale) {
  if (!isUndefined(scale.rangeBand)) {
    return C.ORDINAL;
  }
  if (isDate(head(scale.domain()))) {
    return C.TIME;
  }
  return C.LINEAR;
}

export function getTextDimensions(element, text='', options={}, rotation=0) {
  const sandBox = element.append('svg:svg');
  const { width: x, height: y } = getElementDimensions(element);
  sandBox
    .append('text')
    .attr({ x, y })
    .attr('transform', `rotate(${rotation})`)
    .style(fontStyle(options))
    .text(text);
  let { width, height } = getElementDimensions(sandBox);
  sandBox.remove();
  return { width, height };
}

function getTickTextSize(element, rotated, axisKey, options) {
  switch (axisKey) {
    case 'y':
      return options.font.size;
    case 'x':
      if (rotated === C.UNROTATED) return getElementDimensions(element).width;
      const height = options.font.size;
      return (Math.abs(height / Math.sin(options.font.rotation * Math.PI / 180)));
  }
}

export function ordinalLabelsOverlap(selection, axis, axisKey, options, rotated) {
  const scale = axis.scale();
  let maxTicksOffset = 0;
  selection.selectAll('text').each(function(d){
    const size = getTickTextSize(d3.select(this), rotated, axisKey, options);
    if (size > maxTicksOffset)
      maxTicksOffset = size;
  });
  const band = scale.rangeBand();
  if (maxTicksOffset >= band) {
    return true;
  }
  return false;
}