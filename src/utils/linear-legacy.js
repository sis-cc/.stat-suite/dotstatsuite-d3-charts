import d3 from 'd3';
import { head, last, round, uniq, concat, min as _min, max as _max } from 'lodash';
import { getDomainDelta, getFormat } from '../utils/';

export function computeStep(domain, step) {
  const delta = getDomainDelta(domain);
  if (!step || step === 0 || round(delta / step) > MAX_TICK_VALUES)
    return round(delta / DEFAULT_TICK_VALUES_COUNT);
  return step;
}

export function computeTickValues(domain, step, pivot) {
  const _step = computeStep(domain, step);
  const [ticksBelowPivot, ticksAbovePivot] = [[], []];
  let [tickBelowPivot, tickAbovePivot] = [pivot, pivot];

  while (tickBelowPivot > head(domain) || tickAbovePivot < last(domain)) {
    if (tickBelowPivot > head(domain)) {
      ticksBelowPivot.push(tickBelowPivot);
      tickBelowPivot -= _step;
    }

    if (tickAbovePivot < last(domain)) {
      ticksAbovePivot.push(tickAbovePivot);
      tickAbovePivot += _step;
    }
  }

  // while allows a n complexity but does not handle the edge cases without an infinite loop
  if (tickAbovePivot === last(domain)) ticksAbovePivot.push(tickAbovePivot);
  if (tickBelowPivot === head(domain)) ticksBelowPivot.push(tickBelowPivot);

  return uniq(concat([], ticksBelowPivot, ticksAbovePivot));
}

export function getLinearTickValues(scale, options, pivot) {
  const size = options.baseSize;
  const min = options.linear.minDisplaySize;
  
  return (size >= min && options.tick.display) ?
    computeTickValues(scale.domain(), options.linear.step, pivot) : [];
}

export function computeLinearAxis(scale, pivot, options) {
  return d3.svg.axis()
    .scale(scale)
    .orient(options.orient)
    .tickSize(options.tick.size, options.tick.size)
    .tickFormat(getFormat(options.format))
    .tickValues(getLinearTickValues(scale, options, pivot));
}

