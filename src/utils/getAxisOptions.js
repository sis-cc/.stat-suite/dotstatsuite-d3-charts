import * as R from 'ramda';

export const getLinearAxisOptions = (axis, options) => {
  const ticks = axis.tickValues();
  if (R.isNil(ticks) || R.isEmpty(ticks)) {
    return ({ min: null, max: null, step: null, pivot: null });
  }
  const domain = axis.scale().domain();
  const pivot = R.path(['linear', 'pivot', 'value'], options);
  const min = R.min(...domain);
  const max = R.max(...domain);
  const [t1, t2] = R.take(2, ticks);
  const step = t2 - t1;
  return ({ min, max, step, pivot });
}
