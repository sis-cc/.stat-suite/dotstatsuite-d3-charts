import { head, last, min as _min, max as _max, isNull } from 'lodash';
import * as R from 'ramda';
import d3 from 'd3';
import { getDomainDelta, getFormat } from '../utils/';
import { computeLinearTicks } from './linear';
import { computeTimeSteps, computeTimeTicks } from './time';
export { computeTimeDomain } from './time';
import { refineLinearDomain } from './linear-domain';

export function computeLinearScale(range, domain) {
  return d3.scale.linear().range(range).domain(domain);
}

export function computeLinearDomain(options, data, datumAccessor) {
  const extent = d3.extent(data, datumAccessor);
  let [min, max] = [
    _min([head(extent), options.linear.min]),
    _max([last(extent), options.linear.max])
  ];
  const pivot = options.linear.pivot.value;
  if (!isNull(pivot)) {
    if (pivot < min) min = pivot;
    if (pivot > max) max = pivot; 
  }
  if (min === max) {
    const delta = Math.abs(min);
    [min, max] = [min - delta, min + delta];
  }
  return refineLinearDomain([min, max]);
}

export function computeLinearAxis(scale, pivot, options) {
  const range = scale.range();
  const domain = scale.domain();
  const ticks = computeLinearTicks({ range, domain, options });
  return d3.svg.axis()
    .scale(scale)
    .orient(options.orient)
    .tickSize(options.tick.size, options.tick.size)
    .tickFormat(getFormat(options.format))
    .tickValues(ticks);
};

export function computeOrdinalScale(options, interval, values) {

  return d3.scale.ordinal().domain(values).rangeBands(interval, options.gap, options.padding);
}

function getOrdinalTickValues(scale, options) {
  const size = options.baseSize;
  const min = options.ordinal.minDisplaySize;

  return (size >= min && options.tick.display) ? scale.domain() : [];
}

export function computeOrdinalAxis(scale, options) {
  return d3.svg.axis()
    .scale(scale)
    .orient(options.orient)
    .tickSize(options.tick.size, options.tick.size)
    .tickFormat(getFormat(options.format))
    .tickValues(getOrdinalTickValues(scale, options));
}

export function computeTimeScale(range, domain) {
  return d3.time.scale.utc().range(range).domain(domain);
}

export function computeTimeAxis(scale, pivot, options) {
  const range = scale.range();
  const domain = scale.domain();
  const rangeDelta = Math.abs(range[1] - range[0]) + 1;
  const min = options.ordinal.minDisplaySize;
  if (rangeDelta < min) {
    return d3.svg.axis()
    .scale(scale)
    .orient(options.orient)
    .tickSize(options.tick.size, options.tick.size)
    .tickFormat(getFormat(options.format))
    .tickValues([]);
  }
  const { minorStep } = computeTimeSteps({ range, domain, options });
  const frequency = R.pathOr('year', ['linear', 'frequency'], options);
  const freq = R.has(frequency, d3.time) ? frequency : 'year';
  return d3.svg.axis()
    .scale(scale)
    .orient(options.orient)
    .tickSize(options.tick.size, options.tick.size)
    .tickFormat(getFormat(options.format))
    .tickValues(computeTimeTicks({ domain, minorStep, options }))
}

export function computeAreaIndex(areaIndex, domain, domainIndex) {
  return domainIndex - getDomainDelta(domain) * areaIndex;
}

export function computeOffset(size, start, end) {
  return ((start + size - end) / 2) - start;
}
