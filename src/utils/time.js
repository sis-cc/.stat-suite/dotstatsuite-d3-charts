import d3 from 'd3';
import * as R from 'ramda';

export const getMinLabelWidth = ({ domain, options }) => {
  const proc = R.path(['format', 'proc'], options);
  const labelFormat = R.is(Function, proc) ? proc : d3.time.format(R.path(['format', 'pattern'], options));
  const fontSize = R.pathOr(12, ['font', 'size'], options);
  const charWidth = Math.ceil(fontSize * 0.5);
  const label= labelFormat(R.head(domain));
  if (R.isNil(label)) {
    return 30; //security;
  }
  return (R.length(label) + 3) * charWidth;
};

export const getTimeDomainDelta = ({ domain, frequency, step }) => {
  const [min, max] = R.sortBy(date => Date.parse(date), domain);
  const parsedMax = Date.parse(max);
  const freq = R.has(frequency, d3.time) ? frequency : 'year';
  return R.until(
    offset => {
      const date = d3.time[freq].utc.offset(min, offset);
      const parsed = Date.parse(date);
      return parsed >= parsedMax
    },
    R.add(step)
  )(step);
};

export const getTimeDefaultMajorStep = ({ labelSize, rangeDelta, domainDelta, options }) => {
  const dataStep = R.pathOr('year', ['linear', 'step'], options);
  const maxSlices = R.when(R.equals(0), R.always(1))(Math.floor(rangeDelta / labelSize));
  const minStep = Math.ceil(domainDelta / maxSlices);
  if (dataStep >= minStep) {
    return dataStep;
  }
  const frequency = R.pathOr('year', ['linear', 'frequency'], options);

  const stepValidator = step => {
    if (frequency !== 'month') {
      return true;
    }
    return step < 12 ? 12 % step === 0 : step % 12 === 0;
  };

  return R.until(
    stepValidator,
    R.inc
  )(minStep);
};

export const getTimeMajorStep = ({ labelSize, rangeDelta, domainDelta, options }) => {
  const defaultStep = getTimeDefaultMajorStep({ labelSize, rangeDelta, domainDelta, options });
  const inputStep = R.path(['tick', 'step'], options);
  return inputStep >= defaultStep ? inputStep : defaultStep;
};

export const computeTimeMinorStep = ({ dataStep, majorStep, rangeDelta, domainDelta }) => {
  if (dataStep === majorStep) {
    return dataStep;
  }
  const dataSlices = Math.floor(domainDelta / dataStep);
  const dataSliceDelta = Math.floor(rangeDelta / dataSlices);
  if (dataSliceDelta >= 4) {
    return dataStep;
  }
  const maxSlices = Math.floor(rangeDelta / 4);
  const minStep = Math.ceil(domainDelta / maxSlices);

  return R.pipe(
    R.until(step => majorStep % step === 0, R.inc)
  )(minStep);
};

export const computeTimeSteps = ({ range, domain, options }) => {
  if (R.isNil(domain) || R.isEmpty(domain) || R.any(R.isNil)(domain)) {
    return ({ minorStep: null, majorStep: null });
  }
  const labelSize = getMinLabelWidth({ domain, options });
  const rangeDelta = Math.abs(range[1] - range[0]) + 1;
  const { frequency, step } = options.linear; 
  const domainDelta = getTimeDomainDelta({ domain, frequency, step });
  const majorStep = getTimeMajorStep({ labelSize, rangeDelta, domainDelta, options });
  const minorStep = computeTimeMinorStep({ dataStep: step, rangeDelta, domainDelta, majorStep });

  return ({ majorStep, minorStep });
};

export const computeTimeDomain = (options, data, datumAccessor, range) => {
  const domain = d3.extent(data, datumAccessor);
  if (R.any(R.isNil)(domain)) {
    return ([]);
  }
  const { frequency, step } = options.linear;
  const domainDelta = getTimeDomainDelta({ domain, frequency, step });
  const rangeDelta = Math.abs(range[1] - range[0]) + 1;
  const labelSize = getMinLabelWidth({ domain, options });
  const majorStep = getTimeMajorStep({ labelSize, rangeDelta, domainDelta, options });
  const majorDomainDelta = R.until(
    delta => delta >= domainDelta,
    R.add(majorStep)
  )(majorStep);
  const minDomain = R.pipe(
    R.sortBy(date => Date.parse(date)),
    R.head,
  )(domain);
  const freq = R.has(frequency, d3.time) ? frequency : 'year';
  const res = [minDomain, d3.time[freq].utc.offset(minDomain, majorDomainDelta)]; 
  return res;
};

export const computeTimeTicks = ({ domain, minorStep, options }) => {
  const frequency = R.pathOr('year', ['linear', 'frequency'], options);
  const freq = R.has(frequency, d3.time) ? frequency : 'year';
  const [min, max] = R.sortBy(date => Date.parse(date), domain);

  let ticks = [];
  let tick = min;
  while (Date.parse(tick) <= Date.parse(max)) {
    ticks = R.append(tick, ticks);
    tick = d3.time[freq].utc.offset(tick, minorStep)
  }
  return ticks;
};


export const computeMinorTicks = ({ domain, minorStep, majorStep, size, options }) => {
  //size is a trick, giving the amount of minorTicks to avoid too much computation
  const frequency = R.pathOr('year', ['linear', 'frequency'], options);
  const freq = R.has(frequency, d3.time) ? frequency : 'year';
  const min = R.head(R.sortBy(date => Date.parse(date), domain));

  return R.reduce(
    (ticks, index) => {
      const step = index * minorStep;
      const tick = d3.time[freq].utc.offset(min, step);
      return R.when(
        R.always(step % majorStep !== 0),
        R.append(tick)
      )(ticks);
    },
    [],
    R.times(R.identity, size)
  );
};
