import * as R from 'ramda';
import { ceil, floor } from 'lodash';
import { factorize } from './linear';

export const refineLinearDomain = (domain) => {
  const [min, max] = domain;
  const delta = max - min;

  const { factor, powerScale } = factorize(delta);
  const scale = R.when(
    R.always(factor < 5),
    scale => R.divide(scale, 10)
  )(powerScale);

  if (scale >= 1) {
    return [Math.floor(min), Math.ceil(max)];
  }

  let precision = 0;
  let _scale = scale;
  while (_scale != 1) {
    precision += 1;
    _scale = _scale * 10;
  }

  return [floor(min, precision), ceil(max, precision)];
};
