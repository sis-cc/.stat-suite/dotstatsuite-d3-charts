import d3 from 'd3';
import { head, min, max } from 'lodash';
import { is } from 'ramda';

import BackgroundChart from '../background';

import * as utils from '../utils/';

import selectors from './selectors';
import accessors from './accessors';
import computors from './computors';
import eauors from './eauors';
import { getLinearAxisOptions } from '../utils/getAxisOptions';

@selectors
@computors
@accessors
@eauors
export default class HorizontalSymbolChart extends BackgroundChart {
  static ors = ['selectors', 'accessors', 'computors', 'eauors'];

  constructor(el, options={}, data=[], getInitialOptions, getAxisOptions) {
    const datapoints = HorizontalSymbolChart.computors.data(data);
    //maybe not ideal, but since override of computors.data from BaseChart doesn't work ...
    super(el, options, [{ datapoints }], getInitialOptions);
    _contructor(
      el,
      this.options,
      data,
      {
        selectors: this.constructor.selectors,
        accessors: this.constructor.accessors,
        computors: this.constructor.computors,
        eauors: this.constructor.eauors
      },
      this.base,
      this.axis
    );
  }

  update(el, options={}, data=[], getInitialOptions, getAxisOptions) {
    const datapoints = HorizontalSymbolChart.computors.data(data);
    super.update(el, options, [{ datapoints }], getInitialOptions);
    _update(
      el,
      this.options,
      data,
      {
        selectors: this.constructor.selectors,
        accessors: this.constructor.accessors,
        computors: this.constructor.computors,
        eauors: this.constructor.eauors
      },
      this.base,
      this.axis,
      getAxisOptions
    );
  }
}

function _contructor(el, options, data, ors, base, axis) {

  utils.noElError(el);

  d3.select(el)
    .select(utils.selector(ors.selectors.base))
    .call(utils.setupElement, 'svg:g', ors.selectors.serie);

  _update(el, options, data, ors, base, axis);

}

function _update(el, options, data, ors, base, axis, getAxisOptions) {
  if (process.env.NODE_ENV !== 'production') {
    console.time(`${utils.prefix} HorizontalSymbolChart#_update`);
  }

  utils.noElError(el);
  if (utils.noDataError(ors.accessors.data(data), '\t\t\t\t', 'HorizontalSymbolChart')) {
    d3.select(el)
      .selectAll(utils.selector(ors.selectors.symb)).remove();
    return;
  }
  
  const scales = {
    xScale: ors.accessors.xScale(base, axis),
    yScale: ors.accessors.yScale(base, axis)
  };

  const symbolValues = ors.accessors.symbolValues(data);
  //dimension value name matching the symbol, for the tooltip ...

   const symbJoin = d3.select(el)
    .select(utils.selector(ors.selectors.serie))
    .selectAll(utils.selector(ors.selectors.symb))
    .data(ors.accessors.data(data)) //bound to the ordinal series of datapoints
  symbJoin.enter().call(ors.eauors.symbEnter, options.serie, ors.selectors.symb);
  symbJoin.exit().remove();

  const axisCoord = min(scales.xScale.domain());
  //needed for the following lines,
  //get the coordinate of the ordinalAxis from the linear,
  //the x of yAxis in horizontal, the y of xAxis in vertical,
  //->min value of linear scale domain

  const gapAxisMinJoin = symbJoin //line gap between ordinal axis and min values
    .selectAll(utils.selector(ors.selectors.gapAxisMin))
    .data(data => [data])
  gapAxisMinJoin.enter().call(ors.eauors.gapAxisMinEnter, ors.selectors);
  gapAxisMinJoin.call(ors.eauors.gapAxisMinUpdate, options, ors.accessors, scales, axisCoord);
  gapAxisMinJoin.exit().remove();

  const gapMinMaxJoin = symbJoin //line gap between min and max values
    .selectAll(utils.selector(ors.selectors.gapMinMaxLine))
    .data(data => [data])
  gapMinMaxJoin.enter().call(ors.eauors.gapMinMaxEnter, ors.selectors);
  gapMinMaxJoin.call(ors.eauors.gapMinMaxUpdate, options, ors.accessors, scales);
  gapMinMaxJoin.exit().remove();

  // these two gaps Join annoys me a little => two data join identical
  //didn't find a way to build the two lines inside an unique dataJoin;

  const markerJoin = symbJoin
    .selectAll(utils.selector(ors.selectors.marker))
    .data(ors.computors.computeMarkers(symbolValues));
  markerJoin.enter().call(ors.eauors.markerEnter, options, ors.selectors, ors.accessors, symbJoin, base.hideTooltip);
  markerJoin.call(ors.eauors.markerUpdate, options, ors.accessors, scales, base.showTooltip);
  markerJoin.exit().remove();

  const yAxisLabelJoin = d3.select(el)
    .select(utils.selector(ors.selectors.yAxis))
    .selectAll('text')
    .data(ors.computors.computeOrdinalFocus(data));
  yAxisLabelJoin.call(ors.eauors.yAxisLabelUpdate, options.serie, ors.accessors);

  if (is(Function, getAxisOptions)) {
    const xAxisoptions = getLinearAxisOptions(axis.xAxis, options.axis.x);
    getAxisOptions({
      computedMinX: xAxisoptions.min,
      computedMaxX: xAxisoptions.max,
      computedStepX: xAxisoptions.step,
      computedPivotX: xAxisoptions.pivot
    });
  }
  if (process.env.NODE_ENV !== 'production') {
    console.timeEnd(`${utils.prefix} HorizontalSymbolChart#_update`);
  }
}