export default function selectors(target) {
  const chart = target.selectors.chart;

  target.selectors = {
    ...target.selectors,
    serie: `${chart}__serie`,
    symb: `${chart}__symb`,
    marker: `${chart}__marker`,
    gapMinMaxLine: `${chart}__gapMinMaxLine`,
    gapAxisMin: `${chart}__gapAxisMin`,
  };
}
