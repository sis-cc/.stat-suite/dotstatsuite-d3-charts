import d3 from 'd3';
import { setupElement, getColor, getOverColor, selector } from '../utils';
import { get, curry, max, min, size } from 'lodash';

function symbEnter(selection, options, selector) {
  setupElement(selection, 'svg:g', selector);
}

function markerOver(selectors, options, accessors, symbSelection, datum) {
  const shapeMarkers = symbSelection.selectAll(selector(selectors.marker)).filter(d => d.marker === datum.marker);
  const otherMarkers = symbSelection.selectAll(selector(selectors.marker)).filter(d => d.marker !== datum.marker);
  shapeMarkers.style({
    fill: d => getOverColor(options.serie, accessors.focusData(d)),
    stroke: d => getOverColor(options.serie, accessors.focusData(d))
  });
  otherMarkers.style({ opacity: 0.5 });
}

function markerOut(selectors, options, accessors, symbSelection, datum) {
  const shapeMarkers = symbSelection.selectAll(selector(selectors.marker)).filter(d => d.marker === datum.marker);
  const otherMarkers = symbSelection.selectAll(selector(selectors.marker)).filter(d => d.marker !== datum.marker);
  shapeMarkers.style({
    stroke: datum => accessors.markerColor(options, 'stroke', datum),
    fill: datum => accessors.markerColor(options, 'fill', datum)
  });
  otherMarkers.style({ opacity: 1 });
}

function markerEnter(selection, options, selectors, accessors, symbSelection, hideTooltip) {
  setupElement(selection, 'svg:path', selectors.marker)
  .on('mouseover', curry(markerOver)(selectors, options, accessors, symbSelection))
  .on('mouseout', curry(markerOut)(selectors, options, accessors, symbSelection))
  .on('mouseleave', curry(hideTooltip));
}

function markerUpdate(selection, options, accessors, { xScale, yScale }, showTooltip) {
  selection
    .attr({
      d: datum => {
        const marker = accessors.marker(options.serie, datum);
        const size = get(marker, 'size', options.serie.symbol.markerDefaultSize);
        return marker.path(size);
      },
      transform: datum => {
        const x = xScale(accessors.x(datum));
        const y = yScale(accessors.y(datum)) + yScale.rangeBand() / 2;
        const rotate = accessors.markerRotate(options, datum);
        return `translate(${x}, ${y}) rotate(${rotate})`;
      }
    })
    .on('mousemove', function(datum){
      const serie = d3.select(this.parentNode).datum();
      showTooltip(options.serie, { xScale, yScale }, serie, datum);
    });
  selection.style({
    'stroke-width': datum => accessors.markerStrokeWidth(options, datum),
    stroke: datum => accessors.markerColor(options, 'stroke', datum),
    fill: datum => accessors.markerColor(options, 'fill', datum)
  })
}

function gapMinMaxEnter(selection, selectors) {
  setupElement(selection, 'svg:line', selectors.gapMinMaxLine);
}

function gapMinMaxUpdate(selection, options, accessors, scales) {
  selection
    .attr({
      x1: datum => `${scales.xScale(min(datum.values))}`,
      x2: datum => `${scales.xScale(max(datum.values))}`,
      y1: datum => `${scales.yScale(datum.category) + scales.yScale.rangeBand() / 2}`,
      y2: datum => `${scales.yScale(datum.category) + scales.yScale.rangeBand() / 2}`
    });
  selection.style({
    'stroke-width': options.serie.symbol.gapMinMaxThickness,
    stroke: options.serie.symbol.gapMinMaxColor,
    'shape-rendering': 'crispEdges'
  });
}

function gapAxisMinEnter(selection, selectors) {
  setupElement(selection, 'svg:line', selectors.gapAxisMin);
}

function gapAxisMinUpdate(selection, options, accessors, scales, axisCoord) {
  selection
    .attr({
      x1: datum => `${scales.xScale(axisCoord)}`,
      x2: datum => `${scales.xScale(min(datum.values))}`,
      y1: datum => `${scales.yScale(datum.category) + scales.yScale.rangeBand() / 2}`,
      y2: datum => `${scales.yScale(datum.category) + scales.yScale.rangeBand() / 2}`
    });
  selection.style({
    'stroke-width': options.serie.symbol.gapAxisMinThickness,
    stroke: options.serie.symbol.gapAxisMinColor,
    'shape-rendering': 'crispEdges'
  })
}

export default function eauors(target) {
  target.eauors = {
    ...target.eauors,
    symbEnter,
    markerEnter,
    markerUpdate,
    gapMinMaxEnter,
    gapMinMaxUpdate,
    gapAxisMinEnter,
    gapAxisMinUpdate,
  };
}