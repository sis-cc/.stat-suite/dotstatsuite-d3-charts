import { filter, flatten, get, map, nth, size } from 'lodash';
import { computeOrdinalScale, computeOrdinalAxis } from '../utils/computors';

export default function computors(target) {
  target.computors = {
    ...target.computors,
    data: (data) => {
      return filter(flatten(
        map(
          target.accessors.data(data),
          (serie) => (
            map(
              serie.values,
              (value)  => (
                {
                  x: value,
                  y: get(serie, 'category', ''),
                  baselineIndex: get(serie, 'baselineIndex', -1),
                  highlightIndex: get(serie, 'highlightIndex', -1)
                }
              )
            )
          )
        )
      ), d => d.x !== null);
    },
    computeOrdinalFocus: (data) => map(
      target.accessors.data(data),
      datapoint => ({
        baselineIndex: get(datapoint, 'baselineIndex', -1),
        highlightIndex: get(datapoint, 'highlightIndex', -1)
      })
    ),
    computeMarkers: (symbolValues) => (datum) => filter(
      map(
        datum.values,
        (v, i) => ({
          baselineIndex: get(datum, 'baselineIndex', -1),
          highlightIndex: get(datum, 'highlightIndex', -1),
          formatedValue: nth(get(datum, 'formatedValues', []), i),
          x: v,
          y: datum.category,
          marker: i,
          symbolValue: get(symbolValues, `[${i}]`, '')
        })
      ),
      d => d.x !== null
    ),
    yValues: data => map(target.accessors.data(data), target.accessors.y),
    yScale: (options, data, offset) => {
      return computeOrdinalScale(
        options.axis.y.ordinal,
        target.computors.yRange(options, offset),
        target.computors.yValues(data)
      );
    },
    yAxis: (scale, pivot, options) => computeOrdinalAxis(scale, options)
  };
}