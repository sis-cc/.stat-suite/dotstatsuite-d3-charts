export default function selectors(target) {
  const chart = target.selectors.chart;

  target.selectors = {
    ...target.selectors,
    map: `${chart}__map`,
    background: `${chart}__background`,
    area: `${chart}__area`,
    areas: `${chart}__areas`,
    land: `${chart}__land`,
    intBoundaries: `${chart}__interior_boundaries`,
    extBoundaries: `${chart}__exterior_boundaries`,
    center: `${chart}__center`,
    graticules: `${chart}__graticules`,
  };
}