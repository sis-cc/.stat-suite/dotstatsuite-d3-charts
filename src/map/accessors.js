import { path, pathOr } from 'ramda';

export default function accessors(target) {
  target.accessors = {
    ...target.accessors,
    areas: data => pathOr({}, ['objects', 'areas'], data),
    areaColor: options => path(['map', 'defaultAreaColor'], options),
    id: d => pathOr(null, ['properties', 'id'], d),
    label: d => pathOr(null, ['properties', 'label'], d),
    scale: options => path(['map', 'scale'], options),
    width: options => path(['base', 'width'], options),
    height: options => path(['base', 'height'], options),
    extBoundariesColor: options => path(['map', 'extBoundariesColor'], options),
    extBoundariesThickness: options => path(['map', 'extBoundariesThickness'], options),
    extBoundariesDisplay: options => path(['map', 'extBoundariesDisplay'], options),
    intBoundariesColor: options => path(['map', 'intBoundariesColor'], options),
    intBoundariesThickness: options => path(['map', 'intBoundariesThickness'], options),
    intBoundariesDisplay: options => path(['map', 'intBoundariesDisplay'], options),
    innerPadding: options => path(['map', 'innerPadding'], options),
    projection: options => path(['map', 'projection'], options)
  };
}