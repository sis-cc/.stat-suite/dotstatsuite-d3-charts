import d3 from 'd3';
import { setupElement } from '../utils/';

function areaUpdate(selection, accessors) {
  selection.attr({ 'data-testid': accessors.id });
}

function backgroundUpdate(selection, options, accessors) {
  selection
    .attr({ width: accessors.width(options), height: accessors.height(options) })
    .style({ fill: options.background.color });
}

function landEnter(selection, selectors) {
  setupElement(selection, 'svg:path', selectors.land);
}

function landUpdate(selection, options, accessors, projection) {
  selection.attr({
    d: d3.geo.path().projection(projection)
  });
  selection.style({
    fill: accessors.areaColor(options)
  })
}

function intBoundariesEnter(selection, selectors) {
  setupElement(selection, 'svg:path', selectors.intBoundaries);
}

function intBoundariesUpdate(selection, options, accessors, projection) {
  selection.attr({
    d: d3.geo.path().projection(projection)
  });
  selection.style({
    display: accessors.intBoundariesDisplay(options),
    fill: 'none',
    stroke: accessors.intBoundariesColor(options),
    'stroke-width': accessors.intBoundariesThickness(options)
  })
}

function extBoundariesEnter(selection, selectors) {
  setupElement(selection, 'svg:path', selectors.extBoundaries);
}

function extBoundariesUpdate(selection, options, accessors, projection) {
  selection.attr({
    d: d3.geo.path().projection(projection)
  });
  selection.style({
    display: accessors.extBoundariesDisplay(options),
    fill: 'none',
    stroke: accessors.extBoundariesColor(options),
    'stroke-width': accessors.extBoundariesThickness(options)
  })
}

export default function eauors(target) {
  target.eauors = {
    ...target.eauors,
    areaUpdate,
    backgroundUpdate,
    landEnter,
    landUpdate,
    intBoundariesEnter,
    intBoundariesUpdate,
    extBoundariesEnter,
    extBoundariesUpdate
  };
}
