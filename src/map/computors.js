import { bbox, feature, mesh } from 'topojson-client';
import d3 from 'd3';
import * as d3Geo from 'd3-geo';
import * as d3GeoProjections from 'd3-geo-projection';
import * as R from 'ramda';
import { DEFAULT_PROJECTION } from '../utils/constants';
import { computeOffset } from '../utils/computors';

const projectionsCollection = {
  ...d3Geo,
  ...d3GeoProjections
};

export default function computors(target) {
  target.computors = {
    ...target.computors,
    areas: data => feature(data, target.accessors.areas(data)),
    dataBbox: R.pipe(
      bbox,
      ([xA, yA, xB, yB]) => ([[xA, xB], [yA, yB]]),
      R.map(R.sortBy(R.identity)),
      ([[xMin, xMax], [yMin, yMax]]) => ([
        xMin < -180 || xMax > 180 ? [-180, 180] : [xMin, xMax],
        yMin < -90 || yMax > 90 ? [-90, 90] : [yMin, yMax],
      ])
    ),
    dataBboxCoordinates: (data, projection) => {
      const [[xMin, xMax], [yMin, yMax]] = target.computors.dataBbox(data);
      const geometry = {
        type: 'Feature',
        geometry: {
          type: 'MultiLineString',
          coordinates: [
            [[xMax, yMin], [xMax, yMax]],
            [[xMin, yMax], [xMin, yMin]],
            [[(xMin + xMax) / 2, yMin], [(xMin + xMax) / 2, yMax]]
          ]
        },
      };
      return d3.geo.path().projection(projection).bounds(geometry);
    },
    rawProjection: (options) => {
      const projectionKey = target.accessors.projection(options);
      return R.has(projectionKey, projectionsCollection) ?
        projectionsCollection[`${projectionKey}`] : projectionsCollection[`${DEFAULT_PROJECTION}`];
    },
    projection: (options, data) => {
      const rawProjection = target.computors.rawProjection(options);

      const width = target.accessors.width(options);
      const height = target.accessors.height(options);

      const innerPadding = target.accessors.innerPadding(options);
      const extent = [
        [innerPadding.left, innerPadding.top],
        [width - innerPadding.right, height - innerPadding.bottom]
      ];

      const optionsScale = target.accessors.scale(options);
      if (R.isNil(optionsScale)) {
        return rawProjection()
        .fitExtent(extent, target.computors.areas(data));
      }

      const projection = rawProjection().scale(optionsScale).translate([width / 2, height / 2]);

      const [[left, top], [right, bottom]] = target.computors.dataBboxCoordinates(data, projection);
      const horizontalOffset = computeOffset(width, left, right);
      const verticalOffset = computeOffset(height, top, bottom);

      return rawProjection()
        .scale(optionsScale)
        .translate([(width / 2) + horizontalOffset, (height / 2) + verticalOffset])
        .clipExtent(extent);

    },
    interiorBoundaries: data => mesh(data, target.accessors.areas(data), (a, b) => a !== b),
    exteriorBoundaries: data => mesh(data, target.accessors.areas(data), (a, b) => a === b),
    tooltipLayout: (serie, datum, color) => {
      const style = `
      font-size: 10px;
      font-family: sans-serif;
      color: ${color};
      padding: 5px;
      border: 1px solid ${color};
      background: white;
      opacity: .8;
    `;
    return `<div style="${style}">${R.pathOr(null, ['properties', 'label'], datum)}<br />${R.pathOr(null, ['properties', 'value'], datum)}</div>`;
    }
  };
}