import d3 from 'd3';
import { mergeDeepLeft } from 'ramda';

import BaseChart from '../base';

import * as utils from '../utils/';

import selectors from './selectors';
import accessors from './accessors';
import computors from './computors';
import eauors from './eauors';

@selectors
@accessors
@computors
@eauors
export default class MapChart extends BaseChart {
  constructor(el, options={}, data={}, getInitialOptions) {
    const tooltipOption = { serie: { tooltip: { layout: MapChart.computors.tooltipLayout } } };
    super(el, mergeDeepLeft(tooltipOption, options), data, getInitialOptions);
    this.map = _constructor(
      el,
      this.options,
      data,
      {
        selectors: this.constructor.selectors,
        accessors: this.constructor.accessors,
        computors: this.constructor.computors,
        eauors: this.constructor.eauors
      },
      this.base
    );
  }

  update(el, options={}, data={}, getInitialOptions) {
    const tooltipOption = { serie: { tooltip: { layout: MapChart.computors.tooltipLayout } } };
    super.update(el, mergeDeepLeft(tooltipOption, options), data, getInitialOptions);
    this.map = _update(
      el,
      this.options,
      data,
      {
        selectors: this.constructor.selectors,
        accessors: this.constructor.accessors,
        computors: this.constructor.computors,
        eauors: this.constructor.eauors
      },
      this.base
    );
  }
}

function _constructor(el, options, data, ors, base) {

  utils.noElError(el);

  d3.select(el)
    .select(utils.selector(ors.selectors.base))
    .call(utils.setupElement, 'svg:g', ors.selectors.map);

  d3.select(el)
    .select(utils.selector(ors.selectors.map))
    .call(utils.setupElement, 'svg:rect', ors.selectors.background);

  d3.select(el)
    .select(utils.selector(ors.selectors.map))
    .call(utils.setupElement, 'svg:g', ors.selectors.areas);

  return _update(el, options, data, ors, base);

}

function _update(el, options, data, ors, base) {

  if (process.env.NODE_ENV !== 'production') {
    console.time(`${utils.prefix} MapChart#_update`);
  }

  const map = d3.select(el).select(utils.selector(ors.selectors.map));

  map.select(utils.selector(ors.selectors.background))
    .call(ors.eauors.backgroundUpdate, options, ors.accessors);

  const projection = ors.computors.projection(options, data);

  const computedGeo = ors.computors.areas(data);
  const computedData = computedGeo.type === 'Feature' ? [computedGeo] : computedGeo.features;

  const areas = d3.select(el)
    .select(utils.selector(ors.selectors.areas))
    .selectAll(utils.selector(ors.selectors.area))
    .data(computedData);

  areas.enter().call(utils.setupElement, 'svg:g', ors.selectors.area);
  areas.call(ors.eauors.areaUpdate, ors.accessors);
  areas.exit().remove();

  const lands = areas
    .selectAll(utils.selector(ors.selectors.land))
    .data(data => [data]);

  lands.enter().call(ors.eauors.landEnter, ors.selectors);
  lands.call(ors.eauors.landUpdate, options, ors.accessors, projection);
  lands.exit().remove();

  const intBoundaries = map
    .selectAll(utils.selector(ors.selectors.intBoundaries))
    .data([ors.computors.interiorBoundaries(data)]);

  intBoundaries.enter().call(ors.eauors.intBoundariesEnter, ors.selectors);
  intBoundaries.call(ors.eauors.intBoundariesUpdate, options, ors.accessors, projection);
  intBoundaries.exit().remove();

  const extBoundaries = map
    .selectAll(utils.selector(ors.selectors.extBoundaries))
    .data([ors.computors.exteriorBoundaries(data)]);

  extBoundaries.enter().call(ors.eauors.extBoundariesEnter, ors.selectors);
  extBoundaries.call(ors.eauors.extBoundariesUpdate, options, ors.accessors, projection);
  extBoundaries.exit().remove();

  if (process.env.NODE_ENV !== 'production') {
    console.timeEnd(`${utils.prefix} MapChart#_update`);
  }

  return {
    projection
  };

}