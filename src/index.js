import meta from '../package.json';
console.info(`${meta.name}@${meta.version}`);

export BarChart from './bar/';
export RowChart from './row/';
export ScatterChart from './scatter/';
export LineChart from './line';
export TimelineChart from './timeline';
export VerticalSymbolChart from './vsymbol';
export HorizontalSymbolChart from './hsymbol';
export StackedBarChart from './stacked-bar';
export StackedRowChart from './stacked-row';
export ChoroplethChart from './choropleth';
export ChoroplethLegend from './choro-legend';
export computeOptions from './utils/options';
