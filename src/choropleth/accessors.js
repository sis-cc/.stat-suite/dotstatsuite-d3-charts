import { path, pathOr } from 'ramda';

export default function accessors(target) {
  target.accessors = {
    ...target.accessors,
    colors: options => path(['serie', 'choropleth', 'colors'], options),
    labelDisplay: options => path(['serie', 'choropleth', 'labelDisplay'], options),
    labelFont: options => path(['serie', 'choropleth', 'labelFont'], options),
    maxDomainOptions: options => path(['serie', 'choropleth', 'maxDomain'], options),
    minDomainOptions: options => path(['serie', 'choropleth', 'minDomain'], options),
    value: area => pathOr(null, ['properties', 'value'], area)
  }
}
