import d3 from 'd3';
import { curry } from 'ramda';
import { setupElement, fontStyle } from '../utils/';
import { TEXT_MIDDLE } from '../utils/constants';

function areaOver() {
  d3.select(this.parentNode).style({ opacity: 0.8 });
}

function areaOut() {
  d3.select(this.parentNode).style({ opacity: 1 });
}

function relevantAreasUpdate(selection, accessors, options, scale, base) {
  selection
    .on('mousemove', function(datum){
      base.showTooltip(
        options,
        base, // stands for { xScale, yScale }
        null, // serie ... ?
        datum,
        options.colors[0]
      );
    })
    .on('mouseleave', curry(base.hideTooltip))
    .on('mouseover', areaOver)
    .on('mouseout', areaOut)
    .style({
      fill: d => scale(accessors.value(d))
    });
}

function labelEnter(selection, selectors) {
  setupElement(selection, 'svg:text', selectors.label);
}

function labelUpdate(selection, options, accessors, projection) {
  selection.text(accessors.label);
  selection.attr({
    x: d => {
      const res = d3.geo.path().projection(projection).centroid(d)[0];
      if (isNaN(res)) {
        return null;
      }
      return res;
    },
    y: d => {
      const res = d3.geo.path().projection(projection).centroid(d)[1];
      if (isNaN(res)) {
        return null;
      }
      return res;
    }
  });
  selection.style({
    ...fontStyle(accessors.labelFont(options)),
    display: accessors.labelDisplay(options),
    'text-anchor': TEXT_MIDDLE
  });
}

export default function eauors(target) {
  target.eauors = {
    ...target.eauors,
    relevantAreasUpdate,
    labelEnter,
    labelUpdate,
  };
}