import MapChart from '../map';

import * as utils from '../utils';
import { pathOr } from 'ramda';

import selectors from './selectors';
import accessors from './accessors';
import computors from './computors';
import eauors from './eauors';

@selectors
@accessors
@computors
@eauors
export default class ChoroplethChart extends MapChart {
  constructor(el, options={}, data={}, getInitialOptions) {
    super(el, options, data);
    _update(
      el,
      this.options,
      data,
      {
        selectors: this.constructor.selectors,
        accessors: this.constructor.accessors,
        computors: this.constructor.computors,
        eauors: this.constructor.eauors
      },
      this.base,
      this.map
    );
  }

  update(el, options={}, data={}, getInitialOptions) {
    super.update(el, options, data, getInitialOptions);
    _update(
      el,
      this.options,
      data,
      {
        selectors: this.constructor.selectors,
        accessors: this.constructor.accessors,
        computors: this.constructor.computors,
        eauors: this.constructor.eauors
      },
      this.base,
      this.map
    );
  }
}

function _update(el, options={}, data, ors, base, map) {
  if (process.env.NODE_ENV !== 'production') {
    console.time(`${utils.prefix} ChoroplethChart#_update`);
  }

  const colorScale = ors.computors.colorScale(options, data);

  const relevantsAreas = d3.select(el)
    .selectAll(utils.selector(ors.selectors.land))
    .filter(d => pathOr(null, ['properties', 'value'], d))

  relevantsAreas.call(ors.eauors.relevantAreasUpdate, ors.accessors, options.serie, colorScale, base);

  if (ors.accessors.labelDisplay !== 'none') {
    const labels = d3.select(el)
      .select(utils.selector(ors.selectors.map))
      .selectAll(utils.selector(ors.selectors.area))
      .selectAll(utils.selector(ors.selectors.label))
      .data(data => [data]);

    labels.enter().call(ors.eauors.labelEnter, ors.selectors);
    labels.call(ors.eauors.labelUpdate, options, ors.accessors, map.projection);
    labels.exit().remove();
  }
  if (process.env.NODE_ENV !== 'production') {
    console.timeEnd(`${utils.prefix} ChoroplethChart#_update`);
  }
}