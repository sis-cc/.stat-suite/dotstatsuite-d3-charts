export default function selectors(target) {
  const chart = target.selectors.chart;

  target.selectors = {
    ...target.selectors,
    label: `${chart}__label`,
  };
}