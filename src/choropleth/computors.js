import d3 from 'd3';
import * as R from 'ramda';

export default function computors(target) {
  target.computors = {
    ...target.computors,
    values: data => R.pipe(
      target.accessors.areas,
      R.propOr([], 'geometries'),
      R.map(target.accessors.value),
      R.filter(R.identity)
    )(data),
    domain: (options, data) => R.pipe(
      target.computors.values,
      values => d3.extent(values),
      extent => {
        const minDomainOptions = target.accessors.minDomainOptions(options);
        const maxDomainOptions = target.accessors.maxDomainOptions(options);

        return [
          (!R.isNil(minDomainOptions) && minDomainOptions < domainValues[0]) ?
            minDomainOptions : extent[0],
          (!R.isNil(maxDomainOptions) && maxDomainOptions > domainValues[1]) ?
            maxDomainOptions : extent[1]
        ];
      }
    )(data),
    colorScale: (options, data) => d3.scale
      .quantize().domain(target.computors.domain(options, data))
      .range(target.accessors.colors(options))
  }
}