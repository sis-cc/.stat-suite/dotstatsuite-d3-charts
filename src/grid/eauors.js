import { head, last, includes } from 'lodash';

import * as utils from '../utils/';

function lineEnter(selection, options, selector) {
  selection
    .append('svg:line')
    .classed(selector, true)
    .style(utils.lineStyle({ ...options }));
}

const pivotLineStyle = (style) => (pivotOptions, lineOptions) => (datum) => {
  return pivotOptions.value === datum || includes(lineOptions.baselines, datum)
    ? pivotOptions[style]
    : lineOptions[style];
}

function xLineUpdate(selection, lineOptions, options, xScale, yRange) {
  const pivotOptions = options.axis.x.linear.pivot;

  selection
    .attr({
      x1: datum => xScale(datum),
      y1: head(yRange),
      x2: datum => xScale(datum),
      y2: last(yRange)
    })
    .style({
      'stroke-width': pivotLineStyle('thickness')(pivotOptions, lineOptions),
      'stroke': pivotLineStyle('color')(pivotOptions, lineOptions)
    });
}

function yLineUpdate(selection, lineOptions, options, yScale, xRange) {
  const pivotOptions = options.axis.y.linear.pivot;

  selection
    .attr({
      x1: head(xRange),
      y1: datum => yScale(datum),
      x2: last(xRange),
      y2: datum => yScale(datum)
    })
    .style({
      'stroke-width': pivotLineStyle('thickness')(pivotOptions, lineOptions),
      'stroke': pivotLineStyle('color')(pivotOptions, lineOptions)
    });
}

export default function eauors(target) {
  target.eauors = {
    ...target.eauors,
    xLineEnter: lineEnter,
    xLineUpdate,
    yLineEnter: lineEnter,
    yLineUpdate
  };
}
