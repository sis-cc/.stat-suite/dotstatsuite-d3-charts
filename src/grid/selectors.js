export default function selectors(target) {
  const grid = `${target.selectors.chart}__grid`;
  const line = `${target.selectors.chart}__line`;

  target.selectors = {
    ...target.selectors,
    xGrid: `${grid}--x`,
    yGrid: `${grid}--y`,
    xLine: `${line}--x`,
    yLine: `${line}--y`
  };
}
