import d3 from 'd3';
import { each } from 'lodash';

import AxisChart from '../axis/';

import * as utils from '../utils/';

import selectors from './selectors';
import accessors from './accessors';
import eauors from './eauors';

@selectors
@accessors
@eauors
export default class GridChart extends AxisChart {
  constructor(el, options={}, data=[], getInitialOptions) {
    super(el, options, data, getInitialOptions);
    _contructor(
      el,
      this.options,
      data,
      {
        selectors: this.constructor.selectors,
        accessors: this.constructor.accessors,
        eauors: this.constructor.eauors
      },
      this.base,
      this.axis
    );
  }

  update(el, options={}, data=[], getInitialOptions) {
    super.update(el, options, data, getInitialOptions);
    _update(
      el,
      this.options,
      data,
      {
        selectors: this.constructor.selectors,
        accessors: this.constructor.accessors,
        eauors: this.constructor.eauors
      },
      this.base,
      this.axis
    );
  }
}

function _contructor(el, options, data, ors, base, axis) {
  //console.info(utils.prefix, '\t\t', 'GridChart', '_contructor', '>>');

  utils.noElError(el);

  d3.select(el)
    .select(utils.selector(ors.selectors.base))
    .call(utils.setupElement, 'svg:g', ors.selectors.xGrid)
    .call(utils.setupElement, 'svg:g', ors.selectors.yGrid);

  _update(el, options, data, ors, base, axis);

  //console.info(utils.prefix, '\t\t', 'GridChart', '_contructor', '<<');
}

function _update(el, options, data, ors, base, axis) {
  //console.info(utils.prefix, '\t\t', 'GridChart', '_update', '>>');

  utils.noElError(el);
  if (utils.noDataError(ors.accessors.data(data), '\t\t', 'GridChart')) {
    d3.select(el).select(utils.selector(ors.selectors.base))
      .selectAll(utils.selector(ors.selectors.xLine))
      .remove();
    d3.select(el).select(utils.selector(ors.selectors.base))
      .selectAll(utils.selector(ors.selectors.yLine))
      .remove();
    return;
  }

  each(['x', 'y'], prefix => {
    const gridSelector = ors.selectors[`${prefix}Grid`];
    const lineSelector = ors.selectors[`${prefix}Line`];
    const lineOptions = options.grid[prefix];
    const lineData = ors.accessors[`${prefix}Ticks`](base, axis, options.axis.x);
    const lineScale = ors.accessors[`${prefix}Scale`](base, axis);
    const lineRange = ors.accessors[`${prefix}Range`](base, axis);
    const lineEnter = ors.eauors[`${prefix}LineEnter`];
    const lineUpdate = ors.eauors[`${prefix}LineUpdate`];

    const lineJoin = d3.select(el).select(utils.selector(ors.selectors.base))
      .select(utils.selector(gridSelector))
      .selectAll(utils.selector(lineSelector))
      .data(lineData);

    lineJoin.enter().call(lineEnter, lineOptions, lineSelector);
    lineJoin.call(lineUpdate, lineOptions, options, lineScale, lineRange);
    lineJoin.exit().remove();
  });

  //console.info(utils.prefix, '\t\t', 'GridChart', '_update', '<<');
}
