export default function accessors(target) {
  target.accessors = {
    ...target.accessors,
    xTicks: (base, axis) => axis.xAxis.tickValues(),
    yTicks: (base, axis) => axis.yAxis.tickValues(),
    xRange: (base, axis) => axis.yAxis.scale().range(),
    yRange: (base, axis) => axis.xAxis.scale().range(),
    xScale: (base, axis) => axis.xAxis.scale(),
    yScale: (base, axis) => axis.yAxis.scale(),
  };
}
