import { map } from 'lodash';

import { computeOrdinalScale, computeOrdinalAxis } from '../utils/computors';

export default function computors(target) {
  const annotationData = target.computors.annotationData;
  target.computors = {
    ...target.computors,
    xValues: data => map(target.computors.data(data), target.accessors.x),
    xScale: (options, data, offset) => {
      return computeOrdinalScale(
        options.axis.x.ordinal,
        target.computors.xRange(options, offset),
        target.computors.xValues(data)
      );
    },
    xAxis: (scale, pivot, options) => computeOrdinalAxis(scale, options),
    annotationData: (options, scale, datum) => {
      const limitWidth = scale.rangeBand();
      if (limitWidth < options.minWidth) {
        return [];
      }
      return annotationData(options.display, scale);
    }
  };
}