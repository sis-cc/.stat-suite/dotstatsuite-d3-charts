import d3 from 'd3';
import { curry } from 'lodash';
import { is } from 'ramda';

import BackgroundChart from '../background/';

import * as utils from '../utils/';
import { NEVER } from '../utils/constants';

import selectors from './selectors';
import accessors from './accessors';
import computors from './computors';
import eauors from './eauors';
import { getLinearAxisOptions } from '../utils/getAxisOptions';

@selectors
@accessors
@computors
@eauors
export default class BarChart extends BackgroundChart {
  constructor(el, options={}, data=[], getInitialOptions, getAxisOptions) {
    super(el, options, data, getInitialOptions);
    _contructor(
      el,
      this.options,
      data,
      {
        selectors: this.constructor.selectors,
        accessors: this.constructor.accessors,
        computors: this.constructor.computors,
        eauors: this.constructor.eauors
      },
      this.base,
      this.axis
    );
  }

  update(el, options={}, data=[], getInitialOptions, getAxisOptions) {
    super.update(el, options, data, getInitialOptions);
    _update(
      el,
      this.options,
      data,
      {
        selectors: this.constructor.selectors,
        accessors: this.constructor.accessors,
        computors: this.constructor.computors,
        eauors: this.constructor.eauors
      },
      this.base,
      this.axis,
      getAxisOptions
    );
  }
}

function _contructor(el, options, data, ors, base, axis) {
  //console.info(utils.prefix, '\t\t\t\t', 'BarChart', '_contructor', '>>');

  utils.noElError(el);

  d3.select(el)
    .select(utils.selector(ors.selectors.base))
    .call(utils.setupElement, 'svg:g', ors.selectors.serie)

  _update(el, options, data, ors, base, axis);

  //console.info(utils.prefix, '\t\t\t\t', 'BarChart', '_contructor', '<<');
}

function _update(el, options, data, ors, base, axis, getAxisOptions) {
  //console.info(utils.prefix, '\t\t\t\t', 'BarChart', '_update', '>>');
  if (process.env.NODE_ENV !== 'production') {
    console.time(`${utils.prefix} BarChart#_update`);
  }

  utils.noElError(el);
  if (utils.noDataError(ors.accessors.data(data), '\t\t\t\t', 'BarChart')) {
    d3.select(el).selectAll(utils.selector(ors.selectors.bar)).remove();
    return;
  }

  const scales = {
    xScale: ors.accessors.xScale(base, axis),
    yScale: ors.accessors.yScale(base, axis)
  };

  const pivots = { xPivot: base.xPivot, yPivot: base.yPivot };

  d3.select(el)
    .selectAll(utils.selector(ors.selectors.serie))
    .data(data);

  const barJoin = d3.select(el)
    .select(utils.selector(ors.selectors.serie))
    .selectAll(utils.selector(ors.selectors.bar))
    .data(ors.computors.data(data));
  barJoin.enter().call(ors.eauors.barEnter, options.serie, ors.selectors.bar);
  barJoin.call(ors.eauors.barUpdate, options.serie, ors.accessors);
  barJoin.exit().remove();

  const rectJoin = barJoin.selectAll(utils.selector(ors.selectors.rect)).data(datum => [datum]);
  rectJoin.enter().call(ors.eauors.rectEnter, options.serie, ors.selectors, ors.accessors, base.hideTooltip);
  rectJoin.call(ors.eauors.rectUpdate, options, ors.accessors, scales, pivots, base.showTooltip);
  rectJoin.exit().remove();

  if (options.serie.annotation.display !== NEVER) {
    const annotationJoin = barJoin
      .selectAll(utils.selector(ors.selectors.annotation))
      .data(curry(ors.computors.annotationData)(options.serie.annotation, scales.xScale));
    annotationJoin.enter().call(ors.eauors.annotationEnter, options.serie, ors.selectors.annotation);
    annotationJoin.call(ors.eauors.annotationUpdate, options, ors.accessors, scales, pivots);
    annotationJoin.exit().remove();
  }

  const xAxisLabelJoin = d3.select(el)
    .select(utils.selector(ors.selectors.xAxis))
    .selectAll('text')
    .data(ors.computors.data(data));
  xAxisLabelJoin.call(ors.eauors.xAxisLabelUpdate, options.serie, ors.accessors);

  if (is(Function, getAxisOptions)) {
    const yAxisoptions = getLinearAxisOptions(axis.yAxis, options.axis.y);
    getAxisOptions({
      computedMinY: yAxisoptions.min,
      computedMaxY: yAxisoptions.max,
      computedStepY: yAxisoptions.step,
      computedPivotY: yAxisoptions.pivot
    });
  }
  //console.info(utils.prefix, '\t\t\t\t', 'BarChart', '_update', '<<');
  if (process.env.NODE_ENV !== 'production') {
    console.timeEnd(`${utils.prefix} BarChart#_update`);
  }
}
