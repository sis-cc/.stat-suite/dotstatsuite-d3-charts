import { get, head } from 'lodash';
import * as utils from '../utils/';

export default function accessors(target) {
  target.accessors = {
    ...target.accessors,
    data: data => get(head(data), 'datapoints', []),
    xTicks: (base, axis) => axis.xAxis.scale().domain(),
    yRange: (base, axis) => base.xScale.rangeExtent(),
    annotation: (options, datum) => utils.getFormat(options.format.datapoint)(datum.y),
  };
}
