import {
  setupElement, getColor, getOverColor, fontStyle, getElementDimensions, fontBaselineOffset
} from '../utils/';
import * as C from '../utils/constants';
import { curry, gt, min, max } from 'lodash';

function barEnter(selection, options, selector) {
  setupElement(selection, 'svg:g', selector);
}

function barUpdate(selection, options, accessors) {
  selection.style({ fill: datum => getColor(options, accessors.focusData(datum)) });
}

export function rectHeight(pivot, scale, accessor, datum) {
  return Math.abs(scale(accessor(datum)) - scale(pivot));
}

export function rectOrigin(pivot, scale, accessor, datum) {
  return scale(Math.max(accessor(datum), pivot));
}

export function rectOver(options, accessors, datum) {
  d3.select(this.parentNode).style({ fill: datum => getOverColor(options, accessors.focusData(datum)) });
}

export function rectOut(options, accessors, datum) {
  d3.select(this.parentNode).style({ fill: datum => getColor(options, accessors.focusData(datum)) });
}

function rectEnter(selection, options, selectors, accessors, hideTooltip) {
  setupElement(selection, 'svg:rect', selectors.rect)
    .on('mouseover', curry(rectOver)(options, accessors))
    .on('mouseout', curry(rectOut)(options, accessors))
    .on('mouseleave', curry(hideTooltip));
}

function rectUpdate(selection, options, accessors, { xScale, yScale }, { xPivot, yPivot }, showTooltip) {
  selection.attr({
    width: xScale.rangeBand(),
    height: curry(rectHeight)(yPivot, yScale, accessors.y),
    x: datum => xScale(accessors.x(datum)),
    y: curry(rectOrigin)(yPivot, yScale, accessors.y)
  })
  .on('mousemove', function(datum) {
    const serie = d3.select(this.parentNode.parentNode).datum();
    showTooltip(options.serie, { xScale, yScale }, serie, datum);
  });
}

export function annotationAlign(scale, accessor, datum) {
  return scale(accessor(datum)) + scale.rangeBand() / 2;
}

function annotationEnter(selection, options, selector) {
  setupElement(selection, 'svg:text', selector);
}

function annotationBaseline(datum, pivot, scale, accessor, position) {
  switch(position) {
    case 'above':
      return rectOrigin(pivot, scale, accessor, datum);
    case 'below':
      return rectOrigin(pivot, scale, accessor, datum) + rectHeight(pivot, scale, accessor, datum);
    default:
      return null;
  }
}

function rangeLimits(options, scale, position) {
  const range = options.axis.x.orient === C.BOTTOM ? [0, max(scale.range()) + options.base.innerPadding.top]
    : [min(scale.range()) - options.base.innerPadding.bottom, options.base.height];
  // take this instead of just scale.range() to have access to all space available, including paddings, innerPaddings ...
  switch(position) {
    case 'above':
      return range;
    case 'below':
      return [range[1], range[0]];
    default:
      return null;
  }
}

function annotationOffset(position, adjustment, textHeight, rectHeight, margin) {
  if (position === 'above') {
    switch (adjustment) {
      case 'adjacent':
        return -1 * margin;
      case 'inner':
        return margin + textHeight;
      case 'opposite':
        return margin + textHeight + rectHeight;
      default:
        return null;
    }
  }
  else if (position === 'below') {
    switch (adjustment) {
      case 'adjacent':
        return margin + textHeight;
      case 'inner':
        return -1 * margin;
      case 'opposite':
        return -1 * margin - rectHeight;
      default:
        return null;
    }
  }
  return null;
}

function hideAnnotation(selection) {
  selection.style({ display: C.NOT_DISPLAYED });
}

function annotationAdjustment(selection, options, accessors, scale, pivot, datum, position) {
  // once the default placement of annotation is done (above or below rect) and first render to 
  // gain access to text dimensions, apply adjustment to place text properly, regarding the available space.
  // exemple for above: check available space above the rect: adjacent case. if no space, check inside the
  // rect: inner case. if no space, checks then the space below the rect: opposite case. finally if no space
  // at all or width longer than rect width hide text completely
  const textHeight = fontBaselineOffset(selection, C.TEXT_BELOW);
  const margin = options.serie.annotation.margin;
  const [adjRangeLimit, opRangeLimit] = rangeLimits(options, scale, position);
  const recHeight = rectHeight(pivot, scale, accessors.y, datum);
  const baseline = annotationBaseline(datum, pivot, scale, accessors.y, position);
  const oppositeBaseline = (position === 'above') ? (baseline + recHeight) : baseline - recHeight;
  let adjustment = null;
  if (Math.abs(adjRangeLimit - baseline) > margin + textHeight) {
    adjustment = 'adjacent';
  }
  else if (recHeight > margin + textHeight) {
    adjustment = 'inner';
  }
  else if (Math.abs(opRangeLimit - oppositeBaseline) > margin + textHeight) {
    adjustment = 'opposite';
  }
  if (!adjustment) {
    return hideAnnotation(selection);
  }
  const offset = annotationOffset(position, adjustment, textHeight, recHeight, margin);
  selection.attr({ transform: `translate(0, ${offset})`});
  if (adjustment === 'inner') {
    selection.style({ fill: options.serie.annotation.font.innerColor });
  }
}

function getMaxAnnotationWidth(selection) {
  let result = 0;
  selection.each(function annotationWidth() {
    const elem = d3.select(this);
    const width = getElementDimensions(elem).width;
    if (width > result) {
      result = width;
    }
  });
  return result;
}

function annotationUpdate(selection, options, accessors, { xScale, yScale }, { xPivot, yPivot }) {
  function annotationDefault(datum) {
    let position = null;
    const _selection = d3.select(this);
    if (gt(yScale(accessors.y(datum)), yScale(yPivot))) {
      position = 'below';
    }
    else {
      position = 'above';
    }
    const baseline = annotationBaseline(datum, yPivot, yScale, accessors.y, position);
    _selection
      .text(curry(accessors.annotation)(options.serie.annotation))
      .attr({
        x: curry(annotationAlign)(xScale, accessors.x),
        y: baseline
      })
      .style({
        ...fontStyle(options.serie.annotation.font),
        display: C.DISPLAYED,
        // default display is needed, in case of previous hiding
        'text-anchor': C.TEXT_MIDDLE
      });
      annotationAdjustment(_selection, options, accessors, yScale, yPivot, datum, position);
  }

  selection.each(annotationDefault);
  const maxAnnotationWidth = getMaxAnnotationWidth(selection);
  if (maxAnnotationWidth > xScale.rangeBand()) {
    hideAnnotation(selection);
  }
}

export default function eauors(target) {
  target.eauors = {
    ...target.eauors,
    barEnter,
    barUpdate,
    rectEnter,
    rectUpdate,
    annotationEnter,
    annotationUpdate
  };
}
