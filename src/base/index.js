import d3 from 'd3';
import { cloneDeep, curry, head, isFunction, isNil, max } from 'lodash';

import * as utils from '../utils/';
import { NEVER, TOOLTIP_CURSOR_X_OFFSET, TOOLTIP_CURSOR_Y_OFFSET, VISIBLE, HIDDEN } from '../utils/constants';
import prepareOptions from '../utils/options';

import selectors from './selectors';
import accessors from './accessors';
import computors from './computors';
import eauors from './eauors';

@selectors
@computors
@accessors
@eauors
export default class BaseChart {
  constructor(el, options={}, data=[], getInitialOptions) {
    this.options = prepareOptions(options, el);
    if (isFunction(getInitialOptions)) getInitialOptions(cloneDeep(this.options));
    this.base = _contructor(
      el,
      this.options,
      data,
      {
        accessors: this.constructor.accessors,
        selectors: this.constructor.selectors,
        computors: this.constructor.computors,
        eauors: this.constructor.eauors
      }
    );
  }

  update(el, options={}, data=[], getInitialOptions) {
    this.options = prepareOptions(options);
    if (isFunction(getInitialOptions)) {
      getInitialOptions(cloneDeep(this.options));
    }
    this.base = _update(
      el,
      this.options,
      data,
      {
        accessors: this.constructor.accessors,
        selectors: this.constructor.selectors,
        computors: this.constructor.computors,
        eauors: this.constructor.eauors
      }
    );
  }

  destroy(el) {
    d3.selectAll(el).remove();
  }
}

function _contructor(el, options, data, ors) {
  //console.info(utils.prefix, 'BaseChart', '_contructor', '>>');

  utils.noElError(el);

  d3.select(el)
    .call(utils.setupElement, 'div', ors.selectors.wrapper)
    .select(utils.selector(ors.selectors.wrapper))
    .call(utils.setupElement, 'svg:svg', ors.selectors.chart)
    .select(utils.selector(ors.selectors.chart))
    .call(utils.setupElement, 'svg:g', ors.selectors.base)
    .select(utils.selector(ors.selectors.base));

  if (options.serie.tooltip.display !== NEVER) {
    //console.info(utils.prefix, 'BaseChart', '_contructor', '--', 'injectTooltip');
    let tooltip = d3.select(document.body)
      .select(utils.selector(ors.selectors.tooltip));

      if (isNil(head(head(tooltip)))) {
        d3.select(document.body)
          .call(utils.setupElement, 'div', ors.selectors.tooltip)
      } 

    d3.select(document.body)
      .select(utils.selector(ors.selectors.tooltip))
      .call(ors.eauors.tooltipUpdate, options);
  }

  //console.info(utils.prefix, 'BaseChart', '_contructor', '<<');

  return _update(el, options, data, ors);
}

function _update(el, options, data, ors) {
  //console.info(utils.prefix, 'BaseChart', '_update', '>>');

  utils.noElError(el);
  
  d3.select(el)
    .select(utils.selector(ors.selectors.wrapper))
    .call(ors.eauors.wrapperUpdate, options.base)
    .select(utils.selector(ors.selectors.chart))
    .call(ors.eauors.chartUpdate, options.base)
    .select(utils.selector(ors.selectors.base))
    .call(ors.eauors.containerUpdate, options.base);

    const xScale = ors.computors.xScale({ ...options, fromBase: true }, data);
    const yScale = ors.computors.yScale(options, data);

    const width = options.base.width;
    const height = options.base.height;

  function showTooltip(options, scales, serie, datum, color) {
    const element = d3.select(document.body).select(utils.selector(ors.selectors.tooltip));

    const _color = color || utils.getOverColor(options, ors.accessors.focusData(datum));
    element.html(options.tooltip.layout(serie, datum, _color));

    const limitX = width;
    const limitY = height;

    function placementEngine(element, { limitX, limitY }) {
      const [ mouseX, mouseY ] = d3.mouse(d3.select(el).select(utils.selector(ors.selectors.chart)).node());

      const [absMouseX, absMouseY] = d3.mouse(d3.select(document.body).node());

      const { width, height } = element.node().getBoundingClientRect();

      const left = (limitX >= mouseX + width + TOOLTIP_CURSOR_X_OFFSET || limitX - mouseX >= mouseX)
        ? absMouseX + TOOLTIP_CURSOR_X_OFFSET
        : max([absMouseX - width - TOOLTIP_CURSOR_X_OFFSET, 0]);

      const top = (limitY >= mouseY + height + TOOLTIP_CURSOR_Y_OFFSET || limitY - mouseY >= mouseY)
        ? absMouseY + TOOLTIP_CURSOR_Y_OFFSET
        : max([absMouseY - height - TOOLTIP_CURSOR_Y_OFFSET, 0]);

      element.style({
        position: 'absolute',
        left: `${left}px`,
        top: `${top}px`,
        visibility: VISIBLE,
        "z-index": options.tooltip.zIndex
      });
    };

    placementEngine(element, { limitX, limitY });
  }

  function hideTooltip(partial) {
    d3.select(document.body).select(utils.selector(ors.selectors.tooltip)).style({ visibility: HIDDEN });
  }

  //console.info(utils.prefix, 'BaseChart', '_update', '<<');
  
  return {
    xScale,
    yScale,
    xPivot: ors.computors.xPivot(options, data),
    yPivot: ors.computors.yPivot(options, data),
    hideTooltip,
    showTooltip
  };
}
