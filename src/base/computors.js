import {
  head, last, isNull, min as _min, max as _max, every, gt, lt, flatten, map, get,
  isArray, floor, ceil } from 'lodash';
import d3 from 'd3';

import { NEVER, HIGHLIGHT, ALWAYS, BASELINE, FOCUS } from '../utils/constants';

import * as utilsComputors from '../utils/computors';

export function computePivot(options, data, datumAccessor) {
  const pivot = options.linear.pivot.value;
  if (!isNull(pivot)) return pivot;
  const extent = d3.extent(data, datumAccessor);
  if (every(extent, e => gt(e, 0))) return ceil(head(extent));
  else if (every(extent, e => lt(e, 0))) return ceil(last(extent));
  return 0;
}

export default function computors(target) {
  target.computors = {
    ...target.computors,
    xRange: options => [0, options.base.width],
    yRange: options => [options.base.height, 0],
    xDomain: (options, data) => {
      return utilsComputors.computeLinearDomain(
        options.axis.x,
        target.computors.data(data),
        target.accessors.x
      );
    },
    yDomain: (options, data) => {
      return utilsComputors.computeLinearDomain(
        options.axis.y,
        target.computors.data(data),
        target.accessors.y
      );
    },
    xScale: (options, data) => {
      return utilsComputors.computeLinearScale(
        target.computors.xRange(options),
        target.computors.xDomain(options, data)
      );
    },
    yScale: (options, data) => {
      return utilsComputors.computeLinearScale(
        target.computors.yRange(options),
        target.computors.yDomain(options, data)
      );
    },
    data: data => flatten(map(data, serie => get(serie, 'datapoints', []))),
    annotationData: (display, datum) => {
      switch(display) {
        case NEVER: return [];
        case HIGHLIGHT: return target.accessors.isHighlighted(datum) ? [datum] : [];
        case BASELINE: return target.accessors.isABaseline(datum) ? [datum] : [];
        case FOCUS: return target.accessors.isFocused(datum) ? [datum] : [];
        case ALWAYS: default: return [datum];
      }
    },
    xPivot: (options, data) => {
      return computePivot(
        options.axis.x,
        target.computors.data(data),
        target.accessors.x
      );
    },
    yPivot: (options, data) => {
      return computePivot(
        options.axis.y,
        target.computors.data(data),
        target.accessors.y
      );
    }
  };
}