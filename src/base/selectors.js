export default function selectors(target) {
  const prefix = 'rcw-chart';

  target.selectors = {
    ...target.selectors,
    wrapper: `${prefix}__wrapper`,
    chart: `${prefix}__chart`,
    tooltip: `${prefix}__tooltip`,
    base: `${prefix}__base`
  };
}
