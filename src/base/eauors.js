import { HIDDEN } from '../utils/constants';

function wrapperUpdate(selection, options) {
  selection.style({
    position: 'relative',
    // the width should not be a constraint at this level
    // width: options.width+'px',
    height: options.outerHeight+'px',
  });
}

function chartUpdate(selection, options) {
  selection.attr({
    width: '100%',
    height: '100%',
  });
}

function containerUpdate(selection, options) {
  selection.attr({
    width: options.width,
    height: options.height,
    transform: `translate(${options.margin}, ${options.margin})`
  });
}

function tooltipUpdate(selection, options) {
  selection.style({ visibility: HIDDEN });
}

export default function eauors(target) {
  target.eauors = {
    ...target.eauors,
    wrapperUpdate,
    chartUpdate,
    containerUpdate,
    tooltipUpdate
  };
}
