import { get, gte, some, identity } from 'lodash';
import {
  HIGHLIGHT, DEFAULT_HIGHLIGHT_INDEX,
  BASELINE, DEFAULT_BASELINE_INDEX
} from '../utils/constants';

export default function accessors(target) {
  target.accessors = {
    ...target.accessors,
    x: datum => datum.x,
    y: datum => datum.y,
    highlightIndex: datum => get(datum, 'highlightIndex', DEFAULT_HIGHLIGHT_INDEX),
    isHighlighted: datum => gte(datum.highlightIndex, 0),
    baselineIndex: datum => get(datum, 'baselineIndex', DEFAULT_BASELINE_INDEX),
    isABaseline: datum => gte(datum.baselineIndex, 0),
    isFocused: datum => {
      return some([
        target.accessors.isHighlighted(datum),
        target.accessors.isABaseline(datum)
      ], identity);
    },
    focusData: datum => {
      if (target.accessors.isABaseline(datum)) 
        return { index: target.accessors.baselineIndex(datum), type: BASELINE };
      else if (target.accessors.isHighlighted(datum))
        return { index: target.accessors.highlightIndex(datum), type: HIGHLIGHT };
      return { index: 0 };
    }
  };
}
