export default function selectors(target) {
  const chart = target.selectors.chart;

  target.selectors = {
    ...target.selectors,
    background: `${chart}__background`
  };
}
