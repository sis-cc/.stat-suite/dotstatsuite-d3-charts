import d3 from 'd3';
import { gt } from 'lodash';
import * as utils from '../utils/';
import { BOTTOM } from '../utils/constants';

function backgroundEnter(selection, selector, selectors) {
  selection.insert('svg:rect', ':first-child').classed(selector, true);
}

function backgroundUpdate(selection, options, el, selectors) {
  const xAxisOrient = options.axis.x.orient;
  const xAxisTickSize = options.axis.x.tick.size;
  const xAxis = d3.select(el).select(utils.selector(selectors.xAxis));
  const xAxisHeight = xAxis.empty() ? 0 : utils.getElementDimensions(xAxis).height;
  
  const height = xAxisOrient === BOTTOM
    ? options.base.height - xAxisHeight - (gt(xAxisTickSize, 0) ? 0 : xAxisTickSize)
    : options.base.height;
  
  selection
    .attr({ width: options.base.width, height: gt(height, 0) ? height : 0 })
    .style({ fill: options.background.color });
}

export default function eauors(target) {
  target.eauors = {
    ...target.eauors,
    backgroundEnter,
    backgroundUpdate
  };
}
