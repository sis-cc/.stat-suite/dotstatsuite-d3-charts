import d3 from 'd3';

import GridChart from '../grid/';

import * as utils from '../utils/';

import selectors from './selectors';
import eauors from './eauors';

@selectors
@eauors
export default class BackgroundChart extends GridChart {
  constructor(el, options={}, data=[], getInitialOptions) {
    super(el, options, data, getInitialOptions);
    _contructor(
      el,
      this.options,
      data,
      {
        selectors: this.constructor.selectors,
        eauors: this.constructor.eauors
      },
      this.base,
      this.axis
    );
  }

  update(el, options={}, data=[], getInitialOptions) {
    super.update(el, options, data, getInitialOptions);
    _update(
      el,
      this.options,
      data,
      {
        selectors: this.constructor.selectors,
        eauors: this.constructor.eauors
      },
      this.base,
      this.axis
    );
  }
}

function _contructor(el, options, data, ors, base, axis) {
  //console.info(utils.prefix, '\t\t\t', 'BackgroundChart', '_contructor', '>>');

  utils.noElError(el);

  d3.select(el)
    .select(utils.selector(ors.selectors.base))
    .call(ors.eauors.backgroundEnter, ors.selectors.background, ors.selectors);

  _update(el, options, data, ors, base, axis);

  //console.info(utils.prefix, '\t\t\t', 'BackgroundChart', '_contructor', '<<');
}

function _update(el, options, data, ors, base, axis) {
  //console.info(utils.prefix, '\t\t\t', 'BackgroundChart', '_update', '>>');

  utils.noElError(el);

  d3.select(el)
    .select(utils.selector(ors.selectors.base))
    .select(utils.selector(ors.selectors.background))
    .call(ors.eauors.backgroundUpdate, options, el, ors.selectors);

  //console.info(utils.prefix, '\t\t\t', 'BackgroundChart', '_update', '<<');
}
