import BarChart from '../bar';
import accessors from './accessors';
import computors from './computors';
import eauors from './eauors';

import * as utils from '../utils/';

@accessors
@computors
@eauors
export default class StackedBarChart extends BarChart {
  constructor(el, options={}, data=[], getInitialOptions, getAxisOptions) {
    const _data = StackedBarChart.computors.stackedData(data);
    const _options = StackedBarChart.computors.computeOptions(options, data);
    super(el, _options, _data, getInitialOptions, getAxisOptions);
    _constructor(
      el,
      this.options,
      data,
      {
        selectors: this.constructor.selectors,
        accessors: this.constructor.accessors,
        computors: this.constructor.computors,
        eauors: this.constructor.eauors
      }
    )
  }

  update(el, options={}, data=[], getInitialOptions, getAxisOptions) {
    const _data = StackedBarChart.computors.stackedData(data);
    const _options = StackedBarChart.computors.computeOptions(options, data);
    super.update(el, _options, _data, getInitialOptions, getAxisOptions);
    _update(
    el,
      this.options,
      data,
      {
        selectors: this.constructor.selectors,
        accessors: this.constructor.accessors,
        computors: this.constructor.computors,
        eauors: this.constructor.eauors
      }
    )
  }
}

function _constructor(el, options, data, ors){
  utils.noElError(el);
  _update(el, options, data, ors);
}

function _update(el, options, data, ors){
  
  const xAxisLabelJoin = d3.select(el)
    .select(utils.selector(ors.selectors.xAxis))
    .selectAll('text')
    .data(ors.computors.nonStackedData(data));
  xAxisLabelJoin.call(ors.eauors.xAxisLabelUpdate, options.serie, ors.accessors);

  // multi bars per serie caused a bad data binding in bar chart, this is needed to have a good label serie coloring ...
}