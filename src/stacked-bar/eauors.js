import { curry } from 'lodash';
import d3 from 'd3';
import { getColor, getColorShade, getOverColor, setupElement } from '../utils';

function barUpdate(selection, options, accessors) {
  selection.style({ fill: datum =>
    getColorShade(getColor(options, accessors.focusData(datum)), accessors.colorCoeff(datum, options))
  });
}

function rectOrigin(pivot, scale, accessor, datum) {
  return scale(accessor(datum));
}

function rectHeight(pivot, scale, datum) {
  return Math.abs(scale(Math.abs(datum.height)) - scale(pivot));
}

function rectOver(options, accessors, datum) {
  d3.select(this.parentNode).style({
    fill: datum => getColorShade(getOverColor(options, accessors.focusData(datum)), accessors.colorCoeff(datum, options))
  });
}

function rectOut(options, accessors, datum) {
  d3.select(this.parentNode).style({
    fill: datum => getColorShade(getColor(options, accessors.focusData(datum)), accessors.colorCoeff(datum, options))
  });
}

function rectEnter(selection, options, selectors, accessors, hideTooltip) {
  setupElement(selection, 'svg:rect', selectors.rect)
    .on('mouseleave', curry(hideTooltip));
}

function rectUpdate(selection, options, accessors, { xScale, yScale }, { xPivot, yPivot }, showTooltip) {
  selection.attr({
    width: xScale.rangeBand(),
    height: curry(rectHeight)(yPivot, yScale),
    x: datum => xScale(accessors.x(datum)),
    y: curry(rectOrigin)(yPivot, yScale, d => d.height >= 0 ? d.y : d.y0)
  });
  selection.style({
    'stroke-width': accessors.rectStrokeThickness(options.serie),
    stroke: accessors.rectStrokeColor(options.serie),
    'shape-rendering': 'crispEdges'
  });
  selection
    .on('mouseover', curry(rectOver)(options.serie, accessors))
    .on('mouseout', curry(rectOut)(options.serie, accessors))
    .on('mousemove', function(datum) {
      const serie = d3.select(this.parentNode.parentNode).datum();
      showTooltip(options.serie, { xScale, yScale }, serie, datum);
    });
}

export default function eauors(target) {
  target.eauors = {
    ...target.eauors,
    barUpdate,
    rectEnter,
    rectUpdate
  };
}