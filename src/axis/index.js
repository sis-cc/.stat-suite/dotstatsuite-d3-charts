import d3 from 'd3';
import { curry, merge } from 'lodash';

import BaseChart from '../base/';

import * as utils from '../utils/';
import { LEFT, ORDINAL, TIME, ROTATED, UNROTATED } from '../utils/constants';
import { reduceAxisPadding, hideAxisLabels } from '../utils/options';

import selectors from './selectors';
import accessors from './accessors';
import computors from './computors';
import eauors from './eauors';

@selectors
@computors
@accessors
@eauors
export default class AxisChart extends BaseChart {
  constructor(el, options={}, data=[], getInitialOptions) {
    super(el, options, data, getInitialOptions);
    this.axis = _contructor(
      el,
      this.options,
      data,
      {
        selectors: this.constructor.selectors,
        accessors: this.constructor.accessors,
        computors: this.constructor.computors,
        eauors: this.constructor.eauors
      },
      this.base
    );
  }

  update(el, options={}, data=[], getInitialOptions) {
    super.update(el, options, data, getInitialOptions);
    this.axis = _update(
      el,
      this.options,
      data,
      {
        selectors: this.constructor.selectors,
        accessors: this.constructor.accessors,
        computors: this.constructor.computors,
        eauors: this.constructor.eauors
      },
      this.base
    );
  }
}

function _contructor(el, options, data, ors, base) {
  //console.info(utils.prefix, '\t', 'AxisChart', '_contructor', '>>');
  utils.noElError(el);

  d3.select(el)
    .select(utils.selector(ors.selectors.base))
    .call(utils.setupElement, 'svg:g', ors.selectors.xAxis)
    .call(utils.setupElement, 'svg:g', ors.selectors.yAxis);

  //console.info(utils.prefix, '\t', 'AxisChart', '_contructor', '<<');

  return _update(el, options, data, ors, base);
}

function _update(el, options, data, ors, base, adjusting) {
  //console.info(utils.prefix, '\t', 'AxisChart', '_update', '>>');

  utils.noElError(el);
  if (utils.noDataError(ors.accessors.data(data), '\t', 'AxisChart')) {
    d3.select(el).select(utils.selector(ors.selectors.xAxis)).selectAll('.tick').remove();
    d3.select(el).select(utils.selector(ors.selectors.xAxis)).selectAll('path').remove();
    d3.select(el).select(utils.selector(ors.selectors.yAxis)).selectAll('.tick').remove();
    d3.select(el).select(utils.selector(ors.selectors.yAxis)).selectAll('path').remove();
    return;
  }

  const xOptions = { ...options.axis.x, baseSize: options.base.width, baseOffset: options.base.height };
  const yOptions = { ...options.axis.y, baseSize: options.base.height, baseOffset: options.base.width };

  const yAxis = ors.computors.yAxis(base.yScale, base.yPivot, yOptions);
  const yElement = d3.select(el)
    .select(utils.selector(ors.selectors.yAxis))
    .call(ors.eauors.yAxisEnter, yAxis, yOptions);
  const xScale = ors.computors.xScale(
    options,
    data,
    ors.computors.yAxisOffset(yElement, yOptions),
    ors.computors.xAnnotationOffset(
      d3.select(el).select(utils.selector(ors.selectors.base)),
      options,
      data
    )
  );
  const xAxis = ors.computors.xAxis(xScale, base.xPivot, xOptions);
  const xElement = d3.select(el)
    .select(utils.selector(ors.selectors.xAxis))
    .call(ors.eauors.xAxisEnter, xAxis, xOptions);
  xElement.call(
    ors.eauors.xAxisUpdate,
    xAxis,
    xOptions,
    curry(ors.computors.xOffset)(base.yScale, options.base.padding)
  );

  const yScale = ors.computors.yScale(
    options,
    data,
    ors.computors.xAxisOffset(xElement, xOptions)
  );
  yAxis.scale(yScale);
  yElement.call(
    ors.eauors.yAxisUpdate,
    yAxis,
    yOptions,
    curry(ors.computors.yOffset)(xScale, options.base.padding),
  );

  //console.info(utils.prefix, '\t', 'AxisChart', '_update', '<<');

  if (!adjusting) {
    const elements = { xElement, yElement, xAxis, yAxis };
    const adjustments = adjustAxises(elements, _update, el, options, data, ors, base);
    if (adjustments) return adjustments;
  }

  return { xAxis, yAxis };
}

// -> check for eventual overlap in label series or if the offset
// of an axe is not exceeding the limit ratio of the corresponding chart dimension
// setted by maxOffsetRatio option.
// -> after a rotation of the label series (only x-axis), some part of the text
// might be (highly probable) truncated by the bord of the chart. In this case, 
// calculate the amount of pixels truncated and add this amount to y padding,
// this will "push" the x-axis to the right, so that to be sure that all text is well
// rendered in the chart. SIDE EFFECT: in some cases, because pushing the axis reduce
// the space between ticks, it is possible to cause overlap of the labels, which would not be
// checked after ... 
function adjustAxises(elements, _update, el, options, data, ors, base) {
  const { xElement, yElement, xAxis, yAxis } = elements;
  const yOptions = { ...options.axis.y, baseSize: options.base.height };
  const xOptions = { ...options.axis.x, baseSize: options.base.width };
  const xScaleType = utils.getScaleType(xAxis.scale());
  const yScaleType = utils.getScaleType(yAxis.scale());
  if (xScaleType === ORDINAL && xOptions.baseSize >= xOptions.ordinal.minDisplaySize) {
    const xAxisDimensions = utils.getElementDimensions(xElement);
    const xAxisOffset = xAxisDimensions.height;
    if (xAxisOffset / options.base.height > xOptions.maxOffsetRatio) {
      return _update(el, hideAxisLabels('x', options), data, ors, base, true);
    }
    if (utils.ordinalLabelsOverlap(xElement, xAxis, 'x', xOptions, ROTATED)) {
      // here there is no need to check if rotation hapenned, checking considering
      // rotation hapenned anyway cannot mislead the result
      return _update(el, hideAxisLabels('x', options), data, ors, base, true);
    }
    const xWidth = options.base.width - options.base.innerPadding.right;
    const xAxisWidth = xAxisDimensions.width;
    if ( Math.floor(xAxisWidth) > xWidth) {
      const missingWidth = xAxisWidth - xWidth;
      const yPadding = yOptions.padding;
      const padding = yOptions.orient === LEFT ? (yPadding + missingWidth) : (yPadding - missingWidth);
      return _update(el, reduceAxisPadding('y', padding, options), data, ors, base, true);
    }
  }
  const yAxisOffset = utils.getElementDimensions(yElement).width;
  if (yAxisOffset / options.base.width > yOptions.maxOffsetRatio) {
    return _update(el, hideAxisLabels('y', options), data, ors, base, true);
  }
  //y offset can be an issue even in Linear if domain values are really big numbers ...
  if (yScaleType === ORDINAL && yOptions.baseSize >= yOptions.ordinal.minDisplaySize) {
    if (utils.ordinalLabelsOverlap(yElement, yAxis, 'y', yOptions, UNROTATED)) {
      return _update(el, hideAxisLabels('y', options), data, ors, base, true);
    }
  }
}
