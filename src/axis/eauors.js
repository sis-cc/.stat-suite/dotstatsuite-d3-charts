import { concat, each, get, gt, head, map, size, sortBy } from 'lodash';
import {
  lineStyle, isScaleLinear, getColor, getElementDimensions, getRangeDelta,
  fontStyle, fontBaselineOffset, ordinalLabelsOverlap, getScaleType
} from '../utils/';
import { TEXT_END, ORDINAL, UNROTATED } from '../utils/constants';

function axisTextRotationUpdate(selection, options) {
  selection.selectAll('text')
    .attr('transform', `rotate(${options.rotation} 0 ${options.size})`)
    .style('text-anchor', TEXT_END);
}

function axisTextBaseline(selection, options) {
  selection.selectAll('text').each(function() {
    const element = d3.select(this);
    const offset = fontBaselineOffset(element, options.font.baseline);
    element.attr('transform', `translate(0, ${offset})`);
  });
}

function axisEnter(selection, axis, options) {
  selection.call(axis);
  selection.selectAll('path').style(lineStyle(options));
  selection.selectAll('line').style(lineStyle(options.tick));
  selection.selectAll('text').style(fontStyle(options.font));
}

function xAxisUpdate(selection, axis, options, offset, accessors) {
  axisEnter(selection, axis, options);
  axisTextBaseline(selection, options);
  if (getScaleType(axis.scale()) === ORDINAL && ordinalLabelsOverlap(selection, axis, 'x', options, UNROTATED)) {
    axisTextRotationUpdate(selection, options.font);
  }
  selection.attr('transform', `translate(0, ${offset(selection, axis, options)})`);
}

function yAxisUpdate(selection, axis, options, offset) {
  axisEnter(selection, axis, options);
  selection.attr('transform', `translate(${offset(selection, axis, options)}, 0)`);
  axisTextBaseline(selection, options);
}

function xAxisLabelUpdate(selection, options, accessors) {
  selection.style({ fill: datum => getColor(options, accessors.focusData(datum)) });
}

function yAxisLabelUpdate(selection, options, accessors) {
  selection.style({ fill: datum => getColor(options, accessors.focusData(datum)) });
}

export default function eauors(target) {
  target.eauors = {
    ...target.eauors,
    yAxisEnter: axisEnter,
    yAxisUpdate,
    xAxisEnter: axisEnter,
    xAxisUpdate,
    xAxisLabelUpdate,
    yAxisLabelUpdate
  };
}