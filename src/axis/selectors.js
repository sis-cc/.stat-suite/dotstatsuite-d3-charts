export default function selectors(target) {
  const axis = `${target.selectors.chart}__axis`;

  target.selectors = {
    ...target.selectors,
    xAxis: `${axis}--x`,
    yAxis: `${axis}--y`
  };
}
