import { gt, each, round } from 'lodash';
import * as R from 'ramda';
import { getElementDimensions, getTextDimensions } from '../utils/';
import * as utilsComputors from '../utils/computors';
import { factorizeNegativeScale } from '../utils/linear';
import * as C from '../utils/constants';

export default function computors(target) {
  target.computors = {
    ...target.computors,
    xTickOffset: (options, data) => {
      const domain = target.computors.xDomain(options, data);
      const max = R.max(...domain);
      const min = R.min(...domain);
      const domainDelta = max - min;
      const fontSize = R.path(['axis', 'x', 'font', 'size'], options);
      const charWidth = Math.ceil(fontSize * 0.75);
      const { factor, powerScale } = factorizeNegativeScale(domainDelta);
      const _powerScale = factor > 5 ? powerScale : powerScale / 10;
      return R.pipe(
        R.map(
          v => {
            const rounded = Math.floor(v / _powerScale) * _powerScale;
            return R.length(String(rounded));;
          }
        ),
        d => R.max(...d),
        v => Math.ceil((v + 3) * charWidth / 2)
      )(domain);
    },
    xRange: (options, offset = 0, tickOffset = 0, annotationOffset = 0) => {
      // very specific case:
      // I want to have innerPadding effective only on xScale base's call
      const innerPadding = options.fromBase ? { right: 0, left: 0 } : options.base.innerPadding;

      const width = options.base.width - options.axis.y.thickness;
      if (options.axis.y.orient === C.LEFT) {
        const rightOffset  = R.max(innerPadding.right + annotationOffset, tickOffset);
        return [
          options.base.padding.left + innerPadding.left + offset,
          width - options.base.padding.right - rightOffset,
        ];
      } else if (options.axis.y.orient === C.RIGHT) {
        const leftOffset  = R.max(innerPadding.left + annotationOffset, tickOffset);
        return [
          options.base.padding.left + leftOffset,
          width - offset - innerPadding.right - options.base.padding.right,
        ];
      }
      return [0, width]; // impossible usecase
    },
    yRange: (options, offset = 0) => {
      const innerPadding = options.fromBase ? { top: 0, bottom: 0 } : options.base.innerPadding;
      const height = options.base.height - options.axis.x.thickness;
      if (options.axis.x.orient === C.TOP) {
        return [
          height - options.base.padding.bottom - innerPadding.bottom,
          offset + options.base.padding.top + innerPadding.top,
        ];
      } else if (options.axis.x.orient === C.BOTTOM) {
        return [
          height - offset - options.base.padding.bottom - innerPadding.bottom,
          options.base.padding.top + innerPadding.top,
        ];
      }
      return [height, 0];
    },
    xScale: (options, data, offset, annotationOffset) => {
      const tickOffset = target.computors.xTickOffset(options, data);
      return utilsComputors.computeLinearScale(
        target.computors.xRange(options, offset, tickOffset, annotationOffset),
        target.computors.xDomain(options, data)
      );
    },
    yScale: (options, data, offset) => (
      utilsComputors.computeLinearScale(
        target.computors.yRange(options, offset),
        target.computors.yDomain(options, data)
      )
    ),
    xAxis: (scale, pivot, options) => utilsComputors.computeLinearAxis(scale, pivot, options),
    yAxis: (scale, pivot, options) => utilsComputors.computeLinearAxis(scale, pivot, options),
    xAxisOffset: (el) => getElementDimensions(el).height,
    yAxisOffset: (el, options) => getElementDimensions(el).width + options.padding,
    xOffset: (scale, padding, el, axis, options) => {
      const axisHeight = target.computors.xAxisOffset(el, options);
      const tickSize = options.tick.size;
      if (options.orient === C.TOP) {
        return (gt(tickSize, 0) ? axisHeight : axisHeight + tickSize) + padding.top;
      }
      else if (options.orient === C.BOTTOM) {
        // const axisOffset = max(getScaleExtent(scale)) - axisHeight;
        const axisOffset = options.baseOffset - axisHeight;
        return gt(tickSize, 0) ? axisOffset : axisOffset - tickSize;
      }
      return 0;
    },
    yOffset: (scale, padding, el, axis, options) => {
      const axisWidth = target.computors.yAxisOffset(el, options);
      const tickSize = options.tick.size;
      if (options.orient === C.LEFT) {
        return (gt(tickSize, 0) ? axisWidth : axisWidth + tickSize) + padding.left;
      }
      else if (options.orient === C.RIGHT) {
        // const axisOffset = max(getScaleExtent(scale));
        const axisOffset = options.baseOffset - axisWidth;
        return gt(tickSize, 0) ? axisOffset : axisOffset - tickSize;
      }
      return 0;
    },
    xAnnotationOffset: (el, options, data) => {
      if (!options.base.isAnnotated) return 0;
      const maxWidth = options.serie.annotation.format.serie.maxWidth;
      let longestAnnotationWidth = 0;
      each(data, serie => {
        if (!target.accessors.isFocused(serie)) return;
        const annotation = target.accessors.annotationSerie(options.serie.annotation, serie);
        const annotationWidth = getTextDimensions(el, annotation, options.serie.annotation.font).width;
        if ((annotationWidth > longestAnnotationWidth) && (annotationWidth <= maxWidth) && (maxWidth !== null)) {
          longestAnnotationWidth = annotationWidth;
        }
      });
      return round(longestAnnotationWidth) + C.ANNOTATION_SERIE_OFFSET;
    }
  };
}