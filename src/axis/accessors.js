import { getFormat } from '../utils';

export default function accessors(target) {
  target.accessors = {
    ...target.accessors,
    // used in line, has impact of axis scale
    annotationSerie: (options, serie) => getFormat(options.format.serie)(serie),
  };
}
