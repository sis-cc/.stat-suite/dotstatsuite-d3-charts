import d3 from 'd3';
import { curry, bind, length } from 'ramda';
import {
  setupElement, getColor, getOverColor, selector, fontStyle, fontBaselineOffset,
} from '../utils/';
import { NONE, TEXT_ABOVE, TEXT_MIDDLE, NEVER } from '../utils/constants';

function annotationDatapointBaseline() {
  const element = d3.select(this);
  const offset = fontBaselineOffset(element, TEXT_ABOVE);
  return `translate(0, ${offset})`;
}

function annotationDatapointEnter(selection, selector) {
  setupElement(selection, 'svg:text', selector);
}

function annotationDatapointUpdate(selection, options, accessors, scales, color) {
  selection
    .text(curry(accessors.annotationDatapoint)(options.serie.annotation))
    .attr({
      x: datum => scales.xScale(accessors.x(datum)),
      y: datum => scales.yScale(accessors.y(datum))
    })
    .style({
      ...fontStyle(options.serie.annotation.font),
      fill: color,
      'text-anchor': TEXT_MIDDLE,
      'text-shadow': `1px 1px 0 ${options.background.color}, 0 0 2px ${options.background.color}`,
      opacity: 1,
    });

  // after first rendering to have the correct height
  selection.attr({ transform: annotationDatapointBaseline });
}


function lineEnter(selection, _, selector) {
  return setupElement(selection, 'svg:path', selector);
}

function lineOver(options, selectors, accessors, scales, serieData) {
  const color = getOverColor(options.serie, accessors.focusData(serieData));

  const serie = d3.select(this.parentNode.parentNode);
  serie
    .selectAll(selector(selectors.subSerie))
    .selectAll(selector(selectors.line))
    .style({ stroke: color, 'stroke-width': options.serie.line.focused.thickness });
  if (!accessors.isCondensed(options.serie, serie.datum(), scales.xScale)) {
    serie
      .selectAll(selector(selectors.subSerie))
      .selectAll(selector(selectors.marker))
      .style({ fill: color, opacity: 1 });

    if (options.serie.annotation.display !== NEVER) {
      const annotations = serie
        .selectAll(selector(selectors.subSerie))
        .selectAll(selector(selectors.annotationDatapoint))
        .data(accessors.datapoints);

      annotations.enter().call(annotationDatapointEnter, selectors.annotationDatapoint);
      annotations.call(annotationDatapointUpdate, options, accessors, scales, color);
    }
  }
}

function lineOut(options, selectors, accessors, scales, serie) {
  const color = getColor(options.serie, accessors.focusData(serie));

  const allSerie = d3.select(this.parentNode.parentNode);

  allSerie
    .selectAll(selector(selectors.line))
    .style({
      stroke: color,
      'stroke-width': serie => (
        accessors.isFocused(serie) ? options.serie.line.focused.thickness : options.serie.line.thickness
      )
    });
  if (!accessors.isCondensed(options.serie, allSerie.datum(), scales.xScale)) {
    allSerie
      .selectAll(selector(selectors.subSerie))
      .filter(serie => length(serie.datapoints) > 1)
      .selectAll(selector(selectors.marker))
      .style({
        fill: color,
        opacity: accessors.isFocused(serie) ? 1 : 0
      });

    allSerie
      .selectAll(selector(selectors.subSerie))
      .selectAll(selector(selectors.annotationDatapoint))
      .style({ opacity: 0 });
  }
  allSerie
    .selectAll(selector(selectors.subSerie))
    .filter(serie => length(serie.datapoints) === 1)
    .selectAll(selector(selectors.marker))
    .style({ fill: color, opacity: 1 });
}

function lineUpdate(selection, options, selectors, accessors, { xScale, yScale }) {
  const line = d3.svg.line()
    .x(datum => xScale(accessors.x(datum)))
    .y(datum => yScale(accessors.y(datum)));

  selection
    .attr('d', serie => line(accessors.datapoints(serie)))
    .style({
      stroke: serie => getColor(options.serie, accessors.focusData(serie)),
      'stroke-width': serie => (
        accessors.isFocused(serie) ? options.serie.line.focused.thickness : options.serie.line.thickness
      ),
      fill: NONE
    })
    .on('mouseover', curry(lineOver)(options, selectors, accessors, { xScale, yScale }))
    .on('mouseout', curry(lineOut)(options, selectors, accessors, { xScale, yScale }));
}


function hoverlineEnter(selection, options, selectors) {
  lineEnter(selection, options, selectors.hoverline);
}

function hoverlineUpdate(selection, options, selectors, accessors, scales) {
  lineUpdate(selection, options, selectors, accessors, scales);
  selection.style({ stroke: 'black', 'stroke-width': options.serie.line.shadow, opacity: 0 });
}

function markerEnter(selection, selectors, hideTooltip) {
  setupElement(selection, 'svg:path', selectors.marker)
  .on('mouseleave', curry(hideTooltip));
}

function markerUpdate(selection, options, selectors, accessors, { xScale, yScale }, showTooltip) {
  selection
    .attr({
      d: d3.svg.symbol().type(options.serie.line.marker.shape).size(30),
      transform: datum => `translate(${xScale(accessors.x(datum))}, ${yScale(accessors.y(datum))})`
    })
    .style({
      opacity: function() {
        const serieData = d3.select(this.parentNode.parentNode).datum();
        const subSerieData = d3.select(this.parentNode).datum();
        const isAlone = length(accessors.datapoints(subSerieData)) === 1;
        const isFocused = accessors.isFocused(serieData);
        const isCondensed = accessors.isCondensed(options.serie, serieData, xScale);
        return (isFocused && !isCondensed) || isAlone ? 1 : 0;
      },
      fill: function() {
        return getColor(options.serie, accessors.focusData(d3.select(this.parentNode).datum()));
      },
      stroke: options.background.color,
      'stroke-width': options.serie.line.marker.shadow,
    })
    .on('mouseover', function() {
      const serie = d3.select(this.parentNode.parentNode);
      const hoverline = serie.select(selector(selectors.hoverline)).node();
      bind(lineOver, hoverline)(options, selectors, accessors, { xScale, yScale }, serie.datum());
      if (accessors.isCondensed(options.serie, serie.datum(), xScale)) {
        d3.select(this).style({
          opacity: 1,
          fill: function() {
            return getOverColor(options.serie, accessors.focusData(serie.datum()));
          }
        });
      }
    })
    .on('mousemove', function(datum) {
      const subSerieData = d3.select(this.parentNode).datum();
      const color = getOverColor(options.serie, accessors.focusData(subSerieData));
      showTooltip(options.serie, { xScale, yScale }, subSerieData, datum, color);
    })
    .on('mouseout', function() {
      const serie = d3.select(this.parentNode.parentNode);
      const hoverline = serie.select(selector(selectors.hoverline)).node();
      bind(lineOut, hoverline)(options, selectors, accessors, { xScale, yScale }, serie.datum());
      if (accessors.isCondensed(options.serie, serie.datum(), xScale)) {
        const subSerieData = d3.select(this.parentNode).datum();
        d3.select(this).style({
          opacity: length(accessors.datapoints(subSerieData)) > 1 ? 0 : 1,
          fill: function() {
            return getColor(options.serie, accessors.focusData(subSerieData));
          }
        });
      }
    });
}

export default function eauors(target) {
  target.eauors = {
    ...target.eauors,
    lineEnter,
    lineUpdate,
    hoverlineEnter,
    hoverlineUpdate,
    markerEnter,
    markerUpdate,
  };
}
