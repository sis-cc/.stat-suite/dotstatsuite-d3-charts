import * as R from 'ramda';

export default function computors(target) {
  target.computors = {
    ...target.computors,
    series: R.sortWith([
      R.ascend(target.accessors.baselineIndex),
      R.ascend(target.accessors.highlightIndex)
    ]),
    linesData: serie => {
      const datapoints = serie.datapoints;
      const _split = R.reduce((acc, dp) => {
        if (R.isNil(dp.y)) {
          return R.append([], acc);
        }
        return R.over(R.lensIndex(-1), R.append(dp))(acc);
      }, [[]], datapoints);
      const split = R.reject(R.isEmpty, _split);
      return R.map(datapoints => ({ ...serie, datapoints }), split);
    },
    markersData: serie => R.pipe(
      R.propOr([], 'datapoints'),
      R.reject(dp => R.isNil(dp.y))
    )(serie),
  };
}
