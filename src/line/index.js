import d3 from 'd3';

import BackgroundChart from '../background/';

import * as utils from '../utils/';

import selectors from './selectors';
import accessors from './accessors';
import computors from './computors';
import eauors from './eauors';

@selectors
@accessors
@computors
@eauors
export default class LineChart extends BackgroundChart {
  constructor(el, options={}, data=[], getInitialOptions) {
    super(el, options, data, getInitialOptions);
    _contructor(
      el,
      this.options,
      data,
      {
        selectors: this.constructor.selectors,
        accessors: this.constructor.accessors,
        computors: this.constructor.computors,
        eauors: this.constructor.eauors
      },
      this.base,
      this.axis
    );
  }

  update(el, options={}, data=[], getInitialOptions) {
    super.update(el, options, data, getInitialOptions);
    _update(
      el,
      this.options,
      data,
      {
        selectors: this.constructor.selectors,
        accessors: this.constructor.accessors,
        computors: this.constructor.computors,
        eauors: this.constructor.eauors
      },
      this.base,
      this.axis
    );
  }
}

function _contructor(el, options, data, ors, base, axis) {
  //console.info(utils.prefix, '\t\t\t\t', 'LineChart', '_contructor', '>>');

  utils.noElError(el);

  _update(el, options, data, ors, base, axis);

  //console.info(utils.prefix, '\t\t\t\t', 'LineChart', '_contructor', '<<');
}

function _update(el, options, data, ors, base, axis) {
  //console.info(utils.prefix, '\t\t\t\t', 'LineChart', '_update', '>>');
  if (process.env.NODE_ENV !== 'production') {
    console.time(`${utils.prefix} LineChart#_update`);
  }

  utils.noElError(el);
  if (utils.noDataError(ors.accessors.data(data), '\t\t\t\t', 'LineChart')) {
    d3.select(el)
      .selectAll(utils.selector(ors.selectors.serie)).remove();
    return;
  }

  const scales = {
    xScale: ors.accessors.xScale(base, axis),
    yScale: ors.accessors.yScale(base, axis)
  };

  const serieJoin = d3.select(el)
    .select(utils.selector(ors.selectors.base))
    .selectAll(utils.selector(ors.selectors.serie))
    .data(ors.computors.series(data));
  serieJoin.enter().call(utils.setupElement, 'svg:g', ors.selectors.serie);
  serieJoin.exit().remove();

  const subSeriesJoin = serieJoin
    .selectAll(utils.selector(ors.selectors.subSerie))
    .data(serie => ors.computors.linesData(serie, options));
  subSeriesJoin.enter().call(utils.setupElement, 'svg:g', ors.selectors.subSerie);
  subSeriesJoin.exit().remove();

  const lineJoin = subSeriesJoin
    .selectAll(utils.selector(ors.selectors.line))
    .data(serie => [serie]);
  lineJoin.enter().call(ors.eauors.lineEnter, options.serie, ors.selectors.line);
  lineJoin.call(ors.eauors.lineUpdate, options, ors.selectors, ors.accessors, scales);
  lineJoin.exit().remove();

  const hoverlineJoin = subSeriesJoin
    .selectAll(utils.selector(ors.selectors.hoverline))
    .data(serie => [serie]);
  hoverlineJoin
    .enter()
    .call(ors.eauors.hoverlineEnter, options.serie, ors.selectors);
  hoverlineJoin.call(ors.eauors.hoverlineUpdate, options, ors.selectors, ors.accessors, scales);
  hoverlineJoin.exit().remove();

  const markerJoin = subSeriesJoin
    .selectAll(utils.selector(ors.selectors.marker))
    .data(ors.accessors.datapoints);
  markerJoin
    .enter()
    .call(ors.eauors.markerEnter, ors.selectors, base.hideTooltip);
  markerJoin.call(ors.eauors.markerUpdate, options, ors.selectors, ors.accessors, scales, base.showTooltip);
  markerJoin.exit().remove();

  //console.info(utils.prefix, '\t\t\t\t', 'LineChart', '_update', '<<');
  if (process.env.NODE_ENV !== 'production') {
    console.timeEnd(`${utils.prefix} LineChart#_update`);
  }
}
