import { getFormat, getRangeDelta } from '../utils';
import { map, pipe, propOr, reject, isNil, last, length } from 'ramda';

export default function accessors(target) {
  target.accessors = {
    ...target.accessors,
    data: data => data || [],
    datapoints: serie => pipe(
      propOr([], 'datapoints'),
      reject(dp => isNil(dp.y))
    )(serie),
    lastDatapoint: serie => last(target.accessors.datapoints(serie)),
    yRange: (base) => base.xScale.range(),
    annotationDatapoint: (options, datum) => getFormat(options.format.datapoint)(datum.y),
    isCondensed: (options, serie, scale) => {
      const datapoints = target.accessors.datapoints(serie);
      const datapointsCount = length(datapoints);
      const datapointsXPositions = map(
        dp => scale(target.accessors.x(dp)),
        datapoints
      );
      const datapointsXExtent = [Math.min(...datapointsXPositions), Math.max(...datapointsXPositions)];
      const rangeDelta = getRangeDelta(datapointsXExtent);
      const ratio = rangeDelta / Math.max(1, datapointsCount);
      return ratio < options.line.densityRatio;
    }
  };
}
