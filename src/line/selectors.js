export default function selectors(target) {
  const chart = target.selectors.chart;

  target.selectors = {
    ...target.selectors,
    serie: `${chart}__serie`,
    subSerie: `${chart}__subserie`,
    line: `${chart}__line`,
    hoverline: `${chart}__hoverline`,
    marker: `${chart}__marker`,
    annotationDatapoint: `${chart}__annotationDatapoint`
  };
}
