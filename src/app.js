import { map, random, range, size, last, get, round } from 'lodash';
import * as R from 'ramda';
import { BarChart, RowChart, ScatterChart, LineChart,
  TimelineChart, VerticalSymbolChart, HorizontalSymbolChart, StackedBarChart,
  ChoroplethChart, ChoroplethLegend, StackedRowChart } from './';
import lineData from '../test/mock-data/line';
import choroSeries from '../test/mock-data/choro';
import mapSeriesFrance from '../test/mock-data/choro2';
import choroSeries3 from '../test/mock-data/choro3';
import vsymbol0 from '../test/mock-data/vsymbol.0';
import worldMap from '../test/mock-data/world.json';

import './assets/utils.less';
import './assets/app.less';

function render(id, chart) {
  let root = document.createElement('div');
  root.id = id;
  document.getElementById('root').appendChild(root);
  chart(root);
}

const datapoints = map(range(30), n => ({ x: `label#${n}`, y: random(0, 100000, true) }));
// cut inner padding case
/*datapoints[0] = { ...datapoints[0], x: 'lalala hello things end ' };
datapoints[4] = { ...datapoints[4], baselineIndex: 0 };
datapoints[8] = { ...datapoints[8], y: 15, highlightIndex: 0 };
datapoints[11] = { ...datapoints[11], y: -5, highlightIndex: 1 };
datapoints[12] = { ...datapoints[12], y: -20.1 };*/

const worldChoroSeries = R.over(
  R.lensPath(['objects', 'areas', 'geometries']),
  R.addIndex(R.map)((geometry) => {
    const id = R.path(['properties', 'id'], geometry);
    return R.pipe(
      R.assocPath(['properties', 'value'], random(0, 20, true)),
      R.assocPath(['properties', 'label'], id)
    )(geometry);
  })
)(worldMap);

const symbolDatapoints = [
  {category: ' label1', values: [25, 27]},
  {category: ' label2', values: [24.7, 26.6]},
  {category: ' label3', values: [24.4, 26], highlightIndex: 0 },
  {category: ' label4', values: [null, 25.7]},
  {category: ' label5', values: [24.2, 25], baselineIndex: 0 },
  {category: ' label6', values: [24, 24.5]},
  {category: ' label7', values: [23.7, 22.8], highlightIndex: 1 },
  {category: ' label8', values: [23.5, null]},
  {category: ' label9', values: [23, 15.5]},
  {category: ' label10', values: [22, 16]},
  {category: ' label11', values: [20, 17]},
  {category: ' label12', values: [null, 22]},
  {category: ' label13', values: [16, 20]},
  {category: ' label14', values: [12, 15]},
  {category: ' label15', values: [9, 16]},
  { category: 'elementary particle in the Standard Model', values: [6, null]}
];

const symbolDatapoints2 = [
  { category: 'Hungary', values: [5.5] },
  { category: 'Sweden', values: [5.6] },
  { category: 'Estonia', values: [5.7] },
  { category: 'Finland', values: [5.8] },
  { category: 'Belgium', values: [5.9] },
  { category: 'Turkey', values: [6] },
  { category: 'France', values: [6.5] },
  { category: 'Poland', values: [6.6] },
  { category: 'Slovenia', values: [6.8] },
  { category: 'European Union', values: [7.4], baselineIndex: 0 },
  { category: 'Euro area (18 countries)', values: [8.2] },
  { category: 'Ireland', values: [8.7], highlightIndex: 0 },
  { category: 'Italy', values: [8.9] },
  { category: 'Slovak Republic', values: [9.9] },
  { category: 'Portugal', values: [10.9] },
  { category: 'South Africa', values: [12.3], highlightIndex: 1 },
  { category: 'Spain (2 digits)', values: [16.86] }
];

const barData2 = [{ datapoints: [
  { x: '1', y: 1,  },
  { x: '2', y: 2, },
  { x: '3', y: 3, },
  { x: '4', y: 4, },
  { x: '5', y: 5, },
  { x: '6', y: 6, },
  { x: '7', y: 7, },
  { x: '8', y: 8, },
  { x: '9', y: 9, },
  { x: '10', y: 10, },
  { x: '11', y: 11, },
  { x: '12', y: 12.55,},
  { x: '13', y: 13, },
  { x: '14', y: 14, },
  { x: '15', y: 15, },
] }];

const stackedBarSeries = [{
  datapoints: [
    { "x": 0, "y": [21, 24, 63, 11, 11] },
    { "x": 1, "y": [34, null, 15, 23, 9], test: 'toto' },
    { "x": 2, "y": [55, 65, 32, 35, 7] },
    { "x": 3, "y": [62, 82, 27, 21, 13] },
    { "x": 4, "y": [10, 73, 54, 24, 4], highlightIndex: 0 },
    { "x": 5, "y": [0, 16, 92, 32, 9] },
    { "x": 6, "y": [32, 53, 18, 34, 12] },
    { "x": 7, "y": [-68, 53, 41, 21, -7] },
    { "x": 8, "y": [42, 46, 83, 13, 15] },
    { "x": 9, "y": [28, 38, 22, 17, 7], baselineIndex: 0 },
    { "x": 10, "y": [36, 52, 62, 23, 14] },
    { "x": 11, "y": [-29, -64, -33, -39, -10], highlightIndex: 1 }
  ],
  layerSeries: [
    { id: '1', label: 'layer 1' },
    { id: '2', label: 'layer 2' },
    { id: '3', label: 'layer 3' },
    { id: '4', label: 'layer 4' },
    { id: '5', label: 'layer 5' }
  ]
}];

const stackedRowSeries = [{
  datapoints: [
    { y: 0, x: [21, 24, 63, 11, 11] },
    { y: 1, x: [34, null, 15, 23, 9], test: 'toto' },
    { y: 2, x: [55, 65, 32, 35, 7] },
    { y: 3, x: [62, 82, 27, 21, 13] },
    { y: 4, x: [10, 73, 54, 24, 4], highlightIndex: 0 },
    { y: 5, x: [0, 16, 92, 32, 9] },
    { y: 6, x: [32, 53, 18, 34, 12] },
    { y: 7, x: [-68, 53, 41, 21, -7] },
    { y: 8, x: [42, 46, 83, 13, 15] },
    { y: 9, x: [28, 38, 22, 17, 7], baselineIndex: 0 },
    { y: 10, x: [36, 52, 62, 23, 14] },
    { y: 11, x: [-29, -64, -33, -39, -10], highlightIndex: 1 }
  ],
  layerSeries: [
    { id: '1', label: 'layer 1' },
    { id: '2', label: 'layer 2' },
    { id: '3', label: 'layer 3' },
    { id: '4', label: 'layer 4' },
    { id: '5', label: 'layer 5' }
  ]
}];

const getInitialOptions = (options) => {
  console.log('options', options);
}

const layout = (serie, datum, color) => {
  const style = `
    font-size: 10px;
    font-family: sans-serif;
    color: ${color};
    padding: 5px;
    border: 1px solid ${color};
    background: white;
    opacity: .8;
  `;
  return `<div style="${style}">X: ${datum.x}<br />Y: ${datum.y}</div>`;
}

const rowData2 = [{ datapoints: map(barData2[0].datapoints, dp => ({ ...dp, x: dp.y, y: dp.x })) }];
const barData = [{ datapoints }];
const rowData = [{ datapoints: map(datapoints, dp => ({ ...dp, x: dp.y, y: dp.x })) }];
const scatterData = [{ datapoints: map(
  datapoints,
  (dp, index) => ({
    ...dp,
    ...index % 2 === 0 ? { highlightIndex: index } : {},
    ...index === 1 ? { baselineIndex: 0 } : {},
    x: dp.y*random(0, 1, true)
  }))
}];
const symbolData = [{ datapoints: symbolDatapoints }];
const symbolData2 = [{ datapoints: symbolDatapoints2 }];

const choroSeriesWithValues = mapSeries => (
  {
    ...mapSeries,
    objects: {
      areas: {
        type: 'GeometryCollection',
        geometries: map(
          mapSeries.objects.areas.geometries,
          (geo) => ({
            ...geo,
            properties: {
              ...geo.properties,
              value: random(-20, 100, true)
            }
          })
        )
      }
    }
  }
);

const choroSeries2 = choroSeriesWithValues(mapSeriesFrance);

/*render('root654654', el => new ChoroplethChart(
  el,
  {
    base: {
      width: 300,
      height: 200,
    },
    background: {
      color: 'dark grey'
    },
    map: {
      innerPadding: { left: 5, top: 10, right: 5, bottom: 10 },
      projection: 'geoRobinson',
      //scale: 150,
    },
    serie: {
      choropleth: {
        labelDisplay: 'none'
      }
    }
  },
  worldChoroSeries,
  null
));
render('root654659', el => new ChoroplethChart(
  el,
  {
    base: {
      width: 350,
      height: 250,
    },
    background: {
      color: 'grey'
    },
    map: {
      projection: 'geoMercator',
      //scale: 150,
    },
    serie: {
      choropleth: {
        labelDisplay: 'none'
      }
    }
  },
  worldChoroSeries,
  null
));
render('root654659', el => new ChoroplethChart(
  el,
  {
    base: {
      width: 350,
      height: 300,
    },
    background: {
      color: 'light grey'
    },
    map: {
      projection: 'geoConicEquidistant',
      //scale: 150,
    },
    serie: {
      choropleth: {
        labelDisplay: 'none'
      }
    }
  },
  worldChoroSeries,
  null
));
render('rootlegend', el => new ChoroplethLegend(
  el,
  {
    legend: {
      choropleth: {
        width: 500,
        height: 50,
        margin: {
          left: 10, right: 10
        },
        axis: {
          format: {
            proc: datum => round(datum, 2)
          },
        }
      }
    }
  },
  worldChoroSeries,
));

/*render('root80', el => new VerticalSymbolChart(
  el,
  vsymbol0.options,
  vsymbol0.data,
  vsymbol0.getInitialOptions,
));*/

/*render('root1', el => new ChoroplethChart(
  el,
  {
    base: {
      width: 800,
      height: 500,
    },
    background: {
      color: 'blue'
    },
    map: {
      projection: 'geoRobinson',
      extBoundariesColor: 'black',
      intBoundariesDisplay: 'none',
    },
  },
  choroSeries3,
  null
));*/

/*render('root2', el => new ChoroplethChart(
  el,
  {
    base: {
      width: 800,
      height: 800,
    },
    map: {
      scale: 120,
      extBoundariesColor: 'black',
      intBoundariesDisplay: 'none',
    },
  },
  choroSeries3,
  null
));

render('root3', el => new ChoroplethChart(
  el,
  {
    base: {
      width: 500,
      height: 450,
    },
    map: {
      projection: 'geoRobinson',
      scale: 2000,
      extBoundariesColor: 'black',
      intBoundariesDisplay: 'none',
    },
    serie: {
      choropleth: {
        divisions: 30,
        domain: 'RdYlGn',
        invertColors: true,
        labelFont: {
          color: '#A9A9A9'
        }
      }
    }
  },
  choroSeries,
  null
));

render('root3.5', el => new ChoroplethLegend(
  el,
  {
    serie: {
      choropleth: {
        divisions: 30,
        domain: 'RdYlGn',
        invertColors: true,
        labelFont: {
          color: '#A9A9A9'
        }
      }
    },
    legend: {
      choropleth: {
        width: 70,
        height: 450,
        margin: {
          top: 10, bottom: 10
        },
        axis: {
          format: {
            proc: datum => round(datum, 2)
          },
          orient: 'left',
        }
      }
    }
  },
  choroSeries,
));

render('root4', el => new ChoroplethChart(
  el,
  {
    base: {
      width: 400,
      height: 400,
    },
    background: {
      color: 'yellow'
    },
    map: {
      projection: 'geoMercator',
      scale: 2000,
      extBoundariesColor: 'black',
      intBoundariesDisplay: 'none',
    },
    serie: {
      choropleth: {
        divisions: 30,
        domain: 'Purples',
        labelDisplay: 'none'
      }
    }
  },
  choroSeries2,
  null
));*/

/*render('root4.5', el => new ChoroplethLegend(
  el,
  {
    serie: {
      choropleth: {
        divisions: 30,
        domain: 'Purples',
        labelDisplay: 'none'
      }
    },
    legend: {
      choropleth: {
        width: 70,
        height: 450,
        margin: {
          top: 10, bottom: 10
        },
        axis: {
          format: {
            proc: datum => round(datum, 2)
          },
          orient: 'right',
        }
      }
    }
  },
  choroSeries2,
));

render('root5', el => new ChoroplethChart(
  el,
  {
    base: {
      width: 550,
      height: 750,
    },
    map: {
      scale: 2000,
      extBoundariesColor: 'black',
      intBoundariesDisplay: 'none',
    },
    serie: {
      choropleth: {
        divisions: 30,
        domain: 'Blues',
        labelFont: {
          color: '#A9A9A9'
        }
      }
    }
  },
  choroSeries2,
  null
));

render('root6', el => new ChoroplethChart(
  el,
  {
    base: {
      width: 600,
      height: 600,
    },
    map: {
      scale: 2000,
      extBoundariesColor: 'black',
      intBoundariesDisplay: 'none',
    },
    serie: {
      choropleth: {
        divisions: 7,
        minDomain: -50,
        maxDomain: 100,
        labelDisplay: 'none'
      }
    }
  },
  choroSeries2,
  null
));

render('root6.5', el => new ChoroplethLegend(
  el,
  {
    serie: {
      choropleth: {
        divisions: 7,
        minDomain: -50,
        maxDomain: 100,
        labelDisplay: 'none'
      }
    },
    legend: {
      choropleth: {
        width: 300,
        height: 50,
        margin: {
          left: 15, right: 15
        },
        axis: {
          format: {
            proc: datum => round(datum, 2)
          },
          orient: 'bottom',
        }
      }
    }
  },
  choroSeries2,
));

render('root7', el => new ChoroplethChart(
  el,
  {
    base: {
      width: 600,
      height: 600,
    },
    map: {
      scale: 2000,
    },
    serie: {
      choropleth: {
        labelDisplay: 'none'
      }
    }
  },
  choroSeries2,
  null
));

render('root8', el => new ChoroplethChart(
  el,
  {
    base: {
      width: 600,
      height: 600,
    },
    background: {
      color: 'grey'
    },
    map: {
      scale: 2000,
    },
    serie: {
      choropleth: {
        colors: ['#556B2F', '#FF8C00', '#9932CC', '#8B0000', '#E9967A', '#8FBC8F'],
        labelDisplay: 'none'
      }
    }
  },
  choroSeries2,
  null
));

render('root8.5', el => new ChoroplethLegend(
  el,
  {
    serie: {
      choropleth: {
        colors: ['#556B2F', '#FF8C00', '#9932CC', '#8B0000', '#E9967A', '#8FBC8F'],
        labelDisplay: 'none'
      }
    },
    legend: {
      choropleth: {
        width: 300,
        height: 50,
        margin: {
          left: 15, right: 15
        },
        axis: {
          format: {
            proc: datum => round(datum, 2)
          },
          orient: 'top',
        }
      }
    }
  },
  choroSeries2,
));

render('root10', el => new BarChart(
  el,
  {
    base: { innerPadding: { left: 40, right: 40 }, width: 570, height: 50 },
    axis: { x: { format: { proc: d => d } }, y: { linear: { step: 0 } } },
    serie: { tooltip: { layout } }
  },
  barData
));

render('root11', el => new BarChart(
  el,
  {
    base: { innerPadding: { left: 40, right: 40 }, width: 1000, height: 300 },
    axis: { x: { format: { proc: d => d } }, y: { linear: { step: 1 } } },
    serie: { tooltip: { layout } }
  },
  barData
));

render('root20', el => new RowChart(
  el,
  {
    base: { width: 500, height: 230 },
    axis: { y: { format: { proc: d => d } } },
    serie: { tooltip: { layout } }
  },
  rowData2
));

render('root21', el => new RowChart(
  el,
  {
    base: { width: 500, height: 230 },
    axis: { y: { format: { proc: d => d } } },
    serie: { tooltip: { layout } }
  },
  rowData
));

render('root22', el => new RowChart(
  el,
  {
    base: { width: 500, height: 300 },
    axis: { y: { format: { proc: d => d } } },
    serie: { tooltip: { layout } }
  },
  rowData
));

render('root30', el => new ScatterChart(
  el,
  {
    base: {
      width: 800,
      height: 400,
      padding: { top: 0, right: 0 }
    },
    axis: {
      x: {
        tick: { thickness: 0, size: 0 },
        linear: { min: 0, max: 70000, step: 10000 }
      },
      y: {
        padding: 10,
        tick: { thickness: 0 },
        font: { baseline: 'ideographic' }
      }
    },
    serie: { tooltip: { layout }, annotation: { display: 'never' } }
  },
  scatterData
));*/

render('root40', el => new TimelineChart(
  el,
  {
    base: {
      width: 900,
      height: 300,
      isAnnotated: true,
      padding: { top: 20 },
      innerPadding: { left: 40, right: 40 },
      margin: 20,
    },
    axis: { 
      x: {
        linear: { frequency: 'month', step: 1 },
        format: { pattern: '%b %y', isTime: true },
        tick: { size: -12, minorSize: -4, step: 2 }
      },
      y : { 
        padding: 10,
        thickness: 1,
        orient: 'right'
      }
    }, 
    serie: {
      annotation: {
        format: {
          datapoint: { pattern: '.2f' }
        },
      },
      tooltip: { layout }
    }
  },
  lineData()
));

/*render('root41', el => new TimelineChart(
  el,
  {
    base: {
      width: 700,
      height: 160,
      isAnnotated: false,
      padding: { top: 20 },
      innerPadding: { left: 40, right: 40 },
    },
    axis: { 
      x: {
        linear: { frequency: 'month', step: 1 },
        format: { pattern: '%b %y', isTime: true },
        tick: { size: -12, minorSize: -4, step: 2 }
      },
      y : { 
        padding: 10,
        thickness: 1,
        orient: 'right'
      }
    }, 
    serie: {
      annotation: {
        format: {
          serie: { proc: (serie) => serie.category },
          datapoint: { pattern: '.2f' }
        },
      },
      tooltip: { layout }
    }
  },
  lineData()
));

render('root50', el => new VerticalSymbolChart(
  el,
  {
    base: {
      width: 500,
      height: 300,
      innerPadding: {
        left: 20,
        right: 20,
        top: 10
      }
    },
    axis: { 
      x: { 
        format: { proc: d => d },
        orient: 'bottom'
      }
    },
    serie: { tooltip: { layout } }
},
  symbolData));
render('root60', el => new HorizontalSymbolChart(
  el,
  {
    base: {
      width: 600,
      height: 570,
    },
  },
  symbolData
));

render('root70', el => new StackedBarChart(
  el,
  {
    base: {
      width: 600,
      height: 570,
    },
    serie: {
      //colors: ['#3d9dd1']
      colors: ["#3d9dd1", "#134a71", "#de7c13", "#901216", "#fb5e5b", "#553488", "#aa83be", "#969696", "#333333"]
    }
  },
  stackedBarSeries,
  getInitialOptions
));

render('root71', el => new StackedRowChart(
  el,
  {
    base: {
      width: 570,
      height: 600,
    },
    serie: {
      //colors: ['#3d9dd1']
      colors: ["#3d9dd1", "#134a71", "#de7c13", "#901216", "#fb5e5b", "#553488", "#aa83be", "#969696", "#333333"]
    }
  },
  stackedRowSeries,
  getInitialOptions
));*/
