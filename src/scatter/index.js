import d3 from 'd3';
import { curry } from 'lodash';
import { is } from 'ramda';

import BackgroundChart from '../background/';

import * as utils from '../utils/';
import { NEVER } from '../utils/constants';

import selectors from './selectors';
import accessors from './accessors';
import computors from './computors';
import eauors from './eauors';
import { getLinearAxisOptions } from '../utils/getAxisOptions';

@selectors
@accessors
@computors
@eauors
export default class ScatterChart extends BackgroundChart {
  constructor(el, options={}, data=[], getInitialOptions, getAxisOptions) {
    super(el, options, data, getInitialOptions);
    _contructor(
      el,
      this.options,
      data,
      {
        selectors: this.constructor.selectors,
        accessors: this.constructor.accessors,
        computors: this.constructor.computors,
        eauors: this.constructor.eauors
      },
      this.base,
      this.axis
    );
  }

  update(el, options={}, data=[], getInitialOptions, getAxisOptions) {
    super.update(el, options, data, getInitialOptions);
    _update(
      el,
      this.options,
      data,
      {
        selectors: this.constructor.selectors,
        accessors: this.constructor.accessors,
        computors: this.constructor.computors,
        eauors: this.constructor.eauors
      },
      this.base,
      this.axis,
      getAxisOptions
    );
  }
}

function _contructor(el, options, data, ors, base, axis) {
  //console.info(utils.prefix, '\t\t\t\t', 'ScatterChart', '_contructor', '>>');

  utils.noElError(el);

  d3.select(el)
    .select(utils.selector(ors.selectors.base))
    .call(utils.setupElement, 'svg:g', ors.selectors.serie)

  _update(el, options, data, ors, base, axis);

  //console.info(utils.prefix, '\t\t\t\t', 'ScatterChart', '_contructor', '<<');
}

function _update(el, options, data, ors, base, axis, getAxisOptions) {
  //console.info(utils.prefix, '\t\t\t\t', 'ScatterChart', '_update', '>>');
  if (process.env.NODE_ENV !== 'production') {
    console.time(`${utils.prefix} ScatterChart#_update`);
  }

  utils.noElError(el);
  if (utils.noDataError(ors.accessors.data(data), '\t\t\t\t', 'ScatterChart')) {
    d3.select(el)
      .selectAll(utils.selector(ors.selectors.point)).remove();
    return;
  }

  const scales = {
    xScale: ors.accessors.xScale(base, axis),
    yScale: ors.accessors.yScale(base, axis)
  };

  d3.select(el)
    .selectAll(utils.selector(ors.selectors.serie))
    .data(data);

  const pointJoin = d3.select(el)
    .select(utils.selector(ors.selectors.serie))
    .selectAll(utils.selector(ors.selectors.point))
    .data(ors.computors.data(data));
  pointJoin.enter().call(ors.eauors.pointEnter, options.serie, ors.selectors.point);
  pointJoin.call(ors.eauors.pointUpdate, options.serie, ors.accessors);
  pointJoin.exit().remove();

  const markerJoin = pointJoin.selectAll(utils.selector(ors.selectors.marker)).data(datum => [datum]);
  markerJoin.enter().call(ors.eauors.markerEnter, options.serie, ors.selectors, ors.accessors, base.hideTooltip);
  markerJoin.call(ors.eauors.markerUpdate, options, ors.accessors, scales, base.showTooltip);
  markerJoin.exit().remove();

  if (options.serie.annotation.display !== NEVER) {
    const annotationJoin = pointJoin
      .selectAll(utils.selector(ors.selectors.annotation))
      .data(curry(ors.computors.annotationData)(options.serie.annotation.display));
    annotationJoin.enter().call(ors.eauors.annotationEnter, options.serie, ors.selectors.annotation);
    annotationJoin.call(ors.eauors.annotationUpdate, options, ors.accessors, scales);
    annotationJoin.exit().remove();
  }

  if (is(Function, getAxisOptions)) {
    const xAxisoptions = getLinearAxisOptions(axis.xAxis, options.axis.x);
    const yAxisoptions = getLinearAxisOptions(axis.yAxis, options.axis.y);
    getAxisOptions({
      computedMinX: xAxisoptions.min,
      computedMaxX: xAxisoptions.max,
      computedStepX: xAxisoptions.step,
      computedPivotX: xAxisoptions.pivot,
      computedMinY: yAxisoptions.min,
      computedMaxY: yAxisoptions.max,
      computedStepY: yAxisoptions.step,
      computedPivotY: yAxisoptions.pivot
    });
  }
  //console.info(utils.prefix, '\t\t\t\t', 'ScatterChart', '_update', '<<');
  if (process.env.NODE_ENV !== 'production') {
    console.timeEnd(`${utils.prefix} ScatterChart#_update`);
  }
}
