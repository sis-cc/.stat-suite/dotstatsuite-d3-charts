import { setupElement, getColor, getOverColor, fontStyle, fontBaselineOffset } from '../utils/';
import { computeAreaIndex } from '../utils/computors';
import { TEXT_ABOVE, TEXT_BELOW, TEXT_START, TEXT_END, TEXT_MIDDLE } from '../utils/constants';
import { last, curry } from 'lodash';

function pointEnter(selection, options, selector) {
  setupElement(selection, 'svg:g', selector);
}

function pointUpdate(selection, options, accessors) {
  selection.style({ fill: datum => getColor(options, accessors.focusData(datum)) });
}

export function markerOver(options, accessors, datum) {
  d3.select(this.parentNode).style({ fill: datum => getOverColor(options, accessors.focusData(datum)) });
}

export function markerOut(options, accessors, datum) {
  d3.select(this.parentNode).style({ fill: datum => getColor(options, accessors.focusData(datum)) });
}

function markerEnter(selection, options, selectors, accessors, hideTooltip) {
  setupElement(selection, 'svg:circle', selectors.marker)
    .style({ opacity: .8 }) // opacity is a cheap way to handle the overlap of markers
    .on('mouseover', curry(markerOver)(options, accessors))
    .on('mouseout', curry(markerOut)(options, accessors))
    .on('mouseleave', curry(hideTooltip));
}

function markerUpdate(selection, options, accessors, { xScale, yScale }, showTooltip) {
  selection.attr({
    r: curry(accessors.radius)(options),
    cx: datum => xScale(accessors.x(datum)),
    cy: datum => yScale(accessors.y(datum))
  })
  .on('mousemove', function(datum) {
    const serie = d3.select(this.parentNode.parentNode).datum();
    showTooltip(options.serie, { xScale, yScale }, serie, datum);
  });
}

function annotationEnter(selection, options, selector) {
  setupElement(selection, 'svg:text', selector);
}

export function annotationX(accessors, options, scale, isRight, datum) {
  return scale(accessors.x(datum)) + accessors.radius(options, datum) * (isRight(datum) ? -1 : 1);
}

export function annotationY(accessors, options, scale, isBottom, datum) {
  return scale(accessors.y(datum)) + accessors.radius(options, datum) * (isBottom(datum) ? -1 : 1);
}

export function annotationIsRight(accessors, areaIndex, domain, datum) {
  return accessors.x(datum) > computeAreaIndex(areaIndex, domain, last(domain));
}

export function annotationIsBottom(accessors, areaIndex, domain, datum) {
  return accessors.y(datum) < computeAreaIndex(1 - areaIndex, domain, last(domain));
}

function annotationBaseline(isBottom, datum) {
  const baseline = isBottom(datum) ? TEXT_MIDDLE : TEXT_BELOW;
  const element = d3.select(this);
  const offset = fontBaselineOffset(element, baseline);
  return `translate(0, ${offset})`;
}

function annotationUpdate(selection, options, accessors, { xScale, yScale }) {
  const areaIndex = options.serie.scatter.areaIndex;
  const isRight = curry(annotationIsRight)(accessors, areaIndex, xScale.domain());
  const isBottom = curry(annotationIsBottom)(accessors, areaIndex, yScale.domain());

  selection
    .text(curry(accessors.annotation)(options.serie.annotation))
    .attr({
      x: curry(annotationX)(accessors, options, xScale, isRight),
      y: curry(annotationY)(accessors, options, yScale, isBottom)
    })
    .style({
      ...fontStyle(options.serie.annotation.font),
      'text-anchor': datum => isRight(datum) ? TEXT_END : TEXT_START
    });

  // after first rendering to have the correct height
  selection.attr({ transform: curry(annotationBaseline)(isBottom) });
}

export default function eauors(target) {
  target.eauors = {
    ...target.eauors,
    pointEnter,
    pointUpdate,
    markerEnter,
    markerUpdate,
    annotationEnter,
    annotationUpdate
  };
}
