import * as R from 'ramda';

export default function computors(target) {
  target.computors = {
    ...target.computors,
    /*
      by placing focused datapoints at the end, insure them being rendered last, and so rendered over regular ones.
      baseline shoulbe on top of highlights
    */
    data: data => R.pipe(
      R.head,
      R.propOr([], 'datapoints'),
      R.sortWith([
        R.ascend(target.accessors.baselineIndex),
        R.ascend(target.accessors.highlightIndex),
      ]),
    )(data)
  };
}
