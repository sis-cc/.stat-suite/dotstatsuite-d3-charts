export default function selectors(target) {
  const chart = target.selectors.chart;

  target.selectors = {
    ...target.selectors,
    serie: `${chart}__serie`,
    point: `${chart}__point`,
    marker: `${chart}__marker`,
    annotation: `${chart}__annotation`
  };
}
