import { get, head } from 'lodash';
import { getFormat } from '../utils';

export default function accessors(target) {
  target.accessors = {
    ...target.accessors,
    data: data => get(head(data), 'datapoints', []),
    annotation: (options, datum) => getFormat(options.format.datapoint)(datum),
    yRange: (base, axis) => base.xScale.range(),
    radius: (options, datum) => options.serie.scatter.markerRadius,
  };
}
